/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.constants;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Vishrant Gupta
 */
public class AppConstants {

    static {

        try {
            Properties prop = new Properties();
            InputStream input = null;

            input = new FileInputStream("ITACMv2.config");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            if (prop.getProperty("server") != null) {
                SERVER = "http://" + prop.getProperty("server");
            } else {
                SERVER = "http://localhost:8080";
            }

            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String SERVER;

//    public static final String BASE_URL = "http://andy.cs.niu.edu:8080/networkcomparison";
    public static final String BASE_URL = SERVER + "/networkcomparison";
//    public static final String BASE_URL = "http://10.159.104.244:8080/networkcomparison";
    public static final String URL = BASE_URL + "/auth";
    public static final String ADD_USER_URL = BASE_URL + "/auth/adduser";
    public static final String ADD_ASSIGNMENT_URL = BASE_URL + "/rest/addAssignment";
    public static final String GET_ASSIGNMENT_DETAIL_URL = BASE_URL + "/rest/getAssignmentDetail";
    public static final String GET_ALL_ASSIGNMENT_URL = BASE_URL + "/rest/getAllMyAssignments";
    public static final String GET_ASSIGNMENT_SHARING_DETAILS = BASE_URL + "/rest/getAssignmentSharingDetail";
//    public static final String GET_ALL_ASSIGNMENT_URL = BASE_URL + "/rest/getAssignmentsSharedWithMe";
    public static final String FIND_MY_ASSIGNMENT_URL = BASE_URL + "/rest/getAssignmentsSharedWithMe";
    public static final String UPLOAD_MAP_URL = BASE_URL + "/rest/file/uploadStudentMaps";
    public static final String APP_NAME = "network_comparision";
    public static final String FOLDER_SEPARATOR = "\\";
    public static final String DOWNLOAD_MAP_LOCATION = AppConstants.BASE_URL + "/rest/file/downloadStudentMaps";
    public static final String APP_FOLDER = System.getProperty("java.io.tmpdir") + AppConstants.APP_NAME;

    public static final String SERVER_CONFIG = "ITACMv2.config";

    public static final String SUBMIT_ALIGNMENT = BASE_URL + "/rest/submitAlignment";
    public static final String GET_ALIGNMENT = BASE_URL + "/rest/getAlignment";
    public static final String GET_RECOMMENDATION = BASE_URL + "/rest/getRecommendationForTerm";
    public static final String DELETE_ASSIGNMENT_MAP = BASE_URL + "/rest/file/deleteAssignmentMaps";
    public static final String DELETE_ASSIGNMENT = BASE_URL + "/rest/removeAssignment";
    public static final String UNSHARE_ASSIGNMENT = BASE_URL + "/rest/unshareAssignment";

    public static final String IMPROVEMENT_TABLE_EXPORT_EXTENSION = "csv";

    public static final List<String> GRADE_LEVEL_LIST = new ArrayList<String>() {
        {
            add("K");
            add("1");
            add("2");
            add("3");
            add("4");
            add("5");
            add("6");
            add("7");
            add("8");
            add("9");
            add("10");
            add("11");
            add("12");
        }
    };

    public static final List<String> CLASSES_LIST = new ArrayList<String>() {
        {
            add("Language arts/English");
            add("History/Social Science");
            add("Math");
            add("Science");
            add("Music");
            add("Art");
            add("World language");
            add("Other");

        }
    };

    public static final List<GraphDistanceMethods> GRAPH_METHODS = new ArrayList<GraphDistanceMethods>() {
        {
            add(GraphDistanceMethods.GED);
            add(GraphDistanceMethods.GRAPH_KERNEL);
            add(GraphDistanceMethods.GRAPH_EMBEDDING);
        }
    };

    public static final String GED = "GED";

    public static enum GraphDistanceMethods {
        GED("GED (time consuming)"), GRAPH_KERNEL("Graph Kernel"), GRAPH_EMBEDDING("Graph Embedding");

        private final String method;

        GraphDistanceMethods(String method) {
            this.method = method;
        }

        public String getMethod() {
            return this.method;
        }

        @Override
        public String toString() {
            return this.method;
        }

    }

    public static final String HTML_BASE_DIR = "/html/";

    public static final String STUDENT_VISUALIZATION_HTML = HTML_BASE_DIR + "student.html";
    public static final String EXPERT_VISUALIZATION_HTML = HTML_BASE_DIR + "expert.html";

    public static final String FXML_BASE_DIR = "/fxml/";

    public static final String FORGOT_PASSWORD = FXML_BASE_DIR + "forgot_password.fxml";
    public static final String LOGIN = FXML_BASE_DIR + "login.fxml";
    public static final String MY_ASSIGNMENT_HOME = FXML_BASE_DIR + "myassignment_home_backup.fxml";
    public static final String FIND_ASSIGNMENT_HOME = FXML_BASE_DIR + "findassignment_home_backup.fxml";
    public static final String NEW_ASSIGNMENT = FXML_BASE_DIR + "new_assignment.fxml";
    public static final String COMPARE_NETWORK = FXML_BASE_DIR + "compare_network.fxml";
    public static final String SIGN_UP = FXML_BASE_DIR + "signup.fxml";
    public static final String WELCOME_SCREEN = FXML_BASE_DIR + "welcome_screen.fxml";
    public static final String NETWORK_VISUALIZE = FXML_BASE_DIR + "network_visualize.fxml";
    public static final String ALIGN_TERMS = FXML_BASE_DIR + "align_terms.fxml";
    public static final String HELP_TUTORIALS = FXML_BASE_DIR + "help_navigation.fxml";

//    public static final String HELP_BASE_DIR = "/help/"; // fix for files inside jar
    public static final String HELP_BASE_DIR = "/help/";

    public static final String HELP_TUTORIAL_MOTIVATE_LOCATION = HELP_BASE_DIR + "motivate/";
    public static final String HELP_TUTORIAL_BASIC_CREATION_LOCATION = HELP_BASE_DIR + "basiccreation/";
    public static final String HELP_TUTORIAL_ALIGNMENT_LOCATION = HELP_BASE_DIR + "alignment/";
    public static final String HELP_TUTORIAL_GUIDING_STUDENTS_LOCATION = HELP_BASE_DIR + "guiding/";
    public static final String HELP_TUTORIAL_CLUSTER_STUDENTS_LOCATION = HELP_BASE_DIR + "clusterstudents/";

    public static final String INVALID_EMAILID = "error.invalid.emailid";
    public static final String INVALID_LOGIN = "error.invalid.login";
    public static final String EMAIL_EXISTS = "error.email.already.exists";
    public static final String EMAIL_MATCH_ERROR = "error.email.match";
    public static final String PASSWORD_MATCH = "error.password.match";
    public static final String ACCOUNT_CREATED = "info.account.created";
    public static final String NEW_ASSIGNMENT_CREATED = "info.assignment.created";

    public static final String IMAGE_BASE_DIR = "/images/";

    public static final String TRASH_ICON = IMAGE_BASE_DIR + "trash.png";
    public static final String COMPARE_ICON = IMAGE_BASE_DIR + "compare.png";
    public static final String CHECKBOX_ICON = IMAGE_BASE_DIR + "checkbox.png";

    public static final String APP_ICON = IMAGE_BASE_DIR + "icon-29_new.png";

    public static final String APP_STYLE = "/styles/main.css";

    public static final String GED_PROPERTIES = "properties_letter.prop";

}
