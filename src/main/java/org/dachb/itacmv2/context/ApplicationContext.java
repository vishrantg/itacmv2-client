/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.context;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.exceptions.StageReinitializationException;
import org.dachb.itacmv2.graph.datastructure.StudentExpertGraph;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.model.AssignmentFile;
import org.dachb.itacmv2.model.Message;

public class ApplicationContext implements Cloneable {

    private final StudentExpertGraph studentExpertGraph = new StudentExpertGraph();

    private WebEngine expertMapWebEngine;
    private WebEngine studentMapWebEngine;

    private String tutorialLocation;

    public String getTutorialLocation() {
        return tutorialLocation;
    }

    public void setTutorialLocation(String tutorialLocation) {
        this.tutorialLocation = tutorialLocation;
    }

    public AssignmentFile getSelectedStudentMap() {
        return selectedStudentMap;
    }

    public void setSelectedStudentMap(AssignmentFile selectedStudentMap) {
        this.selectedStudentMap = selectedStudentMap;
    }

    public AssignmentFile getSelectedExpertMap() {
        return selectedExpertMap;
    }

    public void setSelectedExpertMap(AssignmentFile selectedExpertMap) {
        this.selectedExpertMap = selectedExpertMap;
    }

    private AssignmentFile selectedStudentMap;
    private AssignmentFile selectedExpertMap;

//    private final Map<org.graphstream.graph.Node, org.graphstream.graph.Node> previousAlignment = new HashMap<org.graphstream.graph.Node, org.graphstream.graph.Node>();
//    public void setPreviousAlignment(Map<org.graphstream.graph.Node, org.graphstream.graph.Node> previousAlignment) {
//        this.previousAlignment.clear();
//        this.previousAlignment.putAll(previousAlignment);
//    }
//
//    public Map<org.graphstream.graph.Node, org.graphstream.graph.Node> getPreviousAlignment() {
//        return previousAlignment;
//    }
    public Map<org.graphstream.graph.Node, org.graphstream.graph.Node> getFinalAlignment() {
        return finalAlignment;
    }

    private final Map<org.graphstream.graph.Node, org.graphstream.graph.Node> finalAlignment = new HashMap<org.graphstream.graph.Node, org.graphstream.graph.Node>();

    public WebEngine getExpertMapWebEngine() {
        return expertMapWebEngine;
    }

    public void setExpertMapWebEngine(WebEngine expertMapWebEngine) {
        this.expertMapWebEngine = expertMapWebEngine;
    }

    public WebEngine getStudentMapWebEngine() {
        return studentMapWebEngine;
    }

    public void setStudentMapWebEngine(WebEngine studentMapWebEngine) {
        this.studentMapWebEngine = studentMapWebEngine;
    }

    public StudentExpertGraph getStudentExpertGraph() {
        return studentExpertGraph;
    }

    public String getStudentGraph() {
        return studentExpertGraph.getStudentGraphData();
    }

    public String getExpertGraph() {
        return studentExpertGraph.getExpertGraphData();
    }

    public void clearStudentExpertGraph() {
        this.studentExpertGraph.setExpertGraph(null);
        this.studentExpertGraph.setExpertGraphData(null);
        this.studentExpertGraph.setStudentGraph(null);
        this.studentExpertGraph.setStudentGraphData(null);
    }

    private Label lblStudentNetwork;
    private Label lblExpertNetwork;

    public Label getLblStudentNetwork() {
        return lblStudentNetwork;
    }

    public void setLblStudentNetwork(Label lblStudentNetwork) {
        this.lblStudentNetwork = lblStudentNetwork;
    }

    public Label getLblExpertNetwork() {
        return lblExpertNetwork;
    }

    public void setLblExpertNetwork(Label lblExpertNetwork) {
        this.lblExpertNetwork = lblExpertNetwork;
    }

    private String studentMapName;
    private String expertMapName;

    public void setStudentMapName(String studentMapName) {
        this.studentMapName = studentMapName;
    }

    public void setExpertMapName(String expertMapName) {
        this.expertMapName = expertMapName;
    }

    public String getStudentMapName() {
        return this.studentMapName;
    }

    public String getExpertMapName() {
        return this.expertMapName;
    }

    public static ApplicationContext getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private Assignment assignment;

    public void setCurrentAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Assignment getCurrentAssignment() {
        return this.assignment;
    }

    private final Map<String, Initializable> loadedControllers = new HashMap<>();

    public Initializable getLoadedControllers(String controllerName) {
        return loadedControllers.get(controllerName);
    }

    public void setLoadedControllers(String controllerName, Initializable controllerObj) {
        loadedControllers.put(controllerName, controllerObj);
    }

    public void removeController(String controllerName) {
        if (loadedControllers.containsKey(controllerName)) {
            loadedControllers.remove(controllerName);
        }
    }

    private String applicationName;
    private Stage appStage;
    private Scene scene;
    private String token;
    private String username;
    private Message message;
//    private User user;
    private Object userData;

    public Object getUserData() {
        return userData;
    }

    public void setUserData(Object userData) {
        this.userData = userData;
    }

//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
    private Node currentTab;
    private final Locale defaultLocale = Locale.getDefault();
    private final ResourceBundle bundle = ResourceBundle.getBundle("MessageBundle", defaultLocale);

    private ApplicationContext() {
        setApplicationName("Incremental Thesaurus for Assessing Causal Maps v3");
    }

    public Node getCurrentTab() {
        return currentTab;
    }

    public void setCurrentTab(Node currentTab) {
        this.currentTab = currentTab;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApplicationName() {
        return applicationName;
    }

    private void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Stage getAppStage(String resource) {
        scene = createScene(resource);

        setScene(scene, resource);

        return this.appStage;
    }

    public Scene createScene(String resource) {
        Parent parent = (Parent) loadResource(resource);
        Scene s = new Scene(parent);

        s.getStylesheets().add(AppConstants.APP_STYLE);

        return s;
    }

    public void showHelpPopup() {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        // dialog.initOwner(this);

        dialog.setResizable(false);
        // dialog.initStyle(StageStyle.UNDECORATED);
        
        Scene dialogScene = createScene(AppConstants.HELP_TUTORIALS);

        
        dialog.setScene(dialogScene);
        dialog.show();

    }

    public Stage getAppStage(String resource, Double width, Double height) {

        scene = createScene(resource);

        setScene(scene, resource);

        this.appStage.setWidth(width);
        this.appStage.setHeight(height);

        appStage.setOnCloseRequest(e -> {
            System.exit(0);
        });

        appStage.setOnHidden(e -> {
            // 

        });

        return this.appStage;
    }

    public Message getMessage() {
        try {
            return message;
        } finally {
            message = null;
        }
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Node loadResource(String resource) {
        try {
            return FXMLLoader.load(getClass().getResource(resource));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Stage getAppStage() {
        return this.appStage;
    }

    public void setAppStage(Stage appStage) {
        if (this.appStage == null) {
            this.appStage = appStage;
            appStage.setTitle(getApplicationName());
            appStage.getIcons().add(new Image(AppConstants.APP_ICON));
        } else {
            throw new StageReinitializationException("Reinitializing application stage");
        }
    }

    public Scene getScene() {
        return scene;
    }

    private void setScene(Scene scene, String title) {
        appStage.setScene(scene);
    }

    protected Object readResolve() {
        return getInstance();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public String getI18nMessage(String key) {
        return bundle.getString(key);
    }

    private static class SingletonHelper {

        private static final ApplicationContext INSTANCE = new ApplicationContext();
    }

}
