/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Vishrant Gupta
 */
public class AlignTermsModel {

    private String studentTerm;
    private String expertTerm;
    private List<String> expertTerms;

    private String recommendation;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isSingle() {
        return single;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

//    private String fullName;
    private String gender;
    private boolean single;

    public AlignTermsModel(/*String fullName,*/String gender, boolean single) {
//        this.fullName = fullName;
        this.gender = gender;
        this.single = single;
    }

    public AlignTermsModel(String studentTerm, String expertTerm, List<String> expertTerms, String recommendation, boolean single) {
        this.studentTerm = studentTerm;
        this.expertTerm = expertTerm;
        this.expertTerms = expertTerms;
        this.recommendation = recommendation;
        this.single = single;
    }

    public String getStudentTerm() {
        return studentTerm;
    }

    public void setStudentTerm(String studentTerm) {
        this.studentTerm = studentTerm;
    }

    public String getExpertTerm() {
        return expertTerm;
    }

    public void setExpertTerm(String expertTerm) {
        this.expertTerm = expertTerm;
    }

    public List<String> getExpertTerms() {
        return expertTerms;
    }

    public void setExpertTerms(List<String> expertTerms) {
        this.expertTerms = expertTerms;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    @Override
    public String toString() {
        return "AlignTermsModel{" + "studentTerm=" + studentTerm + ", expertTerm=" + expertTerm + ", expertTerms=" + expertTerms + ", recommendation=" + recommendation + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.studentTerm);
        hash = 11 * hash + Objects.hashCode(this.expertTerm);
        hash = 11 * hash + Objects.hashCode(this.expertTerms);
        hash = 11 * hash + Objects.hashCode(this.recommendation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlignTermsModel other = (AlignTermsModel) obj;
        if (!Objects.equals(this.studentTerm, other.studentTerm)) {
            return false;
        }
        if (!Objects.equals(this.expertTerm, other.expertTerm)) {
            return false;
        }
        if (!Objects.equals(this.recommendation, other.recommendation)) {
            return false;
        }
        if (!Objects.equals(this.expertTerms, other.expertTerms)) {
            return false;
        }
        return true;
    }

}
