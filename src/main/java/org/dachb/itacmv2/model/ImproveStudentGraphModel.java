/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ImproveStudentGraphModel {

    public ImproveStudentGraphModel(Action a, String snode, String enode) {
        this.action = a;
        this.startNode = snode;
        this.endNode = enode;
//        this.reason.add(r);
    }

    public void addReason(String reason) {
        this.reasons.add(reason);
    }

    // action required
    private Action action;

    // start node in student graph
    private String startNode;

    // end node in student graph
    private String endNode;

    // this can be an improvement
    // private GraphElement element;
    // possible actions
    public enum Action {

        DELETE_NODE("Delete node"), ADD_NODE("Add node"), DELETE_EDGE("Delete edge"), ADD_EDGE("Add edge");

        private final String action;

        private Action(String action) {
            this.action = action;
        }

        @Override
        public String toString() {
            return this.action;
        }

        public String getAction() {
            return this.action;
        }

    }

    // as there can be multiple reason
    private List<String> reasons = new ArrayList<>();

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getStartNode() {
        return startNode;
    }

    public void setStartNode(String startNode) {
        this.startNode = startNode;
    }

    public String getEndNode() {
        return endNode;
    }

    public void setEndNode(String endNode) {
        this.endNode = endNode;
    }

    public List<String> getReasons() {
        return reasons;
    }

    public void setReason(List<String> reason) {
        this.reasons = reason;
    }

    @Override
    public int hashCode() {
        int hash = 7;

        hash = 71 * hash + Objects.hashCode(this.startNode);
//        hash = 71 * hash + Objects.hashCode(this.reason);
        hash = 71 * hash + Objects.hashCode(this.endNode);
        hash = 71 * hash + Objects.hashCode(this.action);

        // startnode, reason, endnode, action
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ImproveStudentGraphModel other = (ImproveStudentGraphModel) obj;
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
//        if (!Objects.equals(this.reason, other.reason)) {
//            return false;
//        }
        if (!Objects.equals(this.startNode, other.startNode)) {
            return false;
        }

        return Objects.equals(this.endNode, other.endNode);
    }

    @Override
    public String toString() {
        return "ImproveStudentGraphModel{" + "action=" + action + ", startNode=" + startNode + ", endNode=" + endNode + ", reason=" + reasons + '}';
    }

        }
