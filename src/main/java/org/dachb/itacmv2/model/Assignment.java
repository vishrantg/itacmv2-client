/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Vishrant Gupta
 */
public class Assignment {

    private String assignmentName;
    private String className;
    private String dueDate;
    private String gradeLevel;
    private String note;
    private List<String> sharingEmail;
    private Long id;
    private String numberOfPoints;
    private String owner;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getNumberOfPoints() {
        return numberOfPoints;
    }

    public void setNumberOfPoints(String numberOfPoints) {
        this.numberOfPoints = numberOfPoints;
    }

    private List<AssignmentFile> assignmentFiles;

    public List<AssignmentFile> getAssignmentFiles() {
        return assignmentFiles;
    }

    public void setAssignmentFiles(List<AssignmentFile> assignmentFiles) {
        this.assignmentFiles = assignmentFiles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Assignment() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Assignment(Long id, String assignmentName, String numberOfPoints, String className, String dueDate, String gradeLevel, String note, List<String> sharingEmail) {
        this.id = id;
        this.assignmentName = assignmentName;
        this.numberOfPoints = numberOfPoints;
        this.className = className;
        this.dueDate = dueDate;
        this.gradeLevel = gradeLevel;
        this.note = note;
        this.sharingEmail = sharingEmail;
    }

    public String toJSON() {
        StringBuilder sb = new StringBuilder();
        if (sharingEmail != null && !sharingEmail.isEmpty()) {
            sharingEmail.stream().map((email) -> {
                sb.append("\"");
                sb.append(email);
                return email;
            }).map((_item) -> {
                sb.append("\"");
                return _item;
            }).forEachOrdered((_item) -> {
                sb.append(",");
            });
        }

        String json
                = "{ "
                + "  \"assignmentName\" : \"" + assignmentName + "\","
                + "  \"numberOfPoints\" : \"" + numberOfPoints + "\","
                + "  \"className\" : \"" + className + "\","
                + "  \"dueDate\" : \"" + dueDate + "\","
                + "  \"gradeLevel\" : \"" + gradeLevel + "\","
                + "  \"id\" : \"" + id + "\","
                + "  \"note\" : \"" + note + "\"";
        if (sb.length() > 0) {
            json += ","
                    + "  \"sharingEmail\": ["
                    + "  		" + sb.toString().substring(0, sb.length() - 1) + ""
                    + "  	]";
        }
        json += "}";
        return json;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.gradeLevel);
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Assignment other = (Assignment) obj;
        if (!Objects.equals(this.gradeLevel, other.gradeLevel)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Assignment{" + "assignmentName=" + assignmentName + ", className=" + className + ", dueDate=" + dueDate + ", gradeLevel=" + gradeLevel + ", note=" + note + ", sharingEmail=" + sharingEmail + ", id=" + id + ", numberOfPoints=" + numberOfPoints + '}';
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getSharingEmail() {
        if (sharingEmail != null) {
            return Collections.unmodifiableList(sharingEmail);
        }
        return null;
    }

    public void setSharingEmail(List<String> sharingEmail) {
        this.sharingEmail = sharingEmail;
    }

}
