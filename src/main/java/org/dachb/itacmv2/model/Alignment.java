/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.model;

import java.util.Objects;

/**
 *
 * @author Vishrant Gupta
 */
public class Alignment {

    public Alignment() {
    }

    private String startNode;
    private String endNode;
//    private Integer endoursement;

    public Alignment(String startNode, String endNode/*, Integer endoursement*/) {
        this.startNode = startNode;
        this.endNode = endNode;
//        this.endoursement = endoursement;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.startNode);
        hash = 47 * hash + Objects.hashCode(this.endNode);
        return hash;
    }

    @Override
    public String toString() {
        return "Alignment{" + "startNode=" + startNode + ", endNode=" + endNode + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alignment other = (Alignment) obj;
        if (!Objects.equals(this.startNode, other.startNode)) {
            return false;
        }
        if (!Objects.equals(this.endNode, other.endNode)) {
            return false;
        }
        return true;
    }

    public String getStartNode() {
        return startNode;
    }

    public void setStartNode(String startNode) {
        this.startNode = startNode;
    }

    public String getEndNode() {
        return endNode;
    }

    public void setEndNode(String endNode) {
        this.endNode = endNode;
    }

}
