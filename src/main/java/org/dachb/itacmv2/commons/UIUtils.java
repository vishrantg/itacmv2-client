/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.commons;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.dachb.itacmv2.model.SharedWith;

/**
 *
 * @author Vishrant Gupta
 */
public class UIUtils {
    
    public static void clear(ObservableList<Node> nodes) {
        for (Node node : nodes) {
            if (node instanceof ComboBox) {
                ComboBox comboBox = (ComboBox) node;
                comboBox.getSelectionModel().select(0);
            }
            
            if (node instanceof Text) {
                Text text = (Text) node;
                text.setText("");
            }
            
            if (node instanceof TextField) {
                TextField textField = (TextField) node;
                textField.setText("");
            }
            
            if (node instanceof DatePicker) {
                DatePicker datePicker = (DatePicker) node;
                datePicker.setValue(null);
            }
            
            if (node instanceof TextArea) {
                TextArea textArea = (TextArea) node;
                textArea.setText("");
            }
            
            if (node instanceof TableView) {
                TableView tableView = (TableView) node;
                
                ObservableList<SharedWith> data = tableView.getItems();
                data.clear();
//                    = FXCollections.observableArrayList(
//                    new SharedWith("vishrant.gupta@gmail.com")
//                    );
            }
            
            
        }
    }
    
}
