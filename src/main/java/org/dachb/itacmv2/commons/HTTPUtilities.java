/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.commons;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.dachb.itacmv2.context.ApplicationContext;

public class HTTPUtilities {

    public static String sendPostJsonRequest(String post_url, String json) throws IOException {
        try {
            return sendPostRequest(post_url, json, 60000, 60000);
        } catch (java.net.HttpRetryException e) {
            ErrorUtilities.popErrorMessage("Session timed out, please login again.");
        }
        return null;
    }

    public static String sendGETRequest(String urlString) throws IOException {
        try {
            return sendGETRequest(urlString, 60000, 60000);
        } catch (java.net.HttpRetryException e) {
            ErrorUtilities.popErrorMessage("Session timed out, please login again.");
        }
        return null;
    }

    public static byte[] downloadFile(String urlString, String json) throws IOException {
        return downloadFile(urlString, json, 60000);
    }

    public static byte[] downloadFile(String urlString, String json, int timeout) throws IOException {

        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

//        urlConnection.setDoOutput(true);
//        urlConnection.setRequestProperty("X-Auth-Token", ApplicationContext.getInstance().getToken());
        setPostRequestHeader(urlConnection);

        try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
            if (json != null) {
//                System.out.println("json " + json);
                wr.write(json.getBytes("UTF-8"));
            }
            wr.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] buffer = null;

        return readStream(urlConnection.getInputStream(), buffer);
    }

    private static void setPostRequestHeader(HttpURLConnection urlConnection) throws ProtocolException {
        urlConnection.setDoOutput(true);
        urlConnection.setChunkedStreamingMode(0);

        urlConnection.setRequestProperty("content-type", "application/json");
        urlConnection.setRequestProperty("X-Auth-Token", ApplicationContext.getInstance().getToken());
        urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        urlConnection.setRequestProperty("Accept-Encoding", "gzip");
        urlConnection.setRequestProperty("Accept", "*/*");
        urlConnection.setRequestMethod("POST");
    }

    /**
     *
     */
    public static String sendPostRequest(String urlString, String json, int timeout, int readTimeout) throws IOException {

        String response = null;
        HttpURLConnection urlConnection = null;
        try {

            // SSLContext context = getHttpContext();
            // Tell the URLConnection to use a SocketFactory from our SSLContext
            URL url = new URL(urlString);

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                urlConnection = https;
//				urlConnection = setUpHttpsConnection(urlString);
            } else {
                urlConnection = (HttpURLConnection) url.openConnection();
            }
            urlConnection.setChunkedStreamingMode(-1);
            // urlConnection.setSSLSocketFactory(context.getSocketFactory());
//            try {
            setPostRequestHeader(urlConnection);

            urlConnection.setConnectTimeout(timeout);
            urlConnection.setReadTimeout(readTimeout);

            // Send post request
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            if (json != null) {
                wr.write(json.getBytes("UTF-8"));
            }
            wr.flush();
            wr.close();

            // InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            InputStream inputStream = urlConnection.getInputStream();

            // check if we got a compressed resonse back
            if ("gzip".equalsIgnoreCase(urlConnection.getContentEncoding())) {
                response = uncompressInputStream(inputStream);
            } else {
                response = convertStreamToString(inputStream);
            }
//            } finally {
//                urlConnection.disconnect();
//            }

        } catch (IOException ex) {
            System.out.println("Failed to establish SSL connection to server: " + ex.toString());
            throw ex;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return response;
    }

    public static String sendGETRequest(String urlString, int timeout, int readTimeout) throws IOException {
        String response = null;
        try {

            // SSLContext context = getHttpContext();
            // Tell the URLConnection to use a SocketFactory from our SSLContext
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = null;

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                urlConnection = https;
//				urlConnection = setUpHttpsConnection(urlString);
            } else {
                urlConnection = (HttpURLConnection) url.openConnection();
            }
            urlConnection.setChunkedStreamingMode(-1);
            try {

                // add reuqest header
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                urlConnection.setRequestProperty("Accept-Encoding", "gzip");
                urlConnection.setRequestProperty("X-Auth-Token", ApplicationContext.getInstance().getToken());
                urlConnection.setRequestProperty("Accept", "*/*");
                urlConnection.setConnectTimeout(timeout);
                urlConnection.setReadTimeout(readTimeout);

                InputStream inputStream = urlConnection.getInputStream();

                // check if we got a compressed resonse back
                if ("gzip".equalsIgnoreCase(urlConnection.getContentEncoding())) {
                    response = uncompressInputStream(inputStream);
                } else {
                    response = convertStreamToString(inputStream);
                }
            } finally {
                urlConnection.disconnect();
            }

        } catch (IOException ex) {
            System.out.println("Failed to establish SSL connection to server: " + ex.toString());
            throw ex;
        }

        return response;
    }

    private static SSLContext getHttpContext() throws Exception {
        // Load CAs from an InputStream
        // (could be from a resource or ByteArrayInputStream or ...)
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // My CRT file that I put in the assets folder
        // I got this file by following these steps:
        // * Go to https://littlesvr.ca using Firefox
        // * Click the padlock/More/Security/View Certificate/Details/Export
        // * Saved the file as littlesvr.crt (type X.509 Certificate (PEM))
        FileInputStream caInput = new FileInputStream("social_group_com.crt");
//		InputStream caInput = new BufferedInputStream(Constants.assetManager.open("social_group_com.crt"));
        Certificate ca = cf.generateCertificate(caInput);
        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        return context;
    }

    /**
     * Trust every server - dont check for any certificate
     */
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String bufferedStrChunk = null;

            while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                stringBuilder.append(bufferedStrChunk);
            }

            return stringBuilder.toString();
        } finally {
            inputStreamReader.close();
            bufferedReader.close();
        }
    }

    private static String uncompressInputStream(InputStream inputStream) throws IOException {
        StringBuilder value = new StringBuilder();

        GZIPInputStream gzipIn = null;
        InputStreamReader inputReader = null;
        BufferedReader reader = null;

        try {
            gzipIn = new GZIPInputStream(inputStream);
            inputReader = new InputStreamReader(gzipIn, "UTF-8");
            reader = new BufferedReader(inputReader);

            String line = "";
            while ((line = reader.readLine()) != null) {
                value.append(line + "\n");
            }
        } finally {
            try {
                if (gzipIn != null) {
                    gzipIn.close();
                }

                if (inputReader != null) {
                    inputReader.close();
                }

                if (reader != null) {
                    reader.close();
                }

            } catch (IOException io) {
                io.printStackTrace();
            }
        }
        return value.toString();
    }

//	public static String sendPostJsonZippedRequest(String urlString, String json) {
//		try {
//			URL url = new URL(urlString);
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setReadTimeout(15000);
//			conn.setConnectTimeout(3000);
//			conn.setRequestMethod("POST");
//			conn.setUseCaches(false);
//			conn.setDoInput(true);
//			conn.setDoOutput(true);
//
//			conn.setRequestMethod("POST");
//			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//			conn.setRequestProperty("Accept-Encoding", "gzip,deflate");
//
//			byte[] fooGzippedBytes = zipJson(json);
//
//			conn.setRequestProperty("Connection", "Keep-Alive");
//			conn.addRequestProperty("Content-length", fooGzippedBytes.length + "");
//			// conn.addRequestProperty("json-data",
//			// reqEntity.getContentType().getValue());
//
//			conn.connect();
//			OutputStream os = conn.getOutputStream();
//			os.write(fooGzippedBytes);
//			os.close();
//
//			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
//				return readStream(conn.getInputStream());
//			}
//
//		} catch (Exception e) {
//			System.out.println("multipart post error " + e + "(" + urlString + ")");
//		}
//		return null;
//	}
    private static byte[] zipJson(String json) throws IOException, UnsupportedEncodingException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzos = new GZIPOutputStream(baos);
        gzos.write(json.getBytes("UTF-8"));
        byte[] fooGzippedBytes = baos.toByteArray();
        return fooGzippedBytes;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));

            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    private synchronized static byte[] readStream(InputStream in, byte[] buffer) {

        try {
            buffer = new byte[in.available()];
            in.read(buffer);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }

        return buffer;
    }

    static SSLContext context;

    public static HttpsURLConnection setUpHttpsConnection(String urlString) throws Exception {
        HttpsURLConnection urlConnection = null;
        try {

            if (context == null) {
                context = getSSLContext();
            }

            // Tell the URLConnection to use a SocketFactory from our SSLContext
            URL url = new URL(urlString);
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setSSLSocketFactory(context.getSocketFactory());

        } catch (IOException ex) {
            System.out.println("Failed to establish SSL connection to server: " + ex.toString());
            throw ex;
//            return null;
        }
        return urlConnection;
    }

    private static SSLContext getSSLContext() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException,
            KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        FileInputStream caInput = new FileInputStream("social_group_com.crt");
//		InputStream caInput = new BufferedInputStream(Constants.assetManager.open("star_zoyride_com_bundled.crt"));
        X509Certificate ca;
        String alias = null;
        try {
            ca = (X509Certificate) cf.generateCertificate(caInput);
            alias = ca.getSubjectX500Principal().getName();
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
        } finally {
            caInput.close();
        }

        // Create a KeyStore containing our trusted CAs
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null);
        keyStore.setCertificateEntry(alias, ca);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("X509");
        kmf.init(keyStore, null /*clientCertPassword.toCharArray()*/);

        // Create a TrustManager that trusts the CAs in our KeyStore
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        return context;
    }

    // always verify the host - dont check for certificate
    private final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

//    public static String sendFileNew(List<File> files, String urlServer, String json) throws Exception {
//
//        System.out.println("Vishrant files size " + files.size());
//        
//        MultipartEntity entity = new MultipartEntity();
//        files.forEach(file-> {
//            entity.addPart("file", new FileBody(file));
//        });
//        
//
//        HttpPost request = new HttpPost(urlServer);
//        request.setEntity(entity);
//
//        HttpClient client = new DefaultHttpClient();
//        HttpResponse response = client.execute(request);
//        
//        return response.getStatusLine().getReasonPhrase();
//    }
    public static String sendCompressedPostFile(List<File> files, String urlServer, String json) throws Exception {

        try {

            String charset = "UTF-8";
            String param = "value";

            String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
            String CRLF = "\r\n"; // Line separator required by multipart/form-data.

            URLConnection connection = new URL(urlServer).openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            connection.setRequestProperty("X-Auth-Token", ApplicationContext.getInstance().getToken());
//            connection.addRequestProperty("json-data", json);

            try (
                    OutputStream output = connection.getOutputStream();
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);) {

                // Send normal param.
                writer.append("--" + boundary).append(CRLF);
                writer.append("Content-Disposition: form-data; name=\"param\"").append(CRLF);
                writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
                writer.append(CRLF).append(json).append(CRLF).flush();

                for (File file : files) {
                    writer.append("--" + boundary).append(CRLF);
                    writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"").append(CRLF);
                    writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF); // Text file itself must be saved in this charset!
                    writer.append(CRLF).flush();
                    Files.copy(file.toPath(), output);
                    output.flush(); // Important before continuing with writer!
                    writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
                }

                // Send text file.
                // Send binary file.
//                writer.append("--" + boundary).append(CRLF);
//                writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
//                writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
//                writer.append("Content-Transfer-Encoding: binary").append(CRLF);
//                writer.append(CRLF).flush();
//                Files.copy(binaryFile.toPath(), output);
//                output.flush(); // Important before continuing with writer!
//                writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
                // End of multipart/form-data.
                writer.append("--" + boundary + "--").append(CRLF).flush();
            } catch (Exception e) {
                e.printStackTrace();
            }

// Request is lazily fired whenever you need to obtain information about response.
//            int responseCode = ((HttpURLConnection) connection).getResponseCode();
//            System.out.println(responseCode); // Should be 200
            String response = null;
            int responseCode = ((HttpURLConnection) connection).getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                response = readStream(connection.getInputStream());
            } else {
                throw new Exception(response);
            }

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public static byte[] compress(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new DeflaterOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }

    private static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) > 0) {
                baos.write(buffer, 0, len);
            }
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

}
