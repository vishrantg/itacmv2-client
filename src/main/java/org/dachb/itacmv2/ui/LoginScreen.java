/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import java.util.Properties;
import javafx.application.Application;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;

/**
 *
 * @author Vishrant Gupta
 */
public class LoginScreen extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public boolean loadServerInfo() throws Exception {

        File f = new File(LoginScreen.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        File exists = new File(f.getParentFile().getAbsoluteFile(), "ITACMv2.config");

        if (!exists.exists() && !exists.createNewFile()) {
            return false;
        }

        Properties prop = new Properties();
        InputStream input = null;

        input = new FileInputStream(exists);

        // load a properties file
        prop.load(input);
        input.close();

        // get the property value and print it out
        if (prop.getProperty("server") != null && !prop.getProperty("server").trim().isEmpty()) {
            return true;
        } else {
            TextInputDialog dialog = new TextInputDialog("andy.cs.niu.edu:8080");
            dialog.setTitle("Server configuration");
            dialog.setHeaderText("Remote server not configured");
            dialog.setContentText("Please enter server name:");

            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {

                OutputStream output = null;

                output = new FileOutputStream("ITACMv2.config");

                // set the properties value
                prop.setProperty("server", result.get());
                // save properties to project root folder
                prop.store(output, null);

                output.close();

                return true;
            } else {
                System.exit(-1);
            }
        }

        return false;
    }

    @Override
    public void start(Stage stage) throws Exception {

        while (!loadServerInfo()) {
        }
        load(stage);

    }

    private void load(Stage stage) {
        ApplicationContext context = ApplicationContext.getInstance();
        context.setAppStage(stage);
        stage = context.getAppStage(AppConstants.LOGIN);
        stage.setMaximized(false);
        stage.show();
    }

    @Override
    public void stop() throws Exception {

        File f = new File(AppConstants.APP_FOLDER);
        deleteDir(f);

        super.stop();
    }

    private void deleteDir(File file) {
        if (file != null) {
            File[] contents = file.listFiles();
            if (contents != null) {
                for (File f : contents) {
                    deleteDir(f);
                }
            }
            file.delete();
        }
    }

}
