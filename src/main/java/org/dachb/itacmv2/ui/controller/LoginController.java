/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanExpression;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import org.dachb.itacmv2.commons.ErrorUtilities;
import org.dachb.itacmv2.commons.MessageType;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.model.Message;
import org.dachb.itacmv2.services.LoginService;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class LoginController implements Initializable {

    @FXML
    Button btnLogin;

    @FXML
    TextField txtUserName;

    @FXML
    PasswordField txtPassword;

    @FXML
    Label lblError;

    @FXML
    Hyperlink linkForgotPassword;

    @FXML
    Hyperlink linkSignUp;

    @FXML
    ProgressIndicator loginProgress;

    private final ApplicationContext context = ApplicationContext.getInstance();

    public LoginController() {
    }

    /**
     * Initializes the controller class.
     */
    @FXML
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnLogin.disableProperty().bind(
                Bindings.isEmpty(txtUserName.textProperty())
                        .or(Bindings.isEmpty(txtPassword.textProperty())
                        )
        );

        Message msg = context.getMessage();

        if (msg != null) {
            ErrorUtilities.displayError(lblError, msg);
        }
    }

    @FXML
    private void forgotPassword(ActionEvent forgotPasswordEvent) {
        ApplicationContext.getInstance().getAppStage(AppConstants.FORGOT_PASSWORD); //.show();
    }

    @FXML
    private void signUp(ActionEvent signUpEvent) {
        ApplicationContext.getInstance().getAppStage(AppConstants.SIGN_UP); //.show();
    }

    @FXML
    private void login(ActionEvent loginEvent) {

        try {

            ErrorUtilities.hideError(lblError);

            LoginService loginService = new LoginService(txtUserName.getText(), txtPassword.getText());
            loginService.start();

            btnLogin.disableProperty().bind(BooleanExpression.booleanExpression(loginService.runningProperty()));
            txtUserName.disableProperty().bind(BooleanExpression.booleanExpression(loginService.runningProperty()));
            txtPassword.disableProperty().bind(BooleanExpression.booleanExpression(loginService.runningProperty()));

            loginProgress.progressProperty().bind(loginService.progressProperty());
            loginProgress.visibleProperty().bind(BooleanExpression.booleanExpression(loginService.runningProperty()));

            loginService.setOnSucceeded((event) -> {

                if (loginService.getValue() != null) {
                    context.getAppStage(AppConstants.WELCOME_SCREEN).setMaximized(true);
                } else {
                    Message message = new Message();

                    message.setMessage(loginService.getMessage() != null ? loginService.getMessage()
                            : context.getI18nMessage(AppConstants.INVALID_LOGIN));
                    message.setType(MessageType.ERROR);

                    ErrorUtilities.displayError(lblError, message);

                }
            });

            loginService.setOnFailed((event) -> {

            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
