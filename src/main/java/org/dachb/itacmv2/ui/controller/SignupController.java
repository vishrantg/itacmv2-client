/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanExpression;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.dachb.itacmv2.commons.EmailUtilities;
import org.dachb.itacmv2.commons.ErrorUtilities;
import org.dachb.itacmv2.commons.MessageType;
import org.dachb.itacmv2.commons.Utils;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.model.Message;
import org.dachb.itacmv2.services.SignupService;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class SignupController implements Initializable {

    @FXML
    TextField txtFirstName;

    @FXML
    TextField txtLastName;

    @FXML
    TextField txtSchoolName;

    @FXML
    ComboBox comboGrade;

    @FXML
    ComboBox comboSubject;

    @FXML
    TextField txtEmail;

    @FXML
    TextField txtConfirmEmail;

    @FXML
    PasswordField password;

    @FXML
    Hyperlink login;

    @FXML
    Button btnSignup;

    @FXML
    TextField txtUsername;

    @FXML
    PasswordField confirmPassword;
    @FXML
    Label lblError;
    @FXML
    GridPane signupGrid;
    @FXML
    ProgressIndicator progressIndicator;

    SignupService signupService;
    ApplicationContext context = ApplicationContext.getInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnSignup.disableProperty().bind(
                Bindings.isEmpty(txtFirstName.textProperty())
                        .or(Bindings.isEmpty(txtLastName.textProperty())
                                .or(Bindings.isEmpty(txtUsername.textProperty()))
                                //                                .or(Bindings.isEmpty(comboSubject.promptTextProperty()))
                                .or(Bindings.isEmpty(txtEmail.textProperty()))
                                .or(Bindings.isEmpty(txtConfirmEmail.textProperty()))
                                .or(Bindings.isEmpty(password.textProperty()))
                                .or(Bindings.isEmpty(confirmPassword.textProperty()))
                        )
        );

        comboGrade.getItems().clear();

        comboGrade.getItems().addAll(AppConstants.GRADE_LEVEL_LIST);
        comboGrade.getSelectionModel().selectFirst();

        comboSubject.getItems().clear();

        comboSubject.getItems().addAll(AppConstants.CLASSES_LIST);
        comboSubject.getSelectionModel().selectFirst();

        progressIndicator.setVisible(false);

    }

    @FXML
    private void login(ActionEvent loginEvent) {
        ApplicationContext.getInstance().getAppStage(AppConstants.LOGIN); // .show();
    }

    @FXML
    private void signup(ActionEvent signupEvent) {

        ErrorUtilities.hideError(lblError);

        if (!EmailUtilities.validateEmail(txtEmail.getText())) {

            Message message = new Message();
            message.setMessage(context.getI18nMessage(AppConstants.INVALID_EMAILID));
            message.setType(MessageType.ERROR);

            ErrorUtilities.displayError(lblError, message);
            return;
        }

        if (!Utils.compareIgnoreCare(txtEmail.getText(), txtConfirmEmail.getText())) {

            Message message = new Message();
            message.setMessage(context.getI18nMessage(AppConstants.EMAIL_MATCH_ERROR));
            message.setType(MessageType.ERROR);

            ErrorUtilities.displayError(lblError, message);
            return;
        }

        if (!Utils.compareIgnoreCare(password.getText(), confirmPassword.getText())) {

            Message message = new Message();
            message.setMessage(context.getI18nMessage(AppConstants.PASSWORD_MATCH));
            message.setType(MessageType.ERROR);

            ErrorUtilities.displayError(lblError, message);
            return;
        }

//        User user = new User(txtUsername.getText(), password.getText(), txtFirstName.getText(), txtLastName.getText(), txtSchoolName.getText(), comboGrade.getSelectionModel().getSelectedItem().toString(), comboSubject.getSelectionModel().getSelectedItem().toString(), txtEmail.getText());
        signupService = new SignupService(txtUsername.getText(), password.getText(), txtFirstName.getText(), txtLastName.getText(), txtSchoolName.getText(), comboGrade.getSelectionModel().getSelectedItem().toString(), comboSubject.getSelectionModel().getSelectedItem().toString(), txtEmail.getText());

        signupService.start();

        btnSignup.disableProperty().bind(BooleanExpression.booleanExpression(signupService.runningProperty()));
        login.disableProperty().bind(BooleanExpression.booleanExpression(signupService.runningProperty()));

        progressIndicator.progressProperty().bind(signupService.progressProperty());
        progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(signupService.runningProperty()));

        signupService.setOnSucceeded((event) -> {
            String response = signupService.getMessage();
            lblError.textProperty().unbind();

            MessageType type = MessageType.ERROR;
            if (response != null && !response.trim().isEmpty()) {
                if (response.equalsIgnoreCase("success")) {

                    Message message = new Message();
                    message.setMessage(context.getI18nMessage(AppConstants.ACCOUNT_CREATED));
                    message.setType(MessageType.INFO);

                    context.setMessage(message);
                    context.getAppStage(AppConstants.LOGIN); // .setUserData(response);
                }
            } else {
                response = "Error occured";
            }

            Message message = new Message();
            message.setMessage(response);
            message.setType(type);

            ErrorUtilities.displayError(lblError, message);
        });

        signupService.setOnFailed((event) -> {
        });

    }

}
