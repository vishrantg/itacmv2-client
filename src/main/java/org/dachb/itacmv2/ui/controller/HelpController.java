/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.dachb.itacmv2.context.ApplicationContext;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class HelpController implements Initializable {

    @FXML
    private Label lblPageNumber;

    @FXML
    private ImageView tutorialImageView;

    private final ApplicationContext context = ApplicationContext.getInstance();

    private int currentImage = 0;
    
    List<String> files = new ArrayList<String>();
    
    @FXML
    private Button prevBtn;
    @FXML
    private Button nextBtn;
    @FXML
    private Button closeBtn;
    @FXML
    private BorderPane layout;
    @FXML
    private GridPane imageGrid;

    private void getFiles(String path) {

        FileSystem fileSystem = null;

        try {
            URI uri = getClass().getResource(path).toURI();

            Path myPath;
            if (uri.getScheme().equals("jar")) {
                fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
                myPath = fileSystem.getPath(path);
            } else {
                myPath = Paths.get(uri);
            }
            Stream<Path> walk = Files.walk(myPath, 1);
            Iterator<Path> it = walk.iterator();
            it.next(); // skipping directory

            for (; it.hasNext();) {
                files.add(it.next().toString());
            }
        } catch (Exception ex) {
            Logger.getLogger(HelpController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (fileSystem != null) {
                try {
                    fileSystem.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        getFiles(context.getTutorialLocation());

        files.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        prevBtn.setDisable(true);
        loadImage(currentImage);
    }

    private void loadImage(int position) {

        if (position > (files.size() - 1)) {
            return;
        }

        Image image = new Image(files.get(position));

        tutorialImageView.setImage(image);
        setPageNumber();

        tutorialImageView.
                fitWidthProperty().bind(imageGrid.widthProperty());

        if (currentImage == 0) {
            prevBtn.setDisable(true);
        } else {
            prevBtn.setDisable(false);
        }

        if (currentImage == (files.size() - 1)) {
            nextBtn.setDisable(true);
        } else {
            nextBtn.setDisable(false);
        }
    }

    private void setPageNumber() {
        lblPageNumber.setText("Page " + (currentImage + 1) + " of " + files.size());
    }

    @FXML
    private void prevImage(ActionEvent event) {
        if (currentImage > 0) {
            currentImage--;
            loadImage(currentImage);
        }

    }

    @FXML
    private void nextImage(ActionEvent event) {
        if (currentImage != (files.size() - 1)) {
            currentImage++;
            loadImage(currentImage);

        }

    }

    @FXML
    private void exitTutorial(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

}
