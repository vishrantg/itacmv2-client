/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class WelcomeScreenController implements Initializable {

    Button newAssignment;

    @FXML
    ScrollPane myAssignmentScrollPane;

    @FXML
    ScrollPane findAssignmentScrollPane;

    @FXML
    TabPane tabPaneWelcomeScreen;

    private final ApplicationContext context = ApplicationContext.getInstance();
    @FXML
    private Button motivateUseBtn;
    @FXML
    private Button basicCreationBtn;
    @FXML
    private Button alignmentBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        Node content;

        if (context.getCurrentTab() != null) {
            ScrollPane node = (ScrollPane) context.getCurrentTab();

            SingleSelectionModel<Tab> selectionModel = tabPaneWelcomeScreen.getSelectionModel();

            if (node.getId().equalsIgnoreCase("myAssignmentScrollPane")) {
                content = context.loadResource(AppConstants.MY_ASSIGNMENT_HOME);
                context.setCurrentTab(myAssignmentScrollPane);
                myAssignmentScrollPane.setContent(content);

                selectionModel.select(1);

            } else if (node.getId().equalsIgnoreCase("findAssignmentScrollPane")) {
                content = context.loadResource(AppConstants.FIND_ASSIGNMENT_HOME);
                context.setCurrentTab(findAssignmentScrollPane);
                findAssignmentScrollPane.setContent(content);

                selectionModel.select(0);
            }

        } else {
            content = context.loadResource(AppConstants.FIND_ASSIGNMENT_HOME);
            context.setCurrentTab(findAssignmentScrollPane);
            findAssignmentScrollPane.setContent(content);
        }

        tabPaneWelcomeScreen.setPrefHeight(context.getAppStage().getHeight());
        context.getAppStage().heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                tabPaneWelcomeScreen.setPrefHeight(newValue.doubleValue());
            }

        });

    }

    @FXML
    public void findSharedAssignmentHome(MouseEvent findAssignmentEvent) {
        Node node = context.loadResource(AppConstants.FIND_ASSIGNMENT_HOME);
        context.setCurrentTab(findAssignmentScrollPane);
        findAssignmentScrollPane.setContent(node);
    }

    @FXML
    public void myassignmentHome(MouseEvent myAssignmentEvent) {
        Node node = context.loadResource(AppConstants.MY_ASSIGNMENT_HOME);
        context.setCurrentTab(myAssignmentScrollPane);
        myAssignmentScrollPane.setContent(node);
    }

    @FXML
    private void motivateUseBtnAction(ActionEvent event) {
        context.setTutorialLocation(AppConstants.HELP_TUTORIAL_MOTIVATE_LOCATION);
        context.showHelpPopup();
    }

    @FXML
    private void basicCreationBtnAction(ActionEvent event) {
        context.setTutorialLocation(AppConstants.HELP_TUTORIAL_BASIC_CREATION_LOCATION);
        context.showHelpPopup();
    }

    @FXML
    private void alignmentBtnAction(ActionEvent event) {
        context.setTutorialLocation(AppConstants.HELP_TUTORIAL_ALIGNMENT_LOCATION);
        context.showHelpPopup();
    }
    
    @FXML
    private void guidingSudentsBtnAction(ActionEvent event) {
        context.setTutorialLocation(AppConstants.HELP_TUTORIAL_GUIDING_STUDENTS_LOCATION);
        context.showHelpPopup();
    }
    
    @FXML
    private void clusterStudentsBtnAction(ActionEvent event) {
        context.setTutorialLocation(AppConstants.HELP_TUTORIAL_CLUSTER_STUDENTS_LOCATION);
        context.showHelpPopup();
    }
    
}
