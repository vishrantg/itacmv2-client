/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import org.dachb.itacmv2.commons.EmailUtilities;
import org.dachb.itacmv2.commons.ErrorUtilities;
import org.dachb.itacmv2.commons.MessageType;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.model.Message;
import org.dachb.itacmv2.model.SharedWith;
import org.dachb.itacmv2.services.UnshareAssignmentService;
import org.dachb.itacmv2.services.UserAssignmentService;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class NewAssignmentController implements Initializable {

    @FXML
    private ComboBox gradeLevel;

    @FXML
    private TextField className;

    @FXML
    private TableView sharedListingTable;

    @FXML
    private TableColumn<SharedWith, SharedWith> sharedWithEmailId;

    @FXML
    private TableColumn<SharedWith, SharedWith> remove;

    @FXML
    private TextField addEmail;

    @FXML
    private TextField txtNumPoints;

    @FXML
    private Label lblError;

    private final ObservableList<SharedWith> data = FXCollections.observableArrayList();
    ApplicationContext context = ApplicationContext.getInstance();

    @FXML
    TextField assignmentName;

    @FXML
    DatePicker dueDatePicker;
    @FXML
    TextArea txtNote;

    @FXML
    GridPane newAssignmentGrid;

    @FXML
    ProgressIndicator progressIndicator;

    private UserAssignmentService service;

    @FXML
    Button btnOkay;

    Gson gson = new Gson();

    private Assignment assignment = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // assignment = (Assignment) context.getUserData();
        assignment = context.getCurrentAssignment();
        context.setCurrentAssignment(null);

        gradeLevel.getItems().clear();
        gradeLevel.getItems().addAll(AppConstants.GRADE_LEVEL_LIST);

        sharedWithEmailId.setCellValueFactory(
                new PropertyValueFactory<>("emailId"));

        if (assignment != null) {

            assignmentName.setText(assignment.getAssignmentName());
            gradeLevel.getSelectionModel().select(assignment.getGradeLevel());
            className.setText(assignment.getClassName());
            txtNumPoints.setText(assignment.getNumberOfPoints());
            txtNote.setText(assignment.getNote());

            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
                        "MMM dd, yyyy");

                LocalDate ld = LocalDate.parse(assignment.getDueDate(), formatter);

                dueDatePicker.setValue(ld);

            } catch (Exception ex) {
                ex.printStackTrace();
            }

//            String dueDate = assignment.getDueDate();
            assignment.setDueDate(null);
            service = new UserAssignmentService(assignment, AppConstants.GET_ASSIGNMENT_SHARING_DETAILS);

            service.start();

            progressIndicator.progressProperty().bind(service.progressProperty());
            progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));

            service.setOnSucceeded((event) -> {
                if (service.getMessage() != null) {
                    Type listType = new TypeToken<List<String>>() {
                    }.getType();
                    List<String> sharing = new Gson().fromJson(service.getMessage(), listType);

                    for (String email : sharing) {
                        final SharedWith sharedWith = new SharedWith(email);
                        data.add(sharedWith);
                    }
                }

            });

        } else {

            sharedListingTable.setPlaceholder(new Label("No emails added"));

            gradeLevel.getSelectionModel().selectFirst();

            className.setText("");

        }

        sharedListingTable.setRowFactory(tv -> new TableRow<SharedWith>() {
            Tooltip tooltip = new Tooltip();

            @Override
            public void updateItem(SharedWith sharedWith, boolean empty) {
                super.updateItem(sharedWith, empty);
                if (sharedWith == null) {
                    setTooltip(null);
                } else {
                    tooltip.setText(sharedWith.getEmailId());
                    setTooltip(tooltip);
                }
            }
        });

        btnOkay.disableProperty().bind(
                Bindings.isEmpty(assignmentName.textProperty())
        );

        remove.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        remove.setCellFactory(param -> new TableCell<SharedWith, SharedWith>() {
            private final Button deleteButton = new Button("Unshare");

            @Override
            protected void updateItem(SharedWith sharing, boolean empty) {
                super.updateItem(sharing, empty);

                if (sharing == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {

                    UnshareAssignmentService unshareAssignmentService = new UnshareAssignmentService(assignment, sharing.getEmailId(), AppConstants.UNSHARE_ASSIGNMENT);

                    unshareAssignmentService.start();

                    progressIndicator.progressProperty().bind(unshareAssignmentService.progressProperty());
                    progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(unshareAssignmentService.runningProperty()));

                    unshareAssignmentService.setOnSucceeded(serviceSuccess -> {

                        String response = unshareAssignmentService.getMessage();

                        if (response.equalsIgnoreCase("success")) {
                            data.remove(sharing);
                        }
                    });

                    unshareAssignmentService.setOnFailed(value -> {
                        String response = "Error occured, please try again.";

                        Message message = new Message();
                        message.setType(MessageType.ERROR);
                        message.setMessage(response);

                        ErrorUtilities.displayError(lblError, message);
                    });

                });
            }
        });

        sharedListingTable.setItems(data);

// BELOW CODE TO ADD TEXT IN FIELD ON DOUBLE CLICK
//        sharedListingTable.setRowFactory(tv -> {
//            TableRow<SharedWith> row = new TableRow<>();
//            row.setOnMouseClicked(event -> {
//                if (event.getClickCount() == 2 && (!row.isEmpty())) {
//                    SharedWith rowData = row.getItem();
//                    addEmail.setText(rowData.getEmailId());
//                    // System.out.println(rowData);
//                }
//            });
//            return row;
//        });
// BELOW CODE TO ADD TEXT IN FIELD ON DOUBLE CLICK ENDS
    }

    @FXML
    public void addEmail(ActionEvent addEvent) {

        ErrorUtilities.hideError(lblError);

        final String email = addEmail.getText();
        final SharedWith sharedWith = new SharedWith(email);
        if (EmailUtilities.validateEmail(email)) {
            if (!data.contains(sharedWith)) {
                data.add(sharedWith);
                addEmail.clear();
            } else {

                Message message = new Message();
                message.setType(MessageType.ERROR);
                message.setMessage(context.getI18nMessage(AppConstants.EMAIL_EXISTS));

                ErrorUtilities.displayError(lblError, message);
            }
        } else {

            Message message = new Message();
            message.setType(MessageType.ERROR);
            message.setMessage(context.getI18nMessage(AppConstants.INVALID_EMAILID));

            ErrorUtilities.displayError(lblError, message);
        }
    }

    @FXML
    public void cancelAction(ActionEvent cancelEvent) {
        Node node = context.loadResource(AppConstants.MY_ASSIGNMENT_HOME);
        ScrollPane tab = (ScrollPane) context.getCurrentTab();
        tab.setContent(node);
    }

    @FXML
    public void addAssignment(ActionEvent addAssignmentEvent) throws ParseException {

        ErrorUtilities.hideError(lblError);

        List<String> emails = new ArrayList<String>();

        data.forEach((sharedWith) -> {
            emails.add(sharedWith.getEmailId());
        });

        if (dueDatePicker == null || dueDatePicker.getValue() == null || dueDatePicker.getValue().toString().isEmpty()) {

            Message message = new Message();
            message.setType(MessageType.ERROR);
            message.setMessage("Please enter due date");

            ErrorUtilities.displayError(lblError, message);
            return;
        }
        
        if(txtNumPoints == null || txtNumPoints.getText().isEmpty()) {
            Message message = new Message();
            message.setType(MessageType.ERROR);
            message.setMessage("Please enter number of points");

            ErrorUtilities.displayError(lblError, message);
            return;
        }

        String note = new String();
        if (txtNote != null) {
            note = txtNote.getText();
        } else {
            note = "";
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US);
        String formattedValue = (dueDatePicker.getValue()).format(formatter);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = sdf.parse(formattedValue);
        sdf.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        assignment = new Assignment(assignment != null ? assignment.getId() : null, assignmentName.getText(), txtNumPoints.getText(), className.getText(), sdf.format(d), gradeLevel.getSelectionModel().getSelectedItem().toString(), note, emails);

        service = new UserAssignmentService(assignment, AppConstants.ADD_ASSIGNMENT_URL);

        service.start();

        newAssignmentGrid.getChildren().forEach((node) -> {
            node.disableProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));
        });

        progressIndicator.progressProperty().bind(service.progressProperty());
        progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));

        service.setOnSucceeded((event) -> {
            String response = service.getMessage();
            lblError.textProperty().unbind();

            MessageType type = MessageType.ERROR;
            if (response != null) {
                if (response.equalsIgnoreCase("success")) {

                    Node node = context.loadResource(AppConstants.MY_ASSIGNMENT_HOME);
                    ScrollPane tab = (ScrollPane) context.getCurrentTab();
                    tab.setContent(node);

                } else {

                    Message message = new Message();
                    message.setType(type);
                    message.setMessage(response);

                    ErrorUtilities.displayError(lblError, message);
                }
            } else {
                response = "Error occured";

                Message message = new Message();
                message.setType(type);
                message.setMessage(response);

                ErrorUtilities.displayError(lblError, message);
            }

        });

        service.setOnFailed((event) -> {
        });

    }

}
