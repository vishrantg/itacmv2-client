/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import com.google.gson.Gson;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.dachb.itacmv2.commons.HTTPUtilities;
import org.dachb.itacmv2.commons.Utils;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.graph.datastructure.Graph;
import org.dachb.itacmv2.graph.datastructure.VertexDetail;
import org.dachb.itacmv2.model.UserTermsAlignment;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class VisualizeNetworksController implements Initializable {

    @FXML
    private Label lblTop;
    @FXML
    private Label lblBottom;
    @FXML
    private Label lblStudentNetwork;
    @FXML
    private Label lblExpertNetwork;
    @FXML
    private WebView webviewStudent;
    @FXML
    private WebView webviewExpert;

    @FXML
    private ScrollPane scrollNetworkVisualize;

    private final ApplicationContext context = ApplicationContext.getInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {

            String studentUrlStr = getClass().getResource(AppConstants.STUDENT_VISUALIZATION_HTML).toExternalForm();
            String expertUrlStr = getClass().getResource(AppConstants.EXPERT_VISUALIZATION_HTML).toExternalForm();
            webviewStudent.getEngine().load(studentUrlStr);

            webviewExpert.getEngine().load(expertUrlStr);

            WebEngine webEngineStudent = webviewStudent.getEngine();
            WebEngine webEngineExpert = webviewExpert.getEngine();

            context.setStudentMapWebEngine(webEngineStudent);
            context.setExpertMapWebEngine(webEngineExpert);

            context.setLblStudentNetwork(lblStudentNetwork);
            context.setLblExpertNetwork(lblExpertNetwork);

            progressNetworkLoad.visibleProperty().bind(BooleanExpression.booleanExpression(webEngineStudent.getLoadWorker().runningProperty()));
            progressNetworkLoad.visibleProperty().bind(BooleanExpression.booleanExpression(webEngineExpert.getLoadWorker().runningProperty()));

            final com.sun.webkit.WebPage webPage = com.sun.javafx.webkit.Accessor.getPageFor(webEngineStudent);
            webPage.setBackgroundColor(0);

            com.sun.javafx.webkit.Accessor.getPageFor(webviewExpert.getEngine()).setBackgroundColor(0);
//            webPage.setBackgroundColor(0);

            lblExpertNetwork.setText(context.getLblExpertNetwork().getText());
            lblStudentNetwork.setText(context.getLblStudentNetwork().getText());

            Gson gson = new Gson();

            webEngineExpert.getLoadWorker().stateProperty().addListener(
                    (ObservableValue<? extends Worker.State> ov, Worker.State oldState,
                            Worker.State newState) -> {

                        lblExpertNetwork.setText(context.getLblExpertNetwork().getText());
                        synchronized (VisualizeNetworksController.class) {
                            if (newState == Worker.State.SUCCEEDED) {
                                JSObject win = (JSObject) webEngineExpert.executeScript("window");

                                int retryCount = 0;
                                retry:

                                try {
                                    Graph graph = gson.fromJson(context.getStudentExpertGraph().getExpertGraphData(), Graph.class);

                                    UserTermsAlignment termsAlignment = new UserTermsAlignment();
                                    termsAlignment.setAssignmentId(context.getCurrentAssignment().getId());

                                    termsAlignment.setExpertMapId(context.getSelectedExpertMap().getFileId());
                                    termsAlignment.setStudentMapId(context.getSelectedStudentMap().getFileId());

                                    String response = HTTPUtilities.sendPostRequest(AppConstants.GET_ALIGNMENT, gson.toJson(termsAlignment), 20000, 20000);

                                    Map<String, String> alignment = Utils.convert(response);

                                    Collection<VertexDetail> nodes = graph.getNodes();
                                    for (VertexDetail node : nodes) {
                                        if (alignment.containsKey(node.getName())) {
                                            node.setColor("#21ff9d");
                                        } else {
                                            node.setColor("#e8ff1f");
                                        }
                                    }

                                    webEngineExpert.executeScript("loadExpertData(" + gson.toJson(graph) + ")");
                                    win.setMember("app", new JavaApp());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (retryCount < 3) {
                                        retryCount++;
                                        break retry;
                                    }
                                }
                            }
                        }
//                        enableFirebug(webEngineExpert);
                    });

//             webEngineExpert.executeScript("if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
            webEngineStudent.getLoadWorker().stateProperty().addListener(
                    (ObservableValue<? extends Worker.State> ov, Worker.State oldState,
                            Worker.State newState) -> {

                        lblStudentNetwork.setText(context.getLblStudentNetwork().getText());
                        synchronized (VisualizeNetworksController.class) {
                            if (newState == Worker.State.SUCCEEDED) {
                                JSObject win = (JSObject) webEngineStudent.executeScript("window");

                                int retryCount = 0;
                                retryExpert:
                                try {

                                    Graph graph = gson.fromJson(context.getStudentExpertGraph().getStudentGraphData(), Graph.class);

                                    UserTermsAlignment termsAlignment = new UserTermsAlignment();
                                    termsAlignment.setAssignmentId(context.getCurrentAssignment().getId());

                                    termsAlignment.setExpertMapId(context.getSelectedExpertMap().getFileId());
                                    termsAlignment.setStudentMapId(context.getSelectedStudentMap().getFileId());

                                    String response = HTTPUtilities.sendPostRequest(AppConstants.GET_ALIGNMENT, gson.toJson(termsAlignment), 20000, 20000);

                                    Map<String, String> alignment = Utils.convert(response);

                                    Collection<VertexDetail> nodes = graph.getNodes();
                                    for (VertexDetail node : nodes) {
                                        if (alignment.containsKey(node.getName())) {
                                            node.setColor("#21ff9d");
                                        } else {
                                            node.setColor("#e8ff1f");
                                        }
                                    }

                                    webEngineStudent.executeScript("loadStudentData(" + gson.toJson(graph) + ")");
                                    win.setMember("app", new JavaApp());

//                                webEngineStudent.executeScript("loadStudentData(" + context.getStudentExpertGraph().getStudentGraphData() + ")");
//                                win.setMember("app", new JavaApp());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (retryCount < 3) {
                                        retryCount++;
                                        break retryExpert;
                                    }
                                }

                            }
                        }
//                        enableFirebug(webEngineStudent);
                    });

//            progressNetworkLoad.progressProperty().bind(webEngineStudent.getLoadWorker().progressProperty());
//            progressNetworkLoad.progressProperty().bind(webEngineExpert.getLoadWorker().progressProperty());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private static void enableFirebug(final WebEngine engine) {
        engine.executeScript("if (!document.getElementById('FirebugLite')){E = document['createElement' + 'NS'] && document.documentElement.namespaceURI;E = E ? document['createElement' + 'NS'](E, 'script') : document['createElement']('script');E['setAttribute']('id', 'FirebugLite');E['setAttribute']('src', 'https://getfirebug.com/' + 'firebug-lite.js' + '#startOpened');E['setAttribute']('FirebugLite', '4');(document['getElementsByTagName']('head')[0] || document['getElementsByTagName']('body')[0]).appendChild(E);E = new Image;E['setAttribute']('src', 'https://getfirebug.com/' + '#startOpened');}");
    }

    @FXML
    private ProgressIndicator progressNetworkLoad;

    public class JavaApp {

        public String getStudentData() {
            return context.getStudentGraph();
        }

        public String getExpertData() {
            return context.getExpertGraph();
        }
    }
}
