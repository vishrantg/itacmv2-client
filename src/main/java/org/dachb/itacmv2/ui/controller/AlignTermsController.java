/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.graph.GraphType;
import org.dachb.itacmv2.model.AlignTermsModel;
import org.dachb.itacmv2.model.ExpertTerms;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class AlignTermsController implements Initializable {

    @FXML
    private TableView<AlignTermsModel> alignTermsTableView;
    @FXML
    private TableColumn<AlignTermsModel, Boolean> checkBoxColumn;
    @FXML
    private TableColumn<AlignTermsModel, String> colStudentTerms;
    @FXML
    private TableColumn<AlignTermsModel, ExpertTerms> colExpertTerms;
    @FXML
    private TableColumn<AlignTermsModel, AlignTermsModel> colRecommendation;
    @FXML
    private WebView studentNodeConnection;

    @FXML
    private TextField txtSearchBox;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        alignTermsTableView.setEditable(true);
        colStudentTerms.setCellValueFactory(new PropertyValueFactory<>("studentTerm"));
        colStudentTerms.setCellFactory(TextFieldTableCell.<AlignTermsModel>forTableColumn());

        ObservableList<ExpertTerms> genderList = FXCollections.observableArrayList(//
                ExpertTerms.values());

        colExpertTerms.setCellFactory(ComboBoxTableCell.forTableColumn(genderList));

        colExpertTerms.setCellValueFactory(new PropertyValueFactory<>("expertTerm"));

        checkBoxColumn.setCellValueFactory(new Callback<CellDataFeatures<AlignTermsModel, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(CellDataFeatures<AlignTermsModel, Boolean> param) {

                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(/*person.isSingle()*/);

                booleanProp.addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                            Boolean newValue) {

                    }
                });
                return booleanProp;
            }
        });

        checkBoxColumn.setCellFactory(new Callback<TableColumn<AlignTermsModel, Boolean>, //
                TableCell<AlignTermsModel, Boolean>>() {
            @Override
            public TableCell<AlignTermsModel, Boolean> call(TableColumn<AlignTermsModel, Boolean> p) {
                CheckBoxTableCell<AlignTermsModel, Boolean> cell = new CheckBoxTableCell<AlignTermsModel, Boolean>();
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });

        // 1. Wrap the ObservableList in a FilteredList (initially display all data).
        FilteredList<AlignTermsModel> filteredData = new FilteredList<>(graphStudentVerticesTerms, p -> true);

        txtSearchBox.textProperty().addListener((observable, oldValue, newValue) -> {

            filteredData.setPredicate(person -> {

                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (person.getExpertTerm().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (person.getStudentTerm().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<AlignTermsModel> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(alignTermsTableView.comparatorProperty());

        alignTermsTableView.setItems(sortedData);

        context.setLoadedControllers(AppConstants.ALIGN_TERMS, this);
    }

    private final ObservableList<AlignTermsModel> graphStudentVerticesTerms = FXCollections.observableArrayList();
    private final ObservableList<AlignTermsModel> graphExpertVerticesTerms = FXCollections.observableArrayList();

    public void updateTermsList(List<AlignTermsModel> terms, GraphType graphType) {

        if (GraphType.EXPERT == graphType) {
            graphExpertVerticesTerms.clear();

        }

        if (GraphType.STUDENT == graphType) {
            this.graphStudentVerticesTerms.clear();

            terms.forEach((term) -> {
                graphStudentVerticesTerms.add(term);
            });

        }

    }

    private final ApplicationContext context = ApplicationContext.getInstance();

}
