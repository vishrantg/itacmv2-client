/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import com.google.gson.Gson;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.util.DiscreteDistribution;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationModel;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.Axis;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.dachb.itacmv2.commons.ErrorUtilities;
import org.dachb.itacmv2.commons.MessageType;
import org.dachb.itacmv2.commons.Utils;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.constants.AppConstants.GraphDistanceMethods;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.graph.GraphReader;
import org.dachb.itacmv2.graph.GraphType;
import org.dachb.itacmv2.graph.algorithm.GraphKernel;
import org.dachb.itacmv2.graph.algorithm.ged.algorithms.GraphMatching;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.Graph;
import org.dachb.itacmv2.graph.datastructure.StudentExpertGraph;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexDetail;
import org.dachb.itacmv2.graph.improvement.GraphUtils;
import org.dachb.itacmv2.graph.improvement.JavaFXArrow;
import org.dachb.itacmv2.graph.visualization.AlignTerms;
import org.dachb.itacmv2.json.JSONArray;
import org.dachb.itacmv2.json.JSONObject;
import org.dachb.itacmv2.json.JSONUtil;
import org.dachb.itacmv2.model.Alignment;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.model.AssignmentFile;
import org.dachb.itacmv2.model.ClusteredStudentDetail;
import org.dachb.itacmv2.model.ImproveStudentGraphModel;
import org.dachb.itacmv2.model.Message;
import org.dachb.itacmv2.model.UserTermsAlignment;
import org.dachb.itacmv2.services.AlignmentRetrievalService;
import org.dachb.itacmv2.services.AlignmentSubmissionService;
import org.dachb.itacmv2.services.AssignmentDetailsService;
import org.dachb.itacmv2.services.CalculateGED;
import org.dachb.itacmv2.services.DeleteAssignmentMapService;
import org.dachb.itacmv2.services.DownloadFileService;
import org.dachb.itacmv2.services.MapContentService;
import org.dachb.itacmv2.services.UploadFileService;
import org.dachb.itacmv2.ui.custom.DialogBox;
import org.dachb.itacmv2.ui.custom.ImageButton;
/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class CompareNetworkController implements Initializable {

    @FXML
    private Label lblAssignmentName;

    @FXML
    private Label lblGED;

    @FXML
    private Label DUMMY_LABEL;

    @FXML
    private Button btnStudentAddMap;

    @FXML
    private TableColumn<AssignmentFile, AssignmentFile> tblStudentColMaps;

    @FXML
    private TableColumn<AssignmentFile, AssignmentFile> tblStudentColTrash;

    @FXML
    private BorderPane wrapBoderPaneCompareNetwork;

    @FXML
    private Button btnExpertAddMap;

    @FXML
    private TableColumn<AssignmentFile, AssignmentFile> tblExpertColMaps;

    @FXML
    private TableColumn<AssignmentFile, AssignmentFile> tblExpertColTrash;

    private final ApplicationContext context = ApplicationContext.getInstance();

    @FXML
    private Label btnHome;

    @FXML
    private Label lblStudentMapName;

    @FXML
    private Label lblExpertMapName;

    @FXML
    private Label lblGraphStudentMapName;

    @FXML
    private Label lblGraphExpertMapName;

    @FXML
    private TableView<AssignmentFile> tblStudentMap;

    @FXML
    private TableView<AssignmentFile> tblExpertMap;

    @FXML
    private ProgressIndicator progressIndicator;

    private Assignment assignment = null;

    private AssignmentFile selectedStudentMap;
    private AssignmentFile selectedExpertMap;

    private Gson gson = new Gson();

    private AlignmentSubmissionService submissionService;

    @FXML
    private TitledPane titledPaneStudentMap;

    @FXML
    private TitledPane titledPaneExpertMap;

    @FXML
    private TabPane tabNetowrkComparision;

    @FXML
    private ScrollPane scrollPaneAlignTerms;

    @FXML
    private ScrollPane scrollNetworkVisualize;

    @FXML
    private BarChart<String, Number> studentGraphKernelBarChart;

    @FXML
    private BarChart<String, Number> expertGraphKernelBarChart;

    @FXML
    private CategoryAxis studentGraphKernelXAsis;

    @FXML
    private CategoryAxis expertGraphKernelXAsis;

    @FXML
    private NumberAxis studentGraphKernelYAsis;

    @FXML
    private NumberAxis expertGraphKernelYAsis;

    @FXML
    private Label lblCosineSimilarity;

    @FXML
    private Label lblCycleLength;

    @FXML
    private Slider sliderMatchLoops;

    @FXML
    private Slider sliderDisjointPath;

    private static final int CIRCLE_SIZE = 10; // default circle size
    @FXML
    private AnchorPane studentGraph;
    @FXML
    private TableView<ImproveStudentGraphModel> improvementTable;

    @FXML
    private TableColumn<?, ?> actionColumn;
    @FXML
    private TableColumn<?, ?> startNodeColumn;
    @FXML
    private TableColumn<?, ?> endNodeColumn;
    @FXML
    private TableColumn<ImproveStudentGraphModel, ImproveStudentGraphModel> reasonColumn;
    @FXML
    private Tab tabAlignTerms;
    @FXML
    private Tab tabVisualizeNetwork;
    @FXML
    private Tab tabCompareMaps;
    @FXML
    private Tab tabImproveStudentMap;
    @FXML
    private Tab tabClusterStudents;

    @FXML
    private AnchorPane clusterGraph;
    @FXML
    private ComboBox<GraphDistanceMethods> comboSelectMethod;
    @FXML
    private TextField txtThreshold;
    @FXML
    private Slider sliderThreshold;
    @FXML
    private AreaChart<String, Number> barChartStudentDistribution;
    @FXML
    private Button btnClusterStudent;

    final ObservableList<AssignmentFile> studentMapsData = FXCollections.observableArrayList();
    final ObservableList<AssignmentFile> expertMapsData = FXCollections.observableArrayList();
    final ObservableList<AssignmentFile> allMapsData = FXCollections.observableArrayList();
    @FXML
    private NumberAxis clusteredStudentYAsis;
    @FXML
    private CategoryAxis clusteredStudentXAsis;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // assignment = (Assignment) context.getUserData();
        assignment = context.getCurrentAssignment();

        lblAssignmentName.setText("Assignment name: " + assignment.getAssignmentName());

        tblStudentColMaps.setCellValueFactory(
                new PropertyValueFactory<>("fileName"));
        tblExpertColMaps.setCellValueFactory(
                new PropertyValueFactory<>("fileName"));

        tblStudentMap.setPlaceholder(new Label("No map is added"));
        tblExpertMap.setPlaceholder(new Label("No map is added"));

        tblStudentMap.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                try {

                    selectedStudentMap = newSelection;
                    context.setSelectedStudentMap(selectedStudentMap);

                    MapContentService mapContentService = new MapContentService(newSelection, AppConstants.DOWNLOAD_MAP_LOCATION);

                    String graphData = mapContentService.getMapContent();

                    Graph graph = gson.fromJson(graphData, Graph.class);
                    context.getStudentExpertGraph().setStudentGraph(graph);
                    context.getStudentExpertGraph().setStudentGraphData(graphData);
                    context.getStudentExpertGraph().setSelectedStudentGraphName(newSelection.getFileName());

                    context.getLblStudentNetwork().setText("Student map: " + newSelection.getFileName());

                    // setTabAlignTerm();
                    if ("tabVisualizeNetwork".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
                        context.getStudentMapWebEngine().reload();
//                        context.getExpertMapWebEngine().reload();
                    }

                    if ("tabAlignTerms".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {

                        setTabAlignTerm();

                    }

                    if ("tabCompareMaps".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
                        lblGraphStudentMapName.setText("Student map: " + newSelection.getFileName());
                        computeGraphMatrics(true, false);
                    }

                    if ("tabImproveStudentMap".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
//                        refreshImproveGraphTable();

                        initializeStudentImprovementTab();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        tblExpertMap.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                try {

                    MapContentService mapContentService = new MapContentService(newSelection, AppConstants.DOWNLOAD_MAP_LOCATION);

                    String graphData = mapContentService.getMapContent();

                    Graph graph = gson.fromJson(graphData, Graph.class);
                    context.getStudentExpertGraph().setExpertGraph(graph);
                    context.getStudentExpertGraph().setExpertGraphData(graphData);
                    context.getStudentExpertGraph().setSelectedExpertGraphName(newSelection.getFileName());

                    selectedExpertMap = newSelection;
                    context.setSelectedExpertMap(selectedExpertMap);

                    context.getLblExpertNetwork().setText("Expert map: " + newSelection.getFileName());

                    if ("tabVisualizeNetwork".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
                        context.getExpertMapWebEngine().reload();
                    } else // setTabAlignTerm();
                    if ("tabAlignTerms".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {

                        setTabAlignTerm();

                    } else if ("tabCompareMaps".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
                        lblGraphExpertMapName.setText("Expert map: " + newSelection.getFileName());
                        computeGraphMatrics(false, true);
                    } else if ("tabImproveStudentMap".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
//                        refreshImproveGraphTable();
                        initializeStudentImprovementTab();
                    }
                } catch (Exception ex) {
                    Logger.getLogger(CompareNetworkController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        final AssignmentDetailsService assignmentDetailsService = new AssignmentDetailsService(assignment.getId());
        assignmentDetailsService.start();

        assignmentDetailsService.setOnSucceeded(success -> {
            String response = assignmentDetailsService.getMessage();

            if (response != null && response.length() > 0) {
                
                JSONObject json = new JSONObject(response);

                try {

                    JSONArray array = json.getJSONArray("assignmentFiles");
                    for (int i = 0; i < array.length(); i++) {

                        try {
                            AssignmentFile mapDetails = (AssignmentFile) JSONUtil.convertToBean(array.getJSONObject(i), new AssignmentFile());

                            String graphType = mapDetails.getGraphType();
                            if (graphType != null) {
                                if (GraphType.EXPERT.getType().equalsIgnoreCase(graphType)) {
                                    expertMapsData.add(mapDetails);
                                } else if (GraphType.STUDENT.getType().equalsIgnoreCase(graphType)) {
                                    studentMapsData.add(mapDetails);
                                } else {
                                    System.out.println("Unknown type map " + mapDetails);
                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    allMapsData.addAll(expertMapsData);
                    allMapsData.addAll(studentMapsData);

                    DownloadFileService downloadFileService = new DownloadFileService(allMapsData, AppConstants.DOWNLOAD_MAP_LOCATION, assignment);

                    downloadFileService.start();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        assignmentDetailsService.setOnFailed(failed -> {

            if (assignmentDetailsService.getException().getClass().equals(IOException.class)) {
                Message message = new Message();
                message.setType(MessageType.ERROR);
                message.setMessage("Session timeout please login again");

                context.setMessage(message);
                context.getAppStage(AppConstants.LOGIN).setMaximized(false); // .setUserData(response);
            }

        });

        btnStudentAddMap.setOnAction(event -> {

            FileChooser fc = new FileChooser();
            fc.setTitle("Open File");

            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("COGGLE FILE (*.csv)", "*.csv");
            fc.getExtensionFilters().add(extFilter);

            List<File> files = fc.showOpenMultipleDialog(new Stage());

            addNetowrkToList(files, studentMapsData, GraphType.STUDENT);
        });

        btnExpertAddMap.setOnAction(event -> {

            if (expertMapsData.isEmpty()) {

                FileChooser fc = new FileChooser();
                fc.setTitle("Open File");
                File file = fc.showOpenDialog(new Stage());

                if (file != null) {

                    List<File> files = new ArrayList<>();
                    files.add(file);

                    addNetowrkToList(files, expertMapsData, GraphType.EXPERT);

                }
            } else {
                DialogBox dialogBox = new DialogBox("Error", "Expert map already selected, remove it first", Alert.AlertType.ERROR);
                dialogBox.show();
            }
        });

        tblStudentColTrash.setCellValueFactory(param -> new ReadOnlyObjectWrapper<AssignmentFile>(param.getValue()));

        tblStudentColTrash.setCellFactory(param -> new TableCell<AssignmentFile, AssignmentFile>() {

            private final ImageButton btnTrash = new ImageButton(AppConstants.TRASH_ICON, AppConstants.TRASH_ICON);

            @Override
            protected void updateItem(AssignmentFile mapDetails, boolean empty) {
                super.updateItem(mapDetails, empty);

                if (mapDetails == null) {
                    setGraphic(null);
                    return;
                }

                btnTrash.setStyle("-fx-padding: 0 0 0 0; -fx-background-color:  transparent;");

                btnTrash.setOnAction(studentTrashEvent -> {

                    DeleteAssignmentMapService deleteService = new DeleteAssignmentMapService(mapDetails);
                    deleteService.start();

                    deleteService.setOnSucceeded(success -> {
                        studentMapsData.remove(mapDetails);
                    });

                    deleteService.setOnFailed(failed -> {
                        DialogBox dialogBox = new DialogBox("Error", "Failed to delete map, please try again", Alert.AlertType.ERROR);
                        dialogBox.show();
                    });
                });

                setGraphic(btnTrash);
            }
        });

        tblExpertColTrash.setCellValueFactory(param -> new ReadOnlyObjectWrapper<AssignmentFile>(param.getValue()));

        tblExpertColTrash.setCellFactory(param -> new TableCell<AssignmentFile, AssignmentFile>() {

            private final ImageButton btnTrash = new ImageButton(AppConstants.TRASH_ICON, AppConstants.TRASH_ICON);

            @Override
            protected void updateItem(AssignmentFile mapDetails, boolean empty) {
                super.updateItem(mapDetails, empty);

                if (mapDetails == null) {
                    setGraphic(null);
                    return;
                }

                btnTrash.setStyle("-fx-padding: 0 0 0 0; -fx-background-color:  transparent;");

                setGraphic(btnTrash);

                btnTrash.setOnAction(expertTrashEvent -> {

                    DeleteAssignmentMapService deleteService = new DeleteAssignmentMapService(mapDetails);

                    deleteService.start();

                    deleteService.setOnSucceeded(success -> {
                        expertMapsData.remove(mapDetails);
                    });

                    deleteService.setOnFailed(failed -> {
                        DialogBox dialogBox = new DialogBox("Error", "Failed to delete map, please try again", Alert.AlertType.ERROR);
                        dialogBox.show();
                    });
                });

            }
        });

        btnHome.setOnMouseClicked(mouseHomeClickEvent -> {
            context.getAppStage(AppConstants.WELCOME_SCREEN);
            context.clearStudentExpertGraph();
        });

        tblStudentMap.setItems(studentMapsData);
        tblExpertMap.setItems(expertMapsData);

        double height = context.getAppStage().getHeight() - 200;

        if (height > 200) {
            titledPaneStudentMap.setPrefHeight(height);
        } else {
            titledPaneStudentMap.setMinHeight(200);
        }
        context.getAppStage().heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

                double height = (newValue.doubleValue() - titledPaneExpertMap.getHeight());

                if (height > 200) {
                    titledPaneStudentMap.setPrefHeight(height);
                }
            }
        });

        tabNetowrkComparision.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
            Node content;

            studentGraph.getChildren().clear();
            clusterGraph.getChildren().clear();
            viz.getChildren().clear();

            graph = null;

            if ("tabVisualizeNetwork".equalsIgnoreCase(newTab.getId())) {

                content = context.loadResource(AppConstants.NETWORK_VISUALIZE);
                context.getLblExpertNetwork().setText("Expert map: " + context.getStudentExpertGraph().getSelectedExpertGraphName());
                context.getLblStudentNetwork().setText("Student map: " + context.getStudentExpertGraph().getSelectedStudentGraphName());

                scrollNetworkVisualize.setContent(content);

            } else if ("tabAlignTerms".equalsIgnoreCase(newTab.getId())) {

                setTabAlignTerm();

            } else if ("tabCompareMaps".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
                lblGraphStudentMapName.setText("Student map: " + context.getStudentExpertGraph().getSelectedStudentGraphName());
                lblGraphExpertMapName.setText("Expert map: " + context.getStudentExpertGraph().getSelectedExpertGraphName());
                computeGraphMatrics(true, true);
            } else if ("tabImproveStudentMap".equalsIgnoreCase(tabNetowrkComparision.getSelectionModel().getSelectedItem().getId())) {
//                refreshImproveGraphTable();
                initializeStudentImprovementTab();
            }

        });

        setTabAlignTerm();

        context.setLoadedControllers(AppConstants.COMPARE_NETWORK, this);

        context.setLblExpertNetwork(this.lblExpertMapName);
        context.setLblStudentNetwork(this.lblStudentMapName);

        actionColumn.setCellValueFactory(
                new PropertyValueFactory<>("action"));
        startNodeColumn.setCellValueFactory(
                new PropertyValueFactory<>("startNode"));
        endNodeColumn.setCellValueFactory(
                new PropertyValueFactory<>("endNode"));

        reasonColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<ImproveStudentGraphModel>(param.getValue()));

        reasonColumn.setStyle("-fx-table-cell-border-color: transparent");

//        reasonColumn.
        reasonColumn.setCellFactory(param -> new TableCell<ImproveStudentGraphModel, ImproveStudentGraphModel>() {

            @Override
            protected void updateItem(ImproveStudentGraphModel improveStudentGraphModel, boolean empty) {

                setGraphic(DUMMY_LABEL);

                super.updateItem(improveStudentGraphModel, empty);
                if (improveStudentGraphModel == null || improveStudentGraphModel.getReasons() == null || improveStudentGraphModel.getReasons().isEmpty()) {
                    setText("");
                } else {

                    GridPane g = new GridPane();

                    int count = 1;

                    for (String reason : improveStudentGraphModel.getReasons()) {

                        Hyperlink link = new Hyperlink();
                        link.setText("R" + count);

                        Tooltip tip = new Tooltip(reason);

                        link.setTooltip(tip);
                        link.setOnAction((ActionEvent e) -> {

                            DialogBox dialogBox = new DialogBox("Reason", reason, AlertType.INFORMATION);
                            dialogBox.show();

                        });

                        GridPane.setMargin(link, new javafx.geometry.Insets(0, 2, 0, 2));
                        GridPane.setColumnIndex(link, (count - 1));

                        g.getChildren().add(link);
                        count++;

                    }

                    setGraphic(g);

//                        setText(improveStudentGraphModel.getReason().get(0));
                }

            }
        });

        improvementTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newImproveStudentGraphModelSelection) -> {
            takeAction(newImproveStudentGraphModelSelection);
        });

        improvementTable.setRowFactory(new Callback<TableView<ImproveStudentGraphModel>, TableRow<ImproveStudentGraphModel>>() {
            @Override
            public TableRow<ImproveStudentGraphModel> call(TableView<ImproveStudentGraphModel> tableView2) {
                final TableRow<ImproveStudentGraphModel> row = new TableRow<>();
                row.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < improvementTable.getItems().size() && improvementTable.getSelectionModel().isSelected(index)) {
                            improvementTable.getSelectionModel().clearSelection();
                            event.consume();
                        }
                    }
                });

                takeAction(null);

                return row;
            }
        });

        improvementTable.setItems(improvementData);

        improvementTable.setPlaceholder(new Label("No suggestions"));

        Stage s = context.getAppStage();

        ChangeListener<Number> sliderValueChanged = ((observable, oldValue, newValue)
                -> {
            if (newValue != null && oldValue != null && oldValue.intValue() != newValue.intValue()) {
                refreshImproveGraphTable();
            }
        });

        sliderMatchLoops.valueProperty().addListener(sliderValueChanged);
        sliderDisjointPath.valueProperty().addListener(sliderValueChanged);

        ChangeListener<Number> stageSizeListener = ((observable, oldValue, newValue)
                -> {
            if (oldValue != newValue) {
                initializeGraphLayout(studentGraph, true);
            }
        });

        s.widthProperty().addListener(stageSizeListener);
        s.heightProperty().addListener(stageSizeListener);

        comboSelectMethod.getItems().clear();

        comboSelectMethod.getItems().addAll(AppConstants.GRAPH_METHODS);
        comboSelectMethod.getSelectionModel().selectFirst();

        Bindings.bindBidirectional(txtThreshold.textProperty(), sliderThreshold.valueProperty(), new NumberStringConverter());

    }

    @FXML
    Label lblProgress;

    @FXML
    public void clusterStudentsAction() {

        studentGraph.getChildren().clear();
        clusterGraph.getChildren().clear();
        viz.getChildren().clear();

        Map<Double, Double> histo = new TreeMap<>();

//        List<Double> list = new ArrayList<>();
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                ConcurrentHashMap<edu.uci.ics.jung.graph.Graph<Vertex, Edge>, String> studentGraphs = new ConcurrentHashMap<>();

                // studentMapsData
                for (AssignmentFile studentMapData : studentMapsData) {

                    if (studentMapData.getJsonFileData() == null) {
                        continue;
                    }

                    edu.uci.ics.jung.graph.Graph<Vertex, Edge> g = gson.fromJson(studentMapData.getJsonFileData(), Graph.class).convertToJungGraph();

                    studentGraphs.put(g, studentMapData.getFileName());
                }

//        List<ClusteredStudentDetail> clusteredStudentDetails = new LinkedList<>();
                String thresholdStr = txtThreshold.getText();
                double threshold = Double.valueOf(thresholdStr);

//        graph = new JGraph<>();
                graph = new UndirectedSparseGraph<>();

                int vertexId = 0;

                Map<edu.uci.ics.jung.graph.Graph<Vertex, Edge>, Vertex> map = new HashMap<>();
                Set<Map.Entry<edu.uci.ics.jung.graph.Graph<Vertex, Edge>, String>> entrys = studentGraphs.entrySet();

                int i = 0;

                for (Map.Entry<edu.uci.ics.jung.graph.Graph<Vertex, Edge>, String> entry : entrys) {

                    for (Map.Entry<edu.uci.ics.jung.graph.Graph<Vertex, Edge>, String> comparedWith : entrys) {

                        if (entry.getKey() != comparedWith.getKey()) {
                            try {

                                Double distance = null;

                                switch (comboSelectMethod.getValue()) {

                                    case GED:
                                        GraphMatching graphMatching = new GraphMatching(entry.getKey(), comparedWith.getKey());
                                        distance = graphMatching.calculateDistance(AppConstants.GED_PROPERTIES);
                                        break;
                                    case GRAPH_KERNEL:

                                        distance = computeGraphKernel(entry.getKey(), comparedWith.getKey());

                                        break;
                                    case GRAPH_EMBEDDING:

                                        distance = graphCosineSimilarityForJungGraph(entry.getKey(), comparedWith.getKey());

                                        break;
                                    default:
                                        return null;

                                }

                                ClusteredStudentDetail clusteredStudentDetail = new ClusteredStudentDetail();

                                clusteredStudentDetail.setG1(entry.getKey());
                                clusteredStudentDetail.setG2(comparedWith.getKey());

                                clusteredStudentDetail.setDistance(distance);
                                clusteredStudentDetail.setMethod(GraphDistanceMethods.GED.getMethod());

                                Vertex v1;

                                if (map.get(comparedWith.getKey()) == null) {
                                    v1 = new Vertex(vertexId++);
                                    v1.setFullName(comparedWith.getValue());

                                    graph.addVertex(v1);

                                    map.put(comparedWith.getKey(), v1);
                                } else {
                                    v1 = map.get(comparedWith.getKey());
                                }

                                Vertex v2;

                                if (map.get(entry.getKey()) == null) {
                                    v2 = new Vertex(vertexId++);
                                    v2.setFullName(entry.getValue());

                                    graph.addVertex(v2);

                                    map.put(entry.getKey(), v2);
                                } else {
                                    v2 = map.get(entry.getKey());
                                }

                                if (Double.compare(distance, threshold) < 1) {
                                    Edge e = new Edge(vertexId * i++);

                                    e.setValue(distance.toString());

                                    List<String> l = new ArrayList();
                                    l.add(String.format("%.2f", distance));

                                    e.setReason(l);

                                    graph.addEdge(e, v1, v2);
                                }

//                                list.add(distance);
                                double d = Double.valueOf(String.format("%.2f", distance));

                                if (histo.get(d) == null) {
                                    histo.put(d, 1d);
                                } else {
                                    histo.put(d, histo.get(d) + 1);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                    }

                    entrys.remove(entry);
                }

                return null;
            }
        };

        task.setOnSucceeded(taskFinishEvent -> {

//            Collections.sort(list);
//            System.out.println("list -> " + list);
//            List<List<Double>> data = splitList(list);
//            for (List<Double> m : data) {
////            histo.put(Double.NaN, Double.NaN)
//                System.out.println("M -> " + m);
//            }
            System.out.println("histo " + histo);

            populateContineousBarChart(barChartStudentDistribution, clusteredStudentXAsis, clusteredStudentYAsis, "", "Distance", "Pairs", histo);
            initializeClusteredGraphLayout(clusterGraph, false);
        });

        new Thread(task).start();

        wrapBoderPaneCompareNetwork.disableProperty().bind(task.runningProperty());
        progressIndicator.visibleProperty().bind(task.runningProperty());

    }

    private List<List<Double>> splitList(List<Double> originalList) {
        int partitionSize = originalList.size() / 10;
        List<List<Double>> partitions = new LinkedList<List<Double>>();
        for (int i = 0; i < originalList.size(); i += partitionSize) {
            partitions.add(originalList.subList(i,
                    Math.min(i + partitionSize, originalList.size())));
        }

        return partitions;
    }

    private void setTabAlignTerm() {
        Node content;

        final SwingNode swingNode = new SwingNode();

        createSwingContent(swingNode, context.getStudentExpertGraph());
        StackPane pane = new StackPane();

        double height = 700;

        try {
            height = Math.max(context.getStudentExpertGraph().getExpertGraph().getNodes().size(), context.getStudentExpertGraph().getStudentGraph().getNodes().size()) * 50;
        } catch (Exception e) {
        }
        pane.setMinHeight(height > 1000 ? height : 400);
        pane.setMinWidth(200);
        pane.getChildren().add(swingNode);
        content = swingNode.getParent();
        scrollPaneAlignTerms.setContent(content);
    }

    private void createSwingContent(final SwingNode swingNode, StudentExpertGraph studentExpertGraph) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                if (studentExpertGraph.getStudentGraph() != null && studentExpertGraph.getExpertGraph() != null && studentExpertGraph.getExpertGraph().getNodes() != null && studentExpertGraph.getStudentGraph().getNodes() != null) {
                    JPanel jPanel = new AlignTerms(studentExpertGraph.getStudentGraph().getNodes(), studentExpertGraph.getExpertGraph().getNodes()).getViewPanel();
                    Platform.runLater(() -> {

                        context.setExpertMapName(studentExpertGraph.getSelectedExpertGraphName());
                        context.setStudentMapName(studentExpertGraph.getSelectedStudentGraphName());

                        lblStudentMapName.setText("Student map: " + studentExpertGraph.getSelectedStudentGraphName());
                        lblExpertMapName.setText("Expert map: " + studentExpertGraph.getSelectedExpertGraphName());
                    });
                    swingNode.setContent(jPanel);
                } else {
                    JPanel panel = new JPanel();
                    JLabel label = new JLabel("Please select student and expert map to align terms.");

                    panel.setLayout(new GridBagLayout());
                    panel.add(label);
                    panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

                    swingNode.setContent(panel);

                }

            }
        });
    }

    private Collection<VertexDetail> extractVerticesName(AssignmentFile newSelection) throws Exception {

        MapContentService mapContentService = new MapContentService(newSelection, AppConstants.DOWNLOAD_MAP_LOCATION);

        String graphData = mapContentService.getMapContent();

        Graph graph = gson.fromJson(graphData, Graph.class);
        Collection<VertexDetail> nodes = graph.getNodes();

        return nodes;
    }

    private void addNetowrkToList(List<File> files, ObservableList<AssignmentFile> mapsData, GraphType graphType) {
        if (files != null) {

            // change List type File to AssignmentFile
            List<File> modifiableList = new ArrayList<>(files);
            List<File> duplicateFiles = new ArrayList<>();
            List<File> invalidFiles = new ArrayList<>();

            files.forEach(file -> {
                AssignmentFile mapDetails = new AssignmentFile();

                mapDetails.setFileName(file.getName());
                mapDetails.setGraphType(graphType.getType());
                try {
                    GraphReader gr = new GraphReader(file);

//                    ListOfEdges loe = new ListOfEdges();
//                    loe.readDataFromFile(file);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    invalidFiles.add(file);
                    return;
                }

                if (mapsData.contains(mapDetails)) {
                    duplicateFiles.add(file);
                    return;
                }

            });

            if (invalidFiles.size() > 0) {
                modifiableList.removeAll(invalidFiles);
                DialogBox dialogBox = new DialogBox("Error", "Invalid file " + Arrays.toString(invalidFiles.toArray()), Alert.AlertType.ERROR);
                dialogBox.show();
            }

            if (duplicateFiles.size() > 0) {

                modifiableList.removeAll(duplicateFiles);

                DialogBox dialogBox = new DialogBox("Error", "File " + Arrays.toString(duplicateFiles.toArray()) + " already exists in list, if this file is different rename it and try to upload again.", Alert.AlertType.ERROR);
                dialogBox.show();
            }

            if (modifiableList.size() > 0) {

                final UploadFileService fileService = new UploadFileService(assignment, modifiableList, graphType);
                fileService.start();

                progressIndicator.visibleProperty().bind(fileService.runningProperty());
                wrapBoderPaneCompareNetwork.disableProperty().bind(fileService.runningProperty());

                fileService.setOnSucceeded(success -> {
                    modifiableList.forEach(file -> {
                        AssignmentFile mapDetails = new AssignmentFile();

                        mapDetails.setFileName(file.getName());
                        mapDetails.setGraphType(graphType.getType());

                        mapsData.add(mapDetails);
                    });

                    // REFRESHING THE SCREEN, TO LOAD THE RECENTLY UPLOADED FILE ID
                    context.getAppStage(AppConstants.COMPARE_NETWORK);

                });

                fileService.setOnFailed(failed -> {
                    DialogBox dialogBox = new DialogBox("Error", "Error while uploading file, please try again.", Alert.AlertType.ERROR);
                    dialogBox.show();
                });
            }
        }
    }

    @FXML
    private void submitTermAlignment() {

        Map<org.graphstream.graph.Node, org.graphstream.graph.Node> finalAlignment = context.getFinalAlignment();

        Set<Alignment> alignment = new HashSet<>();

        finalAlignment.forEach((studentTerm, expertTerm) -> {

            Alignment newAlignment = new Alignment(studentTerm.getId().replaceFirst("student-", ""), expertTerm.getId().replaceFirst("expert-", ""));
            alignment.add(newAlignment);

        });

        Set<String> terms = new HashSet<String>();

        finalAlignment.forEach((startNode, endNode) -> {
            terms.add(startNode.getId().replaceFirst("student-", ""));
            terms.add(endNode.getId().replaceFirst("expert-", ""));
        });

        if (alignment.size() > 0) {

            UserTermsAlignment termsAlignment = new UserTermsAlignment();
            termsAlignment.setAlignment(alignment);
            termsAlignment.setTerms(terms);
            termsAlignment.setAssignmentId(assignment.getId());
            termsAlignment.setStudentMapId(selectedStudentMap.getFileId());
            termsAlignment.setExpertMapId(selectedExpertMap.getFileId());

            submissionService = new AlignmentSubmissionService(termsAlignment);

            submissionService.start();

            progressIndicator.progressProperty().bind(submissionService.progressProperty());
            progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(submissionService.runningProperty()));

            wrapBoderPaneCompareNetwork.disableProperty().bind(submissionService.runningProperty());

            submissionService.setOnSucceeded((event) -> {
                String response = submissionService.getMessage();

                Message message = new Message();
                message.setMessage(response);

                MessageType type = MessageType.ERROR;
                if (response != null && !response.trim().isEmpty()) {
                    if (response.equalsIgnoreCase("success")) {

                        message.setType(MessageType.INFO);

                        response = "Alignment saved successfully.";
                    }
                } else {
                    response = "Error occured, please try again.";
                }

                message.setType(type);

                DialogBox dialogBox = new DialogBox("Info", response, Alert.AlertType.INFORMATION);
                dialogBox.show();

            });

            submissionService.setOnFailed((event) -> {
            });
        } else {
            Alert alert = new Alert(AlertType.INFORMATION, "No alignment changes made.", ButtonType.OK);
            alert.showAndWait();
        }

    }

    private void computeGraphMatrics(boolean student, boolean expert) {

        resetCalculations();

        StudentExpertGraph studentExpertGraph = context.getStudentExpertGraph();

        Graph expertGraph = studentExpertGraph.getExpertGraph();
        Graph studentGraph = studentExpertGraph.getStudentGraph();

        if (studentGraph == null || expertGraph == null) {
            return;
        }

//        SparseDoubleMatrix2D s = GraphMatrixOperations.graphToSparseMatrix(studentGraph.convertToJungGraph());
//        s.toArray();
//        	studentGraph.convertToJungGraph().
        double cosineSim = graphCosineSimilarity(studentGraph, expertGraph);

        lblCosineSimilarity.setText("The cosine similarity is: " + String.format("%.4f", cosineSim));

        lblCycleLength.setText("The distance is: ");

        String xLabel = "Length of cycle";
        String yLabel = "Numbers of cycles";

        GraphKernel studentGk = new GraphKernel(studentGraph.convertToJungGraph());
        List<List<String>> studentCycles = studentGk.getLoops();

        Map<Integer, Integer> sCycle = mapCycleNumber(studentCycles);

        if (student) {
            populateGraphKernelBarChart(studentGraphKernelBarChart, studentGraphKernelXAsis, studentGraphKernelYAsis, "Student Graph Kernel", xLabel, yLabel, sCycle);
        }

        GraphKernel expertGk = new GraphKernel(expertGraph.convertToJungGraph());
        List<List<String>> expertCycles = expertGk.getLoops();

        Map<Integer, Integer> eCycle = mapCycleNumber(expertCycles);

        if (expert) {
            populateGraphKernelBarChart(expertGraphKernelBarChart, expertGraphKernelXAsis, expertGraphKernelYAsis, "Expert Graph Kernel", xLabel, yLabel, eCycle);
        }

        double value = calculateHellingerDistance(sCycle, eCycle);

        lblCycleLength.setText("The distance is: " + String.format("%.4f", value));

    }

    private Double computeGraphKernel(edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph1, edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph2) {

        GraphKernel studentGk = new GraphKernel(graph1);
        List<List<String>> studentCycles = studentGk.getLoops();

        Map<Integer, Integer> sCycle = mapCycleNumber(studentCycles);

        GraphKernel expertGk = new GraphKernel(graph2);
        List<List<String>> expertCycles = expertGk.getLoops();

        Map<Integer, Integer> eCycle = mapCycleNumber(expertCycles);

        return calculateHellingerDistance(sCycle, eCycle);

    }

    private float getGraphDensity(Graph graph) {
        return (2 * getNumberOfGraphEdges(graph)) / (getNumberOfGraphNodes(graph) * (getNumberOfGraphNodes(graph) - 1));
    }

    private float getNumberOfGraphEdges(Graph graph) {
        return (graph != null && graph.getEdges() != null) ? graph.getEdges().size() : 0;
    }

    private float getNumberOfGraphNodes(Graph graph) {
        return (graph != null && graph.getNodes() != null) ? graph.getNodes().size() : 0;
    }

    private double graphCosineSimilarityForJungGraph(edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph1, edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph2) {

        Graph g1 = new Graph();
        g1.convertFromJungGraph(graph1);

        Graph g2 = new Graph();
        g2.convertFromJungGraph(graph2);

        return graphCosineSimilarity(g1, g2);
    }

    private double graphCosineSimilarity(Graph graph1, Graph graph2) {

        double cosineSim = ((getGraphDensity(graph2) * getGraphDensity(graph1))
                + (getNumberOfGraphEdges(graph1) * getNumberOfGraphEdges(graph2))
                + (getNumberOfGraphNodes(graph2) * getNumberOfGraphNodes(graph1)))
                / ((Math.sqrt(Math.pow(getGraphDensity(graph2), 2) + Math.pow(getNumberOfGraphEdges(graph2), 2) + Math.pow(getNumberOfGraphNodes(graph2), 2)))
                * (Math.sqrt(Math.pow(getGraphDensity(graph1), 2) + Math.pow(getNumberOfGraphEdges(graph1), 2) + Math.pow(getNumberOfGraphNodes(graph1), 2))));

        return cosineSim;
    }

    private double KullbackLeiblerDistribution(Map<Integer, Integer> sCycle, Map<Integer, Integer> eCycle) {

        int sMax = sCycle.keySet().size() > 0 ? Collections.max(sCycle.keySet()) : 0;
        int eMax = eCycle.keySet().size() > 0 ? Collections.max(eCycle.keySet()) : 0;

        double[] student = new double[Math.max(sMax, eMax) + 1];
        double[] expert = new double[Math.max(sMax, eMax) + 1];

        double studentSum = 0;
        for (Map.Entry<Integer, Integer> entry : sCycle.entrySet()) {
            studentSum += entry.getValue();
        }

        double expertSum = 0;
        for (Map.Entry<Integer, Integer> entry : eCycle.entrySet()) {
            expertSum += entry.getValue();
        }

        if (studentSum > 0) {
            for (Map.Entry<Integer, Integer> entry : sCycle.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();

                student[key] = value / studentSum;
            }
        }

        if (expertSum > 0) {
            for (Map.Entry<Integer, Integer> entry : eCycle.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();

                expert[key] = value / expertSum;
            }
        }

        double distance = 0;

        for (int i = 0; i < student.length; i++) {
            if (student[i] > 0 && expert[i] > 0) {
                distance += expert[i] * Math.log((expert[i] / student[i]));
            }
        }

        return DiscreteDistribution.KullbackLeibler(expert, student);
    }

    public double calculateHellingerDistance(Map<Integer, Integer> sCycle, Map<Integer, Integer> eCycle) {

        int sMax = sCycle.keySet().size() > 0 ? Collections.max(sCycle.keySet()) : 0;
        int eMax = eCycle.keySet().size() > 0 ? Collections.max(eCycle.keySet()) : 0;

        double[] student = new double[Math.max(sMax, eMax) + 1];
        double[] expert = new double[Math.max(sMax, eMax) + 1];

        double studentSum = 0;
        for (Map.Entry<Integer, Integer> entry : sCycle.entrySet()) {
            studentSum += entry.getValue();
        }

        double expertSum = 0;
        for (Map.Entry<Integer, Integer> entry : eCycle.entrySet()) {
            expertSum += entry.getValue();
        }

        if (studentSum > 0) {
            for (Map.Entry<Integer, Integer> entry : sCycle.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();

                student[key] = value / studentSum;
            }
        }

        if (expertSum > 0) {
            for (Map.Entry<Integer, Integer> entry : eCycle.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();

                expert[key] = value / expertSum;
            }
        }

        double distance = 0;

        for (int i = 0; i < student.length; i++) {
            distance += Math.pow(Math.sqrt(student[i]) - Math.sqrt(expert[i]), 2);
        }

        distance = (Math.sqrt(distance) / Math.sqrt(2));

        return distance;

    }

    private void displayLabelForData(XYChart.Data<String, Number> data) {
        final Node node = data.getNode();
        final Text dataText = new Text(data.getYValue() + "");

        if (node == null) {
            return;
        }

        node.parentProperty().addListener(new ChangeListener<Parent>() {
            @Override
            public void changed(ObservableValue<? extends Parent> ov, Parent oldParent, Parent parent) {
                Group parentGroup = (Group) parent;
                parentGroup.getChildren().add(dataText);
            }
        });

        node.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> ov, Bounds oldBounds, Bounds bounds) {
                dataText.setLayoutX(
                        Math.round(
                                bounds.getMinX() + bounds.getWidth() / 2 - dataText.prefWidth(-1) / 2
                        )
                );
                dataText.setLayoutY(
                        Math.round(
                                bounds.getMinY() - dataText.prefHeight(-1) * 0.5
                        )
                );
            }
        });
    }

    private void populateGraphKernelBarChart(XYChart<String, Number> barChart, Axis xAxis, Axis yAxis, String graphTitle, String xLabel, String yLabel, Map<Integer, Integer> cyclesMap) {
        barChart.setTitle(graphTitle);

        xAxis.setLabel(xLabel); //
        yAxis.setLabel(yLabel); //

        XYChart.Series<String, Number> data = new XYChart.Series();

        cyclesMap.entrySet().forEach((entry) -> {
            Integer key = entry.getKey();
            Integer value = entry.getValue();

            final XYChart.Data<String, Number> d = new XYChart.Data(key.toString(), value);
            data.nodeProperty().addListener(new ChangeListener<Node>() {
                @Override
                public void changed(ObservableValue<? extends Node> ov, Node oldNode, final Node node) {
                    if (node != null) {
                        setNodeStyle(d);
                        displayLabelForData(d);
                    }
                }
            });

            data.getData().add(d);
        });

        barChart.getData().clear();
        barChart.getData().add(data);
    }

    private void populateContineousBarChart(XYChart<String, Number> barChart, Axis xAxis, Axis yAxis, String graphTitle, String xLabel, String yLabel, Map<Double, Double> cyclesMap) {
        barChart.setTitle(graphTitle);

        xAxis.setLabel(xLabel); //
        yAxis.setLabel(yLabel); //

        XYChart.Series<String, Number> data = new XYChart.Series();

        cyclesMap.entrySet().forEach((entry) -> {
            Double key = entry.getKey();
            Double value = entry.getValue();

            final XYChart.Data<String, Number> d = new XYChart.Data(
                    String.format(
                            // comboSelectMethod.getValue() == GraphDistanceMethods.GED ? "%.2f" : 
                            "%.2f", key), Double.valueOf(String.format("%.0f", value)));
            data.nodeProperty().addListener(new ChangeListener<Node>() {
                @Override
                public void changed(ObservableValue<? extends Node> ov, Node oldNode, final Node node) {
                    if (node != null) {
                        setNodeStyle(d);
                        displayLabelForData(d);
                    }
                }
            });

            data.getData().add(d);
        });

        barChart.getData().clear();
        barChart.getData().add(data);
    }

    /**
     * Change color of bar if value of i is <5 then red, if >5 then green if i>8
     * then blue
     */
    private void setNodeStyle(XYChart.Data<String, Number> data) {
        Node node = data.getNode();
        if (node != null) {
            if (data.getYValue().intValue() > 8) {
                node.setStyle("-fx-bar-fill: -fx-exceeded;");
            } else if (data.getYValue().intValue() > 5) {
                node.setStyle("-fx-bar-fill: -fx-achieved;");
            } else {
                node.setStyle("-fx-bar-fill: -fx-not-achieved;");
            }
        }
    }

    private Map<Integer, Integer> mapCycleNumber(List<List<String>> expertCycles) {
        Map<Integer, Integer> cycles = new HashMap<>();
        expertCycles.forEach((expertCycle) -> {
            if (cycles.containsKey(expertCycle.size())) {
                cycles.put(expertCycle.size(), cycles.get(expertCycle.size()) + 1);
            } else {
                cycles.put(expertCycle.size(), 1);
            }
        });
        return cycles;
    }

    @FXML
    public void calculateGED() {

        UserTermsAlignment termsAlignment = new UserTermsAlignment();
        termsAlignment.setAssignmentId(assignment.getId());

        termsAlignment.setExpertMapId(context.getSelectedExpertMap().getFileId());
        termsAlignment.setStudentMapId(context.getSelectedStudentMap().getFileId());

        AlignmentRetrievalService retrievalService = new AlignmentRetrievalService(termsAlignment);

        progressIndicator.visibleProperty().bind(retrievalService.runningProperty());

        retrievalService.start();

        // wrapBoderPaneCompareNetwork.disableProperty().bind(gedService.runningProperty());
        // progressIndicator.visibleProperty().bind(retrievalService.runningProperty());
        final CalculateGED gedService = new CalculateGED();

        retrievalService.setOnSucceeded(success -> {
            String response = retrievalService.getMessage();

            Map<String, String> alignment = Utils.convert(retrievalService.getMessage());

            Graph sGraph = context.getStudentExpertGraph().getStudentGraph();
            sGraph = GraphUtils.preprocess(sGraph, alignment);

            Graph eGraph = context.getStudentExpertGraph().getExpertGraph();

            gedService.setExpertJungGraph(eGraph.convertToJungGraph());
            gedService.setStudentJungGraph(sGraph.convertToJungGraph());

            gedService.start();
            gedService.setOnSucceeded(successGED -> {
                lblGED.setText("The distance is: " + gedService.getValue());
            });

            tblStudentMap.disableProperty().bind(gedService.runningProperty());
            tblExpertMap.disableProperty().bind(gedService.runningProperty());

            progressIndicator.visibleProperty().bind(gedService.runningProperty());

        });

    }

    public void resetCalculations() {
        lblGED.setText("The distance is: -");
        lblCosineSimilarity.setText("The cosine similarity is: ");
        lblCycleLength.setText("The distance is: ");
    }

    final ObservableList<ImproveStudentGraphModel> improvementData = FXCollections.observableArrayList();

    public void takeAction(ImproveStudentGraphModel improveStudentGraphModel) {

        Collection<Edge> edges = graph.getEdges();
        Collection<Vertex> vertices = graph.getVertices();

        // resetting graph to original state on row change
        edges.forEach((edge) -> {
            edge.setEdgeColor(Color.BLACK);
        });
        vertices.forEach((vertice) -> {
            vertice.setVertexColor(Color.BLACK);
        });

        if (Objects.nonNull(improveStudentGraphModel)) {
            switch (improveStudentGraphModel.getAction()) {
                case ADD_EDGE:
                    Edge e = new Edge(graph.getEdgeCount() + 1);

                    // To implement addition of node
                    improveStudentGraphModel.getStartNode();
                    improveStudentGraphModel.getEndNode();

                    // graph1.addEdge(e, v, v1)
                    break;
                case DELETE_EDGE:

                    // Collection<Edge> edges = graph.getEdges();
                    for (Edge edge : edges) {
                        Vertex source = graph.getSource(edge);
                        Vertex target = graph.getDest(edge);

                        if (improveStudentGraphModel.getStartNode().equalsIgnoreCase(source.getFullName())
                                && improveStudentGraphModel.getEndNode().equalsIgnoreCase(target.getFullName())) {

                            edge.setEdgeColor(Color.RED);
                            edge.setReason(improveStudentGraphModel.getReasons());

                            break;
                        }

                    }

                    break;
                case ADD_NODE:
                    break;
                case DELETE_NODE:

                    for (Vertex vertice : vertices) {
                        if (vertice.getFullName().equalsIgnoreCase(improveStudentGraphModel.getStartNode())) {

                            vertice.setVertexColor(Color.RED);
                            vertice.setReason(improveStudentGraphModel.getReasons());

                            break;
                        }
                    }

                    break;
            }
        }

        renderGraph(true);

    }

    private edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph;
    private Layout<Vertex, Edge> circleLayout;
    private final Group viz = new Group();

    public void initializeGraphLayout(AnchorPane anchorPane, boolean showArrow) {

        if (graph == null || viz == null || anchorPane == null) {
            return;
        }

        anchorPane.getChildren().clear();
        anchorPane.getChildren().add(viz);

        circleLayout = new ISOMLayout<>(graph);
//        circleLayout = new CircleLayout<>(graph);

        VisualizationModel<Vertex, Edge> vm = new DefaultVisualizationModel<>(circleLayout, new Dimension((int) anchorPane.getWidth() - 100, (int) anchorPane.getHeight()));

        // draw the graph
        renderGraph(showArrow);

    }

    public void initializeClusteredGraphLayout(AnchorPane anchorPane, boolean showArrow) {

        if (graph == null || viz == null || anchorPane == null) {
            return;
        }

        anchorPane.getChildren().clear();
        anchorPane.getChildren().add(viz);

//        circleLayout = new ISOMLayout<>(graph);
//        circleLayout = new CircleLayout<>(graph);
        circleLayout = new FRLayout2<>(graph);

        VisualizationModel<Vertex, Edge> vm = new DefaultVisualizationModel<>(circleLayout, new Dimension((int) anchorPane.getWidth() - 100, (int) anchorPane.getHeight()));

        // draw the graph
        renderGraph(showArrow);

    }

    public void initializeStudentImprovementTab() {
        refreshImproveGraphTable();

        initializeGraphLayout(studentGraph, true);
    }

    public void refreshImproveGraphTable() {

        try {

            improvementData.removeAll(improvementData);

            if (context.getStudentExpertGraph() == null || context.getStudentExpertGraph().getStudentGraph() == null) {
                return;
            }

            graph = context.getStudentExpertGraph().getStudentGraph().convertToJungGraph();

            UserTermsAlignment termsAlignment = new UserTermsAlignment();
            termsAlignment.setAssignmentId(assignment.getId());

            termsAlignment.setExpertMapId(context.getSelectedExpertMap().getFileId());
            termsAlignment.setStudentMapId(context.getSelectedStudentMap().getFileId());

            AlignmentRetrievalService retrievalService = new AlignmentRetrievalService(termsAlignment);
            retrievalService.start();

            retrievalService.setOnSucceeded(success -> {
                String response = retrievalService.getMessage();

                Map<String, String> alignment = Utils.convert(retrievalService.getMessage());

                int maxLoop = (int) sliderMatchLoops.getValue(), maxDisJoint = (int) sliderDisjointPath.getValue();

                List<ImproveStudentGraphModel> improvements = GraphUtils.getImprovements(context.getStudentExpertGraph().getStudentGraph(), context.getStudentExpertGraph().getExpertGraph(), alignment, maxLoop, maxDisJoint);

                improvementData.clear();
                improvementData.addAll(improvements);
            });

//            initializeGraphLayout();
//                            improveStudentGraphModel.getStartNode();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Render a graph to a particular <code>Group</code>
     *
     * @param graph
     * @param layout
     * @param viz
     */
    private void renderGraph(boolean showArrow) {

        if (graph == null || viz == null || circleLayout == null) {
            return;
        }

        viz.getChildren().clear();

        // draw the edges
        for (Edge n : graph.getEdges()) {
            // get the end points of the edge
            Pair<Vertex> endpoints = graph.getEndpoints(n);

            // Get the end points as Point2D objects so we can use them in the 
            // builder
            Point2D pStart = circleLayout.transform(endpoints.getFirst());
            Point2D pEnd = circleLayout.transform(endpoints.getSecond());

            if (showArrow) {
                JavaFXArrow arrow = new JavaFXArrow(pStart.getX(), pStart.getY(), pEnd.getX(), pEnd.getY(), 10, n.getEdgeColor());
                viz.getChildren().add(arrow);
            } else {
                Line line = new Line(pStart.getX(), pStart.getY(), pEnd.getX(), pEnd.getY());

                double x = (pStart.getX() + pEnd.getX()) / 2;
                double y = (pStart.getY() + pEnd.getY()) / 2;

                viz.getChildren().add(line);
                viz.getChildren().add(addReasonClusteredGraph(n, x, y));
            }

            if (!n.getEdgeColor().equals(Color.BLACK)) {
                viz.getChildren().add(addReasonToEdge(n, pStart.getX(), pStart.getY(), "R"));
            }

//            Line line = new Line();
//            line.setStartX(pStart.getX());
//            line.setStartY(pStart.getY());
//            line.setEndX(pEnd.getX());
//            line.setEndY(pEnd.getY());
//            line.setStroke(n.getEdgeColor());
            // add the edges to the screen
        }

        // draw the vertices in the graph
        for (Vertex v : graph.getVertices()) {
            // Get the position of the vertex
            Point2D p = circleLayout.transform(v);

            // draw the vertex as a circle
//            Circle circle = CircleBuilder.create().fill(v.getVertexColor())
//                    .centerX(p.getX())
//                    .centerY(p.getY())
//                    .radius(CIRCLE_SIZE)
//                    .build();
//            Circle circle = new Circle(20);
            if (!v.getVertexColor().equals(Color.BLACK)) {

                GridPane g = new GridPane();
                g.relocate(p.getX(), p.getY());

                int count = 1;

                for (String reason : v.getReasons()) {

                    Hyperlink link = new Hyperlink();
                    link.setText("R" + count);

                    Tooltip tip = new Tooltip(reason);

                    link.setTooltip(tip);
                    link.setOnAction((ActionEvent e) -> {

                        DialogBox dialogBox = new DialogBox("Reason", reason, AlertType.INFORMATION);
                        dialogBox.show();

                    });

                    GridPane.setMargin(link, new javafx.geometry.Insets(0, 2, 0, 2));
                    GridPane.setColumnIndex(link, (count - 1));

                    g.getChildren().add(link);
                    count++;

                }

                viz.getChildren().add(g);

            }

            Text text = new Text(v.getFullName());
            text.setFill(v.getVertexColor());

            if (!v.getVertexColor().equals(Color.BLACK)) {
                text.setStrokeWidth(5);
//                text.
            }

            text.setBoundsType(TextBoundsType.VISUAL);
//            StackPane stack = new StackPane();
            text.setX(p.getX());
            text.setY(p.getY());

            // add it to the group, so it is shown on screen
            viz.getChildren().add(text);
        }

    }

    private GridPane addReasonToEdge(Edge n, double x, double y, String leading) {

        GridPane g = new GridPane();
        g.relocate(x, y);

        int count = 1;

        for (String reason : n.getReasons()) {

            Hyperlink link = new Hyperlink();
            link.setText(leading + count);

            Tooltip tip = new Tooltip(reason);

            link.setTooltip(tip);
            link.setOnAction((ActionEvent e) -> {

                DialogBox dialogBox = new DialogBox("Reason", reason, AlertType.INFORMATION);
                dialogBox.show();

            });

            GridPane.setMargin(link, new javafx.geometry.Insets(0, 2, 0, 2));
            GridPane.setColumnIndex(link, (count - 1));

            g.getChildren().add(link);
            count++;

        }

        return g;
    }

    private GridPane addReasonClusteredGraph(Edge n, double x, double y) {

        GridPane g = new GridPane();
        g.relocate(x, y);

        int count = 1;

        for (String reason : n.getReasons()) {

            Hyperlink link = new Hyperlink();
            link.setText(reason);

            Tooltip tip = new Tooltip(reason);

            link.setTooltip(tip);
            link.setOnAction((ActionEvent e) -> {

                DialogBox dialogBox = new DialogBox("Reason", reason, AlertType.INFORMATION);
                dialogBox.show();

            });

            GridPane.setMargin(link, new javafx.geometry.Insets(0, 2, 0, 2));
            GridPane.setColumnIndex(link, (count - 1));

            g.getChildren().add(link);
            count++;

        }

        return g;
    }

    @FXML
    public void exportToCSV() throws IOException {
        Writer writer = null;
        try {

            JFileChooser chooser = new JFileChooser();
            FileFilter filter = new FileNameExtensionFilter("csv", AppConstants.IMPROVEMENT_TABLE_EXPORT_EXTENSION);

            chooser.setFileFilter(filter);
            int returnVal = chooser.showSaveDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {

                String path = chooser.getSelectedFile().getPath();
//                System.out.println("path " + path);
                if (new File(path + "." + AppConstants.IMPROVEMENT_TABLE_EXPORT_EXTENSION).exists()) {
                    ErrorUtilities.popErrorMessage("Selected filename already exists at this location");
                    return;
                }

                if (path != null) {
                    writer = new BufferedWriter(new FileWriter(path + "." + AppConstants.IMPROVEMENT_TABLE_EXPORT_EXTENSION));
                    for (ImproveStudentGraphModel improveStudentGraphModel : improvementData) {

                        String text = improveStudentGraphModel.getAction().toString() + "," + improveStudentGraphModel.getStartNode() + "," + improveStudentGraphModel.getEndNode() + "," + String.join(",", improveStudentGraphModel.getReasons()) + "\n";

                        writer.write(text);
                    }

                    ErrorUtilities.popInfoMessage("Table saved successfully");

                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (writer != null) {
                writer.flush();
                writer.close();
            }

        }
    }

    @FXML
    private void setThresholdValue() {

        switch (comboSelectMethod.getValue()) {

            case GED:

                sliderThreshold.setMin(0);
                sliderThreshold.setSnapToTicks(true);
                sliderThreshold.setMax(100);
                sliderThreshold.setMajorTickUnit(10);
                sliderThreshold.setMinorTickCount(1);
                sliderThreshold.setBlockIncrement(1);

                break;
            case GRAPH_KERNEL:
            case GRAPH_EMBEDDING:

                txtThreshold.setText("0.5");

                sliderThreshold.setMin(0);
                sliderThreshold.setSnapToTicks(false);
                sliderThreshold.setMax(1);

                sliderThreshold.setMajorTickUnit(.2);
                sliderThreshold.setMinorTickCount(1);
                sliderThreshold.setBlockIncrement(1);

                break;

            default:
                break;

        }

    }

}
