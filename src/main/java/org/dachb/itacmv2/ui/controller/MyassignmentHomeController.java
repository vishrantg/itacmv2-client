/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.json.JSONArray;
import org.dachb.itacmv2.json.JSONUtil;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.services.UserAssignmentService;
import org.dachb.itacmv2.ui.custom.DialogBox;
import org.dachb.itacmv2.ui.custom.ImageButton;

/**
 * FXML Controller class
 *
 * @author Vishrant Gupta
 */
public class MyassignmentHomeController implements Initializable {

    @FXML
    private TableColumn<?, ?> assignmentNameColumn;

    @FXML
    private TableColumn<?, ?> numOfPointsColumn;

    @FXML
    private TableColumn<?, ?> gradeLevelColumn;

    @FXML
    private TableColumn<?, ?> classNameColumn;

    @FXML
    private TableColumn<?, ?> dueDateColumn;

    @FXML
    private TableColumn<?, ?> ownerColumn;

    private TableColumn<Assignment, Assignment> myAssignmentNameColumn;

    @FXML
    TableView myAssignmentTable;

    @FXML
    ProgressIndicator progressIndicator;

    private UserAssignmentService service;

    @FXML
    BorderPane myAssignmentHomeGridPane;

    @FXML
    private TableColumn<Assignment, Assignment> colCompare;

    @FXML
    private TableColumn<Assignment, Assignment> colEdit;

    @FXML
    private TableColumn<Assignment, Assignment> colTrash;

    @FXML
    private TableColumn<Assignment, Assignment> assignmentIdColumn;

    private final ApplicationContext context = ApplicationContext.getInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {

            context.setCurrentAssignment(null);

            myAssignmentTable.setPlaceholder(new Label("You have not created any assignment yet"));

            final ObservableList<Assignment> data = FXCollections.observableArrayList();

            assignmentNameColumn.setCellValueFactory(
                    new PropertyValueFactory<>("assignmentName"));
            numOfPointsColumn.setCellValueFactory(
                    new PropertyValueFactory<>("numberOfPoints"));
            gradeLevelColumn.setCellValueFactory(
                    new PropertyValueFactory<>("gradeLevel"));
            classNameColumn.setCellValueFactory(
                    new PropertyValueFactory<>("className"));
            dueDateColumn.setCellValueFactory(
                    new PropertyValueFactory<>("dueDate"));
            assignmentIdColumn.setCellValueFactory(
                    new PropertyValueFactory<>("id"));

            colCompare.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Assignment>(param.getValue()));

            colCompare.setStyle("-fx-table-cell-border-color: transparent");

            colCompare.setCellFactory(param -> new TableCell<Assignment, Assignment>() {
                private final ImageButton analyze = new ImageButton(AppConstants.COMPARE_ICON, AppConstants.COMPARE_ICON);

                @Override
                protected void updateItem(Assignment assignment, boolean empty) {
                    super.updateItem(assignment, empty);

                    if (assignment == null) {
                        setGraphic(null);
                        return;
                    }

                    analyze.setStyle("-fx-background-color:  transparent");

                    setGraphic(analyze);
                    analyze.setOnAction(event
                            -> {
                        context.setUserData(assignment);
                        context.setCurrentAssignment(assignment);
                        context.getAppStage(AppConstants.COMPARE_NETWORK, context.getAppStage().getWidth(), context.getAppStage().getHeight());
                    }
                    );

                }
            });

            colEdit.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Assignment>(param.getValue()));

            colEdit.setStyle("-fx-table-cell-border-color: transparent");

            colEdit.setCellFactory(param -> new TableCell<Assignment, Assignment>() {
                private final ImageButton edit = new ImageButton(AppConstants.CHECKBOX_ICON, AppConstants.CHECKBOX_ICON);

                @Override
                protected void updateItem(Assignment assignment, boolean empty) {
                    super.updateItem(assignment, empty);

                    if (assignment == null) {
                        setGraphic(null);
                        return;
                    }

                    edit.setStyle("-fx-background-color:  transparent");

                    setGraphic(edit);

                    edit.setOnAction(event
                            -> {
                        context.setCurrentAssignment(assignment);
                        Node node = context.loadResource(AppConstants.NEW_ASSIGNMENT);
                        myAssignmentHomeGridPane.setTop(null);
                        myAssignmentHomeGridPane.setCenter(node);
                    }
                    );

                }
            });

            colTrash.setCellValueFactory(param -> new ReadOnlyObjectWrapper<Assignment>(param.getValue()));

            colTrash.setStyle("-fx-table-cell-border-color: transparent");

            colTrash.setCellFactory(param -> new TableCell<Assignment, Assignment>() {
                private final ImageButton trash = new ImageButton(AppConstants.TRASH_ICON, AppConstants.TRASH_ICON);

                @Override
                protected void updateItem(Assignment assignment, boolean empty) {
                    super.updateItem(assignment, empty);

                    if (assignment == null) {
                        setGraphic(null);
                        return;
                    }

                    trash.setStyle("-fx-background-color:  transparent");

                    setGraphic(trash);

                    trash.setOnAction(event
                            -> {

                        assignment.setDueDate(null);
                        service = new UserAssignmentService(assignment, AppConstants.DELETE_ASSIGNMENT);
                        service.start();

                        service.setOnSucceeded(success -> {
                            data.remove(assignment);
                        });

                        service.setOnFailed(failes -> {
                            DialogBox dialogBox = new DialogBox("Error", "Error deleting assignment, please try again.", Alert.AlertType.ERROR);
                            dialogBox.show();
                        });

                        progressIndicator.progressProperty().bind(service.progressProperty());
                        progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));

                    }
                    );

                }

            });

            service = new UserAssignmentService(null, AppConstants.GET_ALL_ASSIGNMENT_URL);

            service.start();

            myAssignmentHomeGridPane.getChildren().forEach((node) -> {
                node.disableProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));
            });

            progressIndicator.progressProperty().bind(service.progressProperty());
            progressIndicator.visibleProperty().bind(BooleanExpression.booleanExpression(service.runningProperty()));

            service.setOnSucceeded((event) -> {
                String result = service.getMessage();

                try {
                    JSONArray array = new JSONArray(result);
                    for (int i = 0; i < array.length(); i++) {

                        try {
                            Assignment assignment = (Assignment) JSONUtil.convertToBean(array.getJSONObject(i), new Assignment());

                            data.add(assignment);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            service.setOnFailed((event) -> {
            });

            myAssignmentTable.setItems(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void newAssignment(ActionEvent newAssignmentEvent) {

        Node node = context.loadResource(AppConstants.NEW_ASSIGNMENT);
        myAssignmentHomeGridPane.setTop(null);
        myAssignmentHomeGridPane.setCenter(node);

    }

}
