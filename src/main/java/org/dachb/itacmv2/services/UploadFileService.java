/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.services;

import com.google.gson.Gson;
import edu.uci.ics.jung.graph.Graph;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.concurrent.Task;
import org.dachb.itacmv2.commons.HTTPUtilities;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.graph.GraphReader;
import org.dachb.itacmv2.graph.GraphType;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.EdgeDetails;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexDetail;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.model.AssignmentMap;

/**
 *
 * @author Vishrant Gupta
 */
public class UploadFileService extends NetworkComparisionService<List<File>> {

    private final List<File> files;
    private final Assignment assignment;
    private GraphType graphType;

    public UploadFileService(Assignment assignment, List<File> files, GraphType graphType) {
        this.assignment = assignment;
        this.files = files;
        this.graphType = graphType;
    }

    @Override
    protected Task<List<File>> createTask() {
        return new Task<List<File>>() {
            @Override
            protected List<File> call() throws Exception {

                final Gson gson = new Gson();

                files.forEach(file -> {
                    String response = null;

                    final String graphInJson = getGraphInJSONFormat(file);

                    final AssignmentMap assignmentMap = new AssignmentMap();
                    assignmentMap.setAssignmentId(assignment.getId());
                    assignmentMap.setGraphData(graphInJson);
                    assignmentMap.setGraphType(graphType != null ? graphType.getType() : null);
                    assignmentMap.setFilename(file.getName());

                    try {
                        response = HTTPUtilities.sendPostRequest(AppConstants.UPLOAD_MAP_URL, gson.toJson(assignmentMap), 10000, 10000);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    updateMessage(response);
                });

                updateProgress(10, 10);

                return null;
            }
        };
    }

    public String getGraphInJSONFormat(/*Graph<Vertex, Edge> graph*/File file) {

        GraphReader graphReader = null;
        try {
            graphReader = new GraphReader(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Graph<Vertex, Edge> graph = graphReader.getGraph();
        Collection<Edge> edges = graph.getEdges();
        Collection<Vertex> vertices = graph.getVertices();

        Collection<VertexDetail> nodeDetails = new ArrayList<VertexDetail>();
        Collection<EdgeDetails> edgeDetails = new ArrayList<EdgeDetails>();

        for (Vertex vertex : vertices) {
            VertexDetail vertexDetail = new VertexDetail();
            vertexDetail.setId(vertex.getId());
            vertexDetail.setName(vertex.getFullName());

            nodeDetails.add(vertexDetail);
        }

        for (Edge edge : edges) {

            Vertex source = graph.getSource(edge);
            Vertex dest = graph.getDest(edge);

            EdgeDetails edgeDetail = new EdgeDetails();
            edgeDetail.setSource(source.getFullName());
            edgeDetail.setTarget(dest.getFullName());

            edgeDetail.setId(edge.getId());

            edgeDetails.add(edgeDetail);
        }

        org.dachb.itacmv2.graph.datastructure.Graph graphDS = new org.dachb.itacmv2.graph.datastructure.Graph();
        graphDS.setEdges(edgeDetails);
        graphDS.setNodes(nodeDetails);

        Gson gson = new Gson();
        String graphData = gson.toJson(graphDS);

        return graphData;
    }

}
