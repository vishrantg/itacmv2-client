/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.services;

import com.google.gson.Gson;
import javafx.concurrent.Task;
import org.dachb.itacmv2.commons.HTTPUtilities;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.model.Term;

/**
 *
 * @author Vishrant Gupta
 */
public class RecommendationService extends NetworkComparisionService<Term> {

    private Term term;
    private ApplicationContext context = ApplicationContext.getInstance();

    public RecommendationService(Term term) {
        this.term = term;
    }

    @Override
    protected Task<Term> createTask() {

        return new Task<Term>() {
            @Override
            protected Term call() throws Exception {

                String response = null;
                if (term != null) {
                    Gson gson = new Gson();

                    term.setAssignmentId(context.getCurrentAssignment().getId());
                    term.setStudentMapId(context.getSelectedStudentMap().getFileId());
                    term.setExpertMapId(context.getSelectedExpertMap().getFileId());
                    
                    response = HTTPUtilities.sendPostRequest(AppConstants.GET_RECOMMENDATION, gson.toJson(term), 20000, 20000);
                }

                updateMessage(response);

                updateProgress(10, 10);

                return null;
            }
        };

    }

}
