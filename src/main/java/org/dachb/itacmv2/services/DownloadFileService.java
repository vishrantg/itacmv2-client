/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javafx.concurrent.Task;
import org.dachb.itacmv2.commons.HTTPUtilities;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.model.Assignment;
import org.dachb.itacmv2.model.AssignmentFile;

/**
 *
 * @author Vishrant Gupta
 */
public class DownloadFileService extends NetworkComparisionService<List<File>> {

    private final List<AssignmentFile> details;
    private final String urlString;
    private final Assignment assignment;

    public DownloadFileService(List<AssignmentFile> details, String urlString, Assignment assignment) {
        this.details = details;
        this.urlString = urlString;
        this.assignment = assignment;
    }

    @Override
    protected Task<List<File>> createTask() {
        return new Task() {
            @Override
            protected List<File> call() throws Exception {
                try {

                    List<File> files = new ArrayList<>();

                    for (AssignmentFile detail : details) {

                        String json = "{\"fileId\": " + detail.getFileId() + "}";
                        byte[] content = HTTPUtilities.downloadFile(urlString, json);

                        String tempFolder = AppConstants.APP_FOLDER
                                + AppConstants.FOLDER_SEPARATOR + assignment.getId()
                                + AppConstants.FOLDER_SEPARATOR + detail.folder()
                                + AppConstants.FOLDER_SEPARATOR;

                        File folder = new File(tempFolder);

                        if (folder.exists() || folder.mkdirs()) {

                            File targetFile = new File(folder, detail.getFileName());
                            try (OutputStream outStream = new FileOutputStream(targetFile)) {
                                outStream.write(content);

                                files.add(targetFile);
                            }
                        }
                    }

                    return files;

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }

}
