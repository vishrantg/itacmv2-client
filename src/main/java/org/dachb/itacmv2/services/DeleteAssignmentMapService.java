/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.services;

import javafx.concurrent.Task;
import org.dachb.itacmv2.commons.HTTPUtilities;
import org.dachb.itacmv2.constants.AppConstants;
import org.dachb.itacmv2.model.AssignmentFile;

/**
 *
 * @author Vishrant Gupta
 */
public class DeleteAssignmentMapService extends NetworkComparisionService<AssignmentFile> {

    private final AssignmentFile alignmentFile;

    public DeleteAssignmentMapService(AssignmentFile alignmentFile) {
        this.alignmentFile = alignmentFile;

    }

    @Override
    protected Task<AssignmentFile> createTask() {
        return new Task<AssignmentFile>() {
            @Override
            protected AssignmentFile call() throws Exception {
                try {

                    String json = "{\"fileId\": " + alignmentFile.getFileId() + "}";

                    String response = HTTPUtilities.sendPostRequest(AppConstants.DELETE_ASSIGNMENT_MAP, json, 10000, 10000);

                    updateMessage(response);

                    updateProgress(10, 10);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }

}
