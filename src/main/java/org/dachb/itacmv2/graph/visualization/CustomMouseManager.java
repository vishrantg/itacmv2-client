/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.visualization;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.awt.event.MouseEvent;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;
import org.dachb.itacmv2.context.ApplicationContext;
import org.dachb.itacmv2.model.Recommendation;
import org.dachb.itacmv2.model.Term;
import org.dachb.itacmv2.services.RecommendationService;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.view.util.DefaultMouseManager;

public class CustomMouseManager extends DefaultMouseManager {

    private ApplicationContext context = ApplicationContext.getInstance();

    private Node currentStudentSelectedNode = null;
    // private List<Edge> edges = new ArrayList<>();
    private List<String> edges = new ArrayList<>();
    private Map<String, CustomNode> expertNodes = null;

    // String - starting node id, Edge - edge created
    // private Map<String, Edge> finalAlignment = new HashMap<String, Edge>();
//	private Map<String, Node> finalAlignment = new HashMap<String, Node>();
//    private Map<Node, Node> finalAlignment = new HashMap<Node, Node>();
    private Graph graph;

    private List<Node> recommendationEdge = new ArrayList<>();

    private Map<String, CustomNode> studentNodes = null;

//    private final Map<org.graphstream.graph.Node, org.graphstream.graph.Node> currentAlignment = new HashMap<org.graphstream.graph.Node, org.graphstream.graph.Node>();
    private Gson gson = new Gson();

    public CustomMouseManager(Graph graph, Map<String, CustomNode> studentNodes, Map<String, CustomNode> expertNodes) {
        this.graph = graph;
        this.studentNodes = studentNodes;
        this.expertNodes = expertNodes;

        context.getFinalAlignment().clear();
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        // just override super class method to block node dragging
    }

    @Override
    public void mousePressed(MouseEvent event) {
        super.mousePressed(event);

        curElement = view.findNodeOrSpriteAt(event.getX(), event.getY());

        if (curElement != null) {

            Node node = graph.getNode(curElement.getId());

            Collection<Node> nodes = graph.getNodeSet();
            nodes.forEach(n -> {
                n.addAttribute("ui.style",
                        "fill-color: #fff;");
            });

            node.addAttribute("ui.style",
                    "fill-color: #000;");

            // fix, as single for loop was not removing all edges
            while (graph.getEdgeCount() != 0) {
                Iterable<? extends Edge> eachEdge = graph.getEachEdge();
                for (Edge edge : eachEdge) {
                    graph.removeEdge(edge);
                }
            }

            // expert node selected and student node already selected
            if (currentStudentSelectedNode != null && node != null && expertNodes.containsKey(node.getId())) {

                try {
                    Edge finalEdge = graph.addEdge("edge-" + currentStudentSelectedNode.getId(),
                            currentStudentSelectedNode.getId(), node.getId());
                    finalEdge.addAttribute("ui.style", "fill-color: #00f; size: 1px;");

//					finalAlignment.put(finalEdge.getSourceNode().getId(), finalEdge.getTargetNode());
//                    finalAlignment.put(finalEdge.getSourceNode(), finalEdge.getTargetNode());
                    context.getFinalAlignment().put(finalEdge.getSourceNode(), finalEdge.getTargetNode());
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }

            } // student node selected
            else if (node != null && studentNodes.containsKey(node.getId())) {

                currentStudentSelectedNode = node;

                for (Node node2 : recommendationEdge) {
                    graph.removeNode(node2);
                }
                recommendationEdge.clear();

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        RecommendationService recommendationService = new RecommendationService(new Term(currentStudentSelectedNode.getId().replaceFirst("student-", "")));

                        recommendationService.start();

                        recommendationService.setOnSucceeded((succeed) -> {

                            Type listType = new TypeToken<Set<Recommendation>>() {
                            }.getType();

                            Set<Recommendation> recommendations = new Gson().fromJson(recommendationService.getMessage(), listType);

                            Map<Recommendation, Recommendation> recommendationTerms = new HashMap<>();
                            if (recommendations != null) {
                                recommendations.stream().forEach(i -> recommendationTerms.put(i, i));
                            }
                            expertNodes.forEach((expertNodeId, expertNode) -> {

                                try {

                                    if (recommendationTerms.containsKey(new Recommendation(expertNodeId.replaceFirst("expert-", "")))) {

                                        Edge edge = graph.addEdge(node.getId() + expertNodeId, node.getId(), expertNode.getNode()
                                                .getId());

                                        Node recommendationNode = graph.addNode("rec-" + expertNodeId);
                                        recommendationNode.setAttribute("xyz", expertNode.getCordinates().getX() + 62, expertNode.getCordinates().getY(), 0);

                                        Float result = recommendationTerms.get(new Recommendation(expertNodeId.replaceFirst("expert-", ""))).getPercentage();

                                        recommendationNode.setAttribute("label",
                                                "   " + String.format("%.2f", result) + "%");

                                        recommendationNode.addAttribute("ui.style",
                                                "text-alignment: at-right; size-mode: fit; shape: box; text-color: blue; fill-color: blue; stroke-mode: none; size: "
                                                + result / 5 + ", 10px;");
                                        // "size-mode: fit; shape: box; text-color: white; fill-color: blue; stroke-mode: plain; size: 150px, 25px;");

                                        recommendationEdge.add(recommendationNode);

                                        edge.addAttribute("ui.style", "fill-color: #f00; size: 1px;");

                                        edges.add(edge.getId());
                                    } else {
                                        // edge.addAttribute("ui.style", "fill-color: #fff; size: 1px;");
                                        // edge = null;
                                    }

                                    Node targetNode = context.getFinalAlignment().get(node/*.getId()*/);

                                    if (targetNode != null && targetNode.getId().equalsIgnoreCase(expertNode.getNode().getId())) {

                                        Edge edge = graph.getEdge(node.getId());

                                        if (edge != null) {
                                            edge = graph.addEdge(node.getId() + expertNodeId, node.getId(), expertNode.getNode()
                                                    .getId());
                                            edge.addAttribute("ui.style", "fill-color: #00f; size: 1px;");

                                            edges.add(edge.getId());
                                        }

                                    }/* else {
                                        edge.addAttribute("ui.style", "fill-color: #f00; size: 1px;");
                                    }*/


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            });

                            // System.out.println("Recommendation: " + recommendationService.getMessage());
                        });

                        recommendationService.setOnFailed((failed) -> {
                            // progressIndicator.setVisible(false);
                        });

                    }
                });

            } // pressed somewhere else
            else {
                selectedElsewhere();
            }
        }

        if (curElement == null) {

            selectedElsewhere();

        }

    }

    private void selectedElsewhere() {
        currentStudentSelectedNode = null;

        Collection<Node> nodes = graph.getNodeSet();
        nodes.forEach(n -> {
            n.addAttribute("ui.style",
                    "fill-color: #fff;");
        });

        while (graph.getEdgeCount() != 0) {
            Iterable<? extends Edge> eachEdge = graph.getEachEdge();
            for (Edge edge : eachEdge) {
                graph.removeEdge(edge);
            }
        }

//        finalAlignment.forEach((startNode, targetNode) -> {
        context.getFinalAlignment().forEach((startNode, targetNode) -> {
//			Edge edge = graph.addEdge("edge-" + startNode, startNode, targetNode.getId());
            Edge edge = graph.addEdge("edge-" + startNode.getId(), startNode.getId(), targetNode.getId());
            edge.addAttribute("ui.style", "fill-color: #00f; size: 1px;");

            edges.add(edge.getId());
        });

        for (Node node2 : recommendationEdge) {
            graph.removeNode(node2);
        }
        recommendationEdge.clear();
    }

}
