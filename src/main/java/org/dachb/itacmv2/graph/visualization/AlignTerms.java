/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.visualization;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import org.dachb.itacmv2.graph.datastructure.VertexDetail;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.util.MouseManager;

public class AlignTerms {

    private Graph graph = new SingleGraph("Align terms");

    private Collection<VertexDetail> studentTerms = null;
    private Collection<VertexDetail> expertTerms = null;

    private Map<String, CustomNode> studentNodes = new HashMap<String, CustomNode>();
    private Map<String, CustomNode> expertNodes = new HashMap<String, CustomNode>();

    private ViewPanel viewPanel = null;

    public ViewPanel getViewPanel() {
        return viewPanel;
    }

    public AlignTerms(Collection<VertexDetail> studentTerms, Collection<VertexDetail> expertTerms) {

        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

        // FIX: org.graphstream.graph.IdAlreadyInUseException; removing all the elements
        graph.clear();
        
        this.studentTerms = studentTerms;
        this.expertTerms = expertTerms;

        setGraphAttributes();

        int expertSize = this.expertTerms.size();
        int studentSize = this.studentTerms.size();

        if (expertSize > studentSize) {
            studentNodeStartingPosition = (expertSize - studentSize) * 10;
        } else {
            expertNodeStartingPosition = (studentSize - expertSize) * 10;
        }

        for (VertexDetail node : studentTerms) {

            CustomNode studentNode = addStudentNode(node.getName());

            studentNodes.put(studentNode.getNode().getId(), studentNode);
        }
        for (VertexDetail node : expertTerms) {

            CustomNode expertNode = addExpertNode(node.getName());

            expertNodes.put(expertNode.getNode().getId(), expertNode);
        }

        startAlignment();
    }

    public Collection<VertexDetail> getStudentTerms() {
        return studentTerms;
    }

    public void setStudentTerms(Collection<VertexDetail> studentTerms) {
        this.studentTerms = studentTerms;
    }

    public Collection<VertexDetail> getExpertTerms() {
        return expertTerms;
    }

    public void setExpertTerms(List<VertexDetail> expertTerms) {
        this.expertTerms = expertTerms;
    }

    public void startAlignment() {

        Viewer viewer = graph.display();

        viewPanel = viewer.getDefaultView();

//        viewPanel.setSize(200, 600);
        
        Border border = BorderFactory.createLineBorder(Color.black);
        viewPanel.setBorder(border);

        viewPanel.openInAFrame(false);

        viewer.disableAutoLayout();

        MouseManager manager = new CustomMouseManager(graph, studentNodes, expertNodes);
        viewPanel.setMouseManager(manager);

    }

    private int studentNodeStartingPosition = 0;
    private int expertNodeStartingPosition = 0;

    private CustomNode addStudentNode(String nodeId) {
        Node node = this.graph.addNode("student-" + nodeId);

        node.setAttribute("label", nodeId);

//        if(studentNodes.size() < 10) {
//            node.setAttribute("xyz", 100, studentNodeStartingPosition, 0);
//        } else {
            node.setAttribute("xyz", 70, studentNodeStartingPosition, 0);
//        }
        
        

        ComponentCordinates cordinates = new ComponentCordinates(50, studentNodeStartingPosition, 0);
        CustomNode customNode = new CustomNode(node, cordinates);

        studentNodeStartingPosition = studentNodeStartingPosition + 10;

        return customNode;
    }

    private CustomNode addExpertNode(String nodeId) {
        Node node = this.graph.addNode("expert" + "-" + nodeId);

        node.setAttribute("label", nodeId);
        
//        if(studentNodes.size() < 10) {
//            node.setAttribute("xyz", 115.5, expertNodeStartingPosition, 0);
//        } else {
            node.setAttribute("xyz", 105.5, expertNodeStartingPosition, 0);
//        }
        
        

        node.setAttribute("ui.style",
                // "text-alignment: at-right; text-offset: 0px, 0px; text-background-mode: rounded-box; text-background-color: #fffC; text-padding: 15px, 14px; text-offset: -10px, -10px;");
                "text-alignment: right; text-offset: -10px, -12px; text-padding: 0px, 0px;");

        Node recommendationNode = this.graph.addNode("drec-" + nodeId);

        recommendationNode.setAttribute("label", "");
        
//        if(studentNodes.size() < 10) {
//            recommendationNode.setAttribute("xyz", 110, expertNodeStartingPosition, 0);
//        } else {
            recommendationNode.setAttribute("xyz", 115, expertNodeStartingPosition, 0);
//        }
        

        recommendationNode
                .addAttribute(
                        "ui.style",
                        "size-mode: normal; shape: box; text-color: white; fill-color: white; stroke-mode: none; stroke-color: #ffffff; size: 0px, 25px;");
        // "size-mode: fit; shape: box; text-color: white; fill-color: blue; stroke-mode: plain; size: 150px, 25px;");

        // expertNode.getNode()
        // .addAttribute(
        // "ui.stylesheet",
        // /*
        // "graph { padding: 20px; } node { size-mode: dyn-size; shape: box; fill-color: white; stroke-mode: plain; size: 150px, 25px; } edge { fill-color: #00f; size: 1px; }");
        // */ "node {  }");
        ComponentCordinates cordinates = new ComponentCordinates(50, expertNodeStartingPosition, 0);
        CustomNode customNode = new CustomNode(node, cordinates);

        expertNodeStartingPosition = expertNodeStartingPosition + 10;

        return customNode;
    }

    private void setGraphAttributes() {
        this.graph
                .addAttribute(
                        "ui.stylesheet",
                        /*
						 * "graph { padding: 20px; } node { size-mode: dyn-size; shape: box; fill-color: white; stroke-mode: plain; size: 150px, 25px; } edge { fill-color: #00f; size: 1px; }"
						 * );
                         */ "graph { padding: 20px; } node { text-alignment: at-left; text-offset: -5px, 0px; shape: box; fill-color: #fffc; stroke-mode: plain; size: 20px, 10px; text-size: 12;} edge { fill-color: #00f; size: 1px; }");
    }

}
