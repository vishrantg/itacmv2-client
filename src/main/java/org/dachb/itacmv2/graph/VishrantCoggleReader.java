/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.collections15.Factory;
import org.apache.commons.lang3.StringUtils;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.EdgeFactory;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexFactory;

/**
 *
 * @author Vishrant Gupta
 */
public class VishrantCoggleReader {

    private Graph<Vertex, Edge> graph;
    private Factory<Vertex> vertex_factory;
    private Factory<Edge> edge_factory;
    private HashMap<String, Vertex> nameToNode;
    private boolean debug = false;

    public Factory<Vertex> getVertexFactory() {
        return vertex_factory;
    }

    public Factory<Edge> getEdgeFactory() {
        return edge_factory;
    }

    public HashMap<String, Vertex> getNameToNode() {
        return nameToNode;
    }

    // added by Vishrant
    public List<String> getEdges() {

        Collection<Edge> edges = graph.getEdges();

        List<String> sEdges = new ArrayList<>();
        for (Edge edge : edges) {
            sEdges.add(edge.getValue());
        }

        return sEdges;
    }

    // added by Vishrant
    public List<String> getVertex() {
        Collection<Vertex> vertices = graph.getVertices();

        List<String> sVertex = new ArrayList<>();
        for (Vertex vertex : vertices) {
            sVertex.add("" + vertex.getName());
        }

        return sVertex;

    }

    // added by Vishrant
    public VishrantCoggleReader() {
        init();
    }

    // Added by Vishrant
    public VishrantCoggleReader(byte[] graphData) throws Exception {

        this();

        InputStream is = new ByteArrayInputStream(graphData);
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));

        loadMap(br);
    }

    public VishrantCoggleReader(File f) throws Exception {

        this();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(f));

            loadMap(br);
//        } catch (Exception e) {
//            System.err.println("[GraphReader:] Couldn't read (" + e.getMessage() + ")");
//        }
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    // Added by Vishrant
    private void init() {
        graph = new DirectedSparseGraph<Vertex, Edge>();//ObservableGraph<Vertex,Edge>(new

        vertex_factory = new VertexFactory();
        edge_factory = new EdgeFactory();

        nameToNode = new HashMap<String, Vertex>();
    }

    public void loadMap(BufferedReader br) throws IOException {

        String line = "";
        //starts processing all other lines

        // level mapped to node
        // List<Node> levedNode = new LinkedList<>();
        Map<Integer, Vertex> nodes = new TreeMap<>();

        while ((line = br.readLine()) != null) {
            if (line.replace("\n", "").replace("\r", "").isEmpty()) {
                continue;
            }

            Integer level = countSpace(line);
            
            line = line.replace(",", "");//removes the , so we get just the name
            
            if (nodes.containsKey(level - 1)) {
                addEdge(nodes.get(level - 1).getFullName(), line);
            } else {
                // add new node
                Vertex v = getNode(line);
                nodes.put(level, v);
            }
            
        }

    }

    //A line looks like ",,,,Promoted to Inside Sales,,,,,,,". By couting the number of ',' before the first letter we know how deep the line is.
    private int countSpace(String line) {
        char[] content = line.toCharArray();
        int count = 0;
        while (count < content.length && !StringUtils.isAlpha("" + content[count])) {
            count++;
        }
        return count;
    }

    public Vertex getNode(String name) {
        Vertex v = null;

        if (!nameToNode.containsKey(name)) {
            v = vertex_factory.create();
            v.setFullName(name);
            graph.addVertex(v);
            nameToNode.put(name, v);
        } else {
            v = nameToNode.get(name);
        }
        return v;
    }

    public void addEdge(String from, String to) {

        if (from == null || to == null || from.trim().isEmpty() || to.trim().isEmpty()) {
            return;
        }

        Vertex fromV = getNode(from.trim());
        Vertex toV = getNode(to.trim());

        //Check if there is already an edge
        for (Vertex v : graph.getSuccessors(fromV)) {
            if (debug) {
                System.out.println("\t\t\t" + v.getFullName());
            }
            if (v.getFullName().equals(to)) {
                return;
            }
        }
        //if the edge HAS to be added, then we add it.
        Edge e = edge_factory.create();
        e.setValue(fromV + "," + toV);

        graph.addEdge(e, fromV, toV, EdgeType.DIRECTED);
        if (debug) {
            System.out.println("<" + e.getId() + ">[" + from + "]->[" + to + "]");
        }
    }

}
