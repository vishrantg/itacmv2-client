/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.improvement;

/**
 *
 * @author Dave
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.dachb.itacmv2.graph.algorithm.DisjointPaths;
import org.dachb.itacmv2.graph.algorithm.GraphKernel;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.EdgeDetails;
import org.dachb.itacmv2.graph.datastructure.Graph;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexDetail;
import org.dachb.itacmv2.model.ImproveStudentGraphModel;

public class GraphUtils {

    public static List<ImproveStudentGraphModel> getImprovements(Graph studentGraph, Graph expertGraph, Map<String, String> align, int maxLoop, int maxDisjoint) {

        System.out.println("Getting improvements with maximum loop len " + maxLoop + " and maximum disjoint path len " + maxDisjoint);

        List<ImproveStudentGraphModel> differences;

        // get disjoint paths and get loops
        Graph tempStudentGraph = preprocess(studentGraph, align);

        differences = addNodeList(tempStudentGraph, expertGraph, maxLoop);
        differences.addAll(delNodeList(tempStudentGraph, expertGraph));
        differences.addAll(addEdgeList(tempStudentGraph, expertGraph, maxLoop, maxDisjoint));
        differences.addAll(delEdgeList(tempStudentGraph, expertGraph));

        return differences;
    }

    public static Graph preprocess(Graph s, Map<String, String> align) {

        if (align == null) {
            return s;
        }

        ArrayList<VertexDetail> smap = new ArrayList<>(s.getNodes());

        for (int i = 0; i < smap.size(); i++)//for every node in s
        {
            if (align.containsKey(smap.get(i).getName())) {
                smap.get(i).setName(align.get(smap.get(i).getName()));//replace name
            }

        }

        s.setNodes(smap);//reset nodes to not shitty names
        return s;
    }

    public static List<ImproveStudentGraphModel> addNodeList(Graph studentGraph, Graph expertGraph, int maxLoops) {
        List<ImproveStudentGraphModel> addlist = new ArrayList<>();

        if (studentGraph == null || expertGraph == null) {
            return addlist;
        }

        GraphKernel expertJungKernel = new GraphKernel(expertGraph.convertToJungGraph());//get the kernel that isnt static for some reason
        List<List<String>> expertLoops = expertJungKernel.getCycles();//get all the cycles from the expert map

        System.out.println("Expert map has " + expertLoops.size() + " loops.");

        if (studentGraph == null || expertGraph == null) {
            return addlist;
        }

        List<VertexDetail> studentNodes = new ArrayList<VertexDetail>(studentGraph.getNodes());
        List<VertexDetail> expertNodes = new ArrayList<VertexDetail>(expertGraph.getNodes());

        //array of whether or not each loop should give description
        List<Boolean> addingLoopDesc = new ArrayList<>();
        for (int j = 0; j < expertLoops.size(); j++) {
            //NOTE TO SELF: tbh check this again when you are more awake, cause this seems right but may be fucked
            addingLoopDesc.add((expertLoops.size() - getLoopVertexCount(studentGraph, expertLoops.get(0))) <= maxLoops);
        }

        for (int i = 0; i < expertNodes.size(); i++)//for every node in expert list
        {
            boolean found = false;
            boolean loopnode = false;
            for (int k = 0; k < studentNodes.size(); k++)//compare one node to every node in student
            {
                if (expertNodes.get(i).getName().equals(studentNodes.get(k).getName()))//if we actually find this node in the students map
                {
                    found = true;
                    break;
                }
            }
            if (!found) {
                ImproveStudentGraphModel studentGraphModel = new ImproveStudentGraphModel(ImproveStudentGraphModel.Action.ADD_NODE,
                        expertNodes.get(i).getName(), "NA");

                int index = nodeInList(expertNodes.get(i), expertLoops);
                if (index != -1)//before you call this hack code, remember that a CPU is a rock we flattened and ran electricity through
                {
                    if (addingLoopDesc.get(index))//if we have enough nodes to warrent further explanation
                    {
                        //So this basically is checking if the last node in the loop array is the node we are currently comparing
                        if (expertLoops.get(index).get(expertLoops.get(index).size() - 1).equals(expertNodes.get(i).getName())) {
                            studentGraphModel.addReason("The node \"" + expertNodes.get(i).getName() + "\" would complete a loop.");
                        } else {
                            studentGraphModel.addReason("The node \"" + expertNodes.get(i).getName() + "\" is part of a loop.");
                        }
                    } else// we would need to add more nodes than the  person specified
                    {
                        studentGraphModel.addReason("The student's map did not have the node \"" + expertNodes.get(i).getName() + "\"");
                    }
                } else//if this node isnt even in the loop
                {

                    studentGraphModel.addReason("The student's map did not have the node \"" + expertNodes.get(i).getName() + "\"");
                }

                addlist.add(studentGraphModel);//add object explaining why we need to add   

            }
        }

        return addlist;
    }

    public static List<ImproveStudentGraphModel> delNodeList(Graph studentGraph, Graph expertGraph) {
        List<ImproveStudentGraphModel> sublist = new ArrayList<>();

        if (studentGraph == null || expertGraph == null) {
            return sublist;
        }

        if (studentGraph == null || expertGraph == null) {
            return sublist;
        }

        List<VertexDetail> studentNodes = new ArrayList<VertexDetail>(studentGraph.getNodes());
        List<VertexDetail> expertNodes = new ArrayList<VertexDetail>(expertGraph.getNodes());

        for (int i = 0; i < studentNodes.size(); i++) {
            boolean found = false;
            for (int k = 0; k < expertNodes.size(); k++) {
                if (studentNodes.get(i).getName().equals(expertNodes.get(k).getName())) {
                    found = true;
                    break;
                }
            }
            if (!found) {

                ImproveStudentGraphModel studentGraphModel = new ImproveStudentGraphModel(ImproveStudentGraphModel.Action.DELETE_NODE,
                        studentNodes.get(i).getName(), "NA");

                studentGraphModel.addReason("The expert's map did not have the node \"" + studentNodes.get(i).getName() + "\"");

                sublist.add(studentGraphModel);//add object explaining why we need to add   

            }
        }

        return sublist;
    }

    public static ArrayList<ImproveStudentGraphModel> addEdgeList(Graph studentGraph, Graph expertGraph, int maxLoops, int maxDisjoint) {
        ArrayList<ImproveStudentGraphModel> addedge = new ArrayList<>();

        if (studentGraph == null || expertGraph == null) {
            return addedge;
        }

        List<EdgeDetails> studentEdges = new ArrayList<EdgeDetails>(studentGraph.getEdges());
        List<EdgeDetails> expertEdges = new ArrayList<EdgeDetails>(expertGraph.getEdges());

        edu.uci.ics.jung.graph.Graph<Vertex, Edge> jungexpert = expertGraph.convertToJungGraph();//so we need to convert to a jung graph
        List<Vertex> jungVertices = new ArrayList<>(jungexpert.getVertices());//because nothing can ever be easy

        List<List<Edge>> expertDisjoint = new ArrayList<List<Edge>>();//this will contain the disjoint edges
        for (int q = 0; q < jungVertices.size(); q++)//start at one node
        {
            for (int e = 0; e < jungVertices.size(); e++)//get allllllll the paths
            {
                if (q == e) {
                    continue;
                }
                ArrayList<List<Edge>> temp;
                temp = new ArrayList<List<Edge>>(DisjointPaths.getEdgeDisjointPaths(jungexpert, jungVertices.get(q), jungVertices.get(e)));
                if (temp != null && temp.size() > 1)//no disjoint paths
                {
                    Vertex disjointFrom = jungVertices.get(q);
                    Vertex disjointTo = jungVertices.get(e);
                    System.out.println(temp.size() + " paths from \"" + disjointFrom.getFullName() + "\" to \"" + disjointTo.getFullName() + "\"");
                    expertDisjoint.addAll(temp);//add the disjoints to the big pile of em'
                }
            }
        }

        List<List<EdgeDetails>> disjointPaths;
        disjointPaths = new ArrayList<List<EdgeDetails>>();
        //This loops just converts the edges to edgedetails, just for convenience
        for (int i = 0; i < expertDisjoint.size(); i++) {
            //send in one of the paths, then add it to the overall disjoint paths
            disjointPaths.add(convertEdgeToEdgeDetails(jungexpert, new ArrayList<Edge>(expertDisjoint.get(i))));
        }

        //this loop creates a boolean array list similair to the one we use for loops
        ArrayList<Integer> addingDisjointDesc = new ArrayList<>();
        for (int j = 0; j < disjointPaths.size(); j++) {
            int sharedEdges = getDisjointEdgeCount(studentGraph, disjointPaths.get(j));

            System.out.print("Path of length " + disjointPaths.get(j).size() + " : ");
            for (int k = 0; k < disjointPaths.get(j).size(); k++) {
                System.out.print(disjointPaths.get(j).get(k).getSource() + "->" + disjointPaths.get(j).get(k).getTarget() + " ");
            }

            addingDisjointDesc.add((disjointPaths.get(j).size() - sharedEdges));

            System.out.println(": Student has " + sharedEdges + " edges. (difference " + (disjointPaths.get(j).size() - sharedEdges) + " max " + maxDisjoint + ")");
        }

        //get the loops
        GraphKernel expertJungKernel = new GraphKernel(expertGraph.convertToJungGraph());//get the kernel that isnt static for some reason
        List<List<String>> expertLoops = expertJungKernel.getCycles();//get all the cycles from the expert map

        List<Integer> addingLoopDesc = new ArrayList<Integer>();
        for (int j = 0; j < expertLoops.size(); j++) {

            int sharedEdges = getLoopEdgeCount(studentGraph, expertLoops.get(j));
            System.out.print("Loop " + j + " : ");
            for (int k = 0; k < expertLoops.get(j).size(); k++) {
                System.out.print(" " + expertLoops.get(j).get(k));
            }
            System.out.println(" " + expertLoops.get(j).size() + " edges, student has " + sharedEdges + ", max is " + maxLoops);

            addingLoopDesc.add((expertLoops.get(j).size() - sharedEdges));
        }

        for (int i = 0; i < expertEdges.size(); i++)//so just go through all the edges
        {
            boolean found = false;
            for (int k = 0; k < studentEdges.size(); k++) {
                if (expertEdges.get(i).getSource().equals(studentEdges.get(k).getSource())) {
                    if (expertEdges.get(i).getTarget().equals(studentEdges.get(k).getTarget())) {
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {

                ImproveStudentGraphModel studentGraphModel = new ImproveStudentGraphModel(ImproveStudentGraphModel.Action.ADD_EDGE,
                        expertEdges.get(i).getSource(), expertEdges.get(i).getTarget());

                studentGraphModel.addReason("The student's map did not have the edge connecting \"" + expertEdges.get(i).getSource()
                        + "\" to \"" + expertEdges.get(i).getTarget() + "\"");

                int index = edgeInList(expertEdges.get(i), expertLoops);//check if this edge is in expertLoops
                if (index != -1) {
                    if (addingLoopDesc.get(index) <= maxLoops)//if we have enough edges of loop to warrent further explanation
                    {
                        //So this basically is checking if the last edge in the loop array is the node we are currently comparing                       
                        if (addingLoopDesc.get(index) == 1) {
                            studentGraphModel.addReason("The edge \"" + expertEdges.get(i).getSource() + "\" to \"" + expertEdges.get(i).getTarget()
                                    + "\" would complete a loop.");

                        } else {
                            studentGraphModel.addReason("The edge \"" + expertEdges.get(i).getSource() + "\" to \"" + expertEdges.get(i).getTarget()
                                    + "\" is part of a loop");
                        }
                    }
                }

                index = edgeInDisjoint(expertEdges.get(i), disjointPaths);
                if (index != -1 && (addingDisjointDesc.get(index) <= maxDisjoint)) {
                    if (addingDisjointDesc.get(index) > 1)//if its in the disjoint paths list, and is also allowed to offer more descriptive suggestions
                    {
                        studentGraphModel.addReason("The edge \"" + expertEdges.get(i).getSource() + "\" to \"" + expertEdges.get(i).getTarget()
                                + "\" is part of a disjoint path");
                    } else {
                        studentGraphModel.addReason("The edge \"" + expertEdges.get(i).getSource() + "\" to \"" + expertEdges.get(i).getTarget()
                                + "\" will complete a disjoint path");
                    }
                }

                addedge.add(studentGraphModel);//add object explaining why we need to add   

            }
        }

        return addedge;
    }

    public static ArrayList<ImproveStudentGraphModel> delEdgeList(Graph studentGraph, Graph expertGraph) {
        ArrayList<ImproveStudentGraphModel> deledge = new ArrayList<>();

        if (studentGraph == null || expertGraph == null) {
            return deledge;
        }

        ArrayList<EdgeDetails> studentEdges = new ArrayList<EdgeDetails>(studentGraph.getEdges());
        ArrayList<EdgeDetails> expertEdges = new ArrayList<EdgeDetails>(expertGraph.getEdges());

        for (int i = 0; i < studentEdges.size(); i++)//
        {
            boolean found = false;
            for (int k = 0; k < expertEdges.size(); k++) {
                if (studentEdges.get(i).getSource().equals(expertEdges.get(k).getSource())) {
                    if (studentEdges.get(i).getTarget().equals(expertEdges.get(k).getTarget())) {
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {

                ImproveStudentGraphModel studentGraphModel = new ImproveStudentGraphModel(ImproveStudentGraphModel.Action.DELETE_EDGE,
                        studentEdges.get(i).getSource(), studentEdges.get(i).getTarget());

                studentGraphModel.addReason("The expert's map did not have the edge connecting \""
                        + studentEdges.get(i).getSource() + "\" to \"" + studentEdges.get(i).getTarget() + "\"");

                deledge.add(studentGraphModel);//add object explaining why we need to add   

            }
        }

        return deledge;
    }

    //This function will return how many vertex's(vertices?) are in a given loop, this is used for that slidey bar
    private static int getLoopVertexCount(Graph studentGraph, List<String> loop) {
        int count = 0;

        List<VertexDetail> studentNodes = new ArrayList<VertexDetail>(studentGraph.getNodes());//nodes in student graoh

        for (int i = 0; i < loop.size(); i++)//for every node in the loop given
        {
            for (int d = 0; d < studentNodes.size(); d++)//for every node in the student graoh
            {
                if (studentNodes.get(d).getName().equals(loop.get(i)))//if node of loop is in map
                {
                    count++;//increase count of nodes that do not need to be added
                    break;
                }
            }

        }

        return count;
    }

    //Returns int which is an index to what loop the given vertex is located
    //Returns -1 if it isn't found anywhere
    private static int nodeInList(VertexDetail v, List<List<String>> looplist) {

        for (int i = 0; i < looplist.size(); i++) {
            for (int d = 0; d < looplist.get(i).size(); d++) {
                if (v.getName().equals(looplist.get(i).get(d))) {
                    return i;
                }
            }
        }

        return -1;
    }

    //same thing as above functions but counts edges
    private static int getLoopEdgeCount(Graph studentGraph, List<String> loop) {
        int count = 0;

        ArrayList<EdgeDetails> studentEdges = new ArrayList<EdgeDetails>(studentGraph.getEdges());//nodes in student graoh

        for (int i = 0; i < loop.size(); i++)//for every node in the loop given
        {
            for (int d = 0; d < studentEdges.size(); d++)//for every node in the student graoh
            {
                if (studentEdges.get(d).getSource().equals(loop.get(i)) && studentEdges.get(d).getTarget().equals(loop.get((i + 1) % loop.size())))//if edge of loop is in map
                {
                    count++;//increase count of nodes that do not need to be added
                    break;
                }
            }

        }

        return count;
    }

    private static int getDisjointEdgeCount(Graph studentGraph, List<EdgeDetails> disjoint) {
        int count = 0;

        List<EdgeDetails> studentEdges = new ArrayList<EdgeDetails>(studentGraph.getEdges());//Edges in student graoh

        for (int i = 0; i < disjoint.size(); i++)//for every edge in the disjoint array
        {
            for (int d = 0; d < studentEdges.size(); d++)//for every edge in the student graph
            {
                if (studentEdges.get(d).getSource().equals(disjoint.get(i).getSource())
                        && studentEdges.get(d).getTarget().equals(disjoint.get(i).getTarget()))//if edge of loop is in map
                {
                    count++;//increase count of edges that do not need to be added
                    break;
                }
            }
        }
        return count;
    }

    //returns index of loop that contains the given edge
    //return -1 if the given edge is not in a loop
    private static int edgeInList(EdgeDetails e, List<List<String>> looplist) {

        for (int i = 0; i < looplist.size(); i++) {
            List<String> loop = looplist.get(i);
            int loopsize = loop.size();
            for (int d = 0; d < loopsize; d++) {
                if (e.getSource().equals(loop.get(d)) && e.getTarget().equals(loop.get((d + 1) % loopsize))) {
                    return i;
                }
            }
        }

        return -1;
    }

    private static int edgeInDisjoint(EdgeDetails e, List<List<EdgeDetails>> disjointlist) {
        for (int i = 0; i < disjointlist.size(); i++)//for every disjoint path
        {
            List<EdgeDetails> path = disjointlist.get(i);
            for (int d = 0; d < path.size(); d++)//for ever edge in disjoint path
            {
                //if these Edges are equal
                if (e.getSource().equals(path.get(d).getSource()) && e.getTarget().equals(path.get(d).getTarget())) {
                    //return what loop so we cna check if we want to reccommend the fact its an edge
                    return i;
                }
            }
        }

        return -1;//was not in the disjoint paths 
    }

    //since id rather die than work with "edges", this returns edges as edgedetails
    private static ArrayList<EdgeDetails> convertEdgeToEdgeDetails(edu.uci.ics.jung.graph.Graph<Vertex, Edge> graph, ArrayList<Edge> oge) {
        ArrayList<EdgeDetails> e;
        e = new ArrayList<EdgeDetails>();

        for (int k = 0; k < oge.size(); k++) {
            EdgeDetails d = new EdgeDetails();
            Vertex source = graph.getSource(oge.get(k));//get source
            Vertex target = graph.getDest(oge.get(k));//get dest
            d.setSource(source.getFullName());//set the last 2 things to the temp edge detail
            d.setTarget(target.getFullName());
            e.add(d);//add that edgedetail to the biglist 
        }

        return e;
    }
}
