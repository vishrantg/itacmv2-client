/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import edu.uci.ics.jung.graph.Graph;
import java.util.Collection;
import java.util.List;
import org.dachb.itacmv2.graph.algo.JohnsonSimpleCycles;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Vishrant Gupta
 */
public class GraphKernel {

    private final Graph<Vertex, Edge> graph;

    // TODO make this class as service
    public GraphKernel(Graph<Vertex, Edge> graph) {
        this.graph = graph;
    }

    public List<List<String>> getLoops() {

//        Loops loops = new Loops(graph);
//        return loops.cycleLength();

          return getCycles();

    }

    
    public List<List<String>> getCycles() {
        
        DefaultDirectedGraph<String, DefaultEdge> directedGraph =
            new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        
        Collection<Vertex> vertices = graph.getVertices();
        
        for (Vertex vertex : vertices) {
            directedGraph.addVertex(vertex.getFullName());
        }
        
        Collection<Edge> edges = graph.getEdges();
        for (Edge edge : edges) {
            Vertex source = graph.getSource(edge);
            Vertex dest = graph.getDest(edge);
            
            directedGraph.addEdge(source.getFullName(), dest.getFullName());
        }
        
        JohnsonSimpleCycles cycles = new JohnsonSimpleCycles(directedGraph);
        
        return cycles.findSimpleCycles();
        
//        
//        SparseDoubleMatrix2DExt s = GraphMatrixOperationsExt.graphToSparseMatrix(graph);
//        double[][] matrix = s.toArray();
//        int k = 0;
//        for (double[] i : matrix) {
//            for (double j : i) {
//                System.out.print(j + " ");
//            }
//            System.out.println(s.getVertices()[k] + "\n");
//            k++;
//        }
//        
//        
//        ElementaryCyclesSearch findCycle = new ElementaryCyclesSearch(matrix, s.getVertices());
//        return findCycle.getElementaryCycles();
        
    }
    
}
