/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import cern.colt.map.AbstractIntDoubleMap;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

/**
 *
 * @author Vishrant Gupta
 */
public class SparseDoubleMatrix2DExt extends SparseDoubleMatrix2D {

    public SparseDoubleMatrix2DExt(double[][] doubles) {
        super(doubles);
    }

    public SparseDoubleMatrix2DExt(int arg0, int arg1) {
        super(arg0, arg1);
    }

    public SparseDoubleMatrix2DExt(int arg0, int arg1, int arg2, double arg3, double arg5) {
        super(arg2, arg1, arg2, arg5, arg5);
    }

    protected SparseDoubleMatrix2DExt(int arg0, int arg1, AbstractIntDoubleMap arg2, int arg3, int arg4, int arg5, int arg6) {
        super(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }

    public Object[] getVertices() {
        return GraphMatrixOperationsExt.vertices;
    }

}
