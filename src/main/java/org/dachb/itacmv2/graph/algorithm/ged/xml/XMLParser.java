package org.dachb.itacmv2.graph.algorithm.ged.xml;

import java.io.FileReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;
import org.dachb.itacmv2.graph.algorithm.ged.nanoxml.XMLElement;
import org.dachb.itacmv2.graph.algorithm.ged.util.Edge;
import org.dachb.itacmv2.graph.algorithm.ged.util.Graph;
import org.dachb.itacmv2.graph.algorithm.ged.util.GraphSet;
import org.dachb.itacmv2.graph.algorithm.ged.util.Node;
import org.dachb.itacmv2.graph.datastructure.Vertex;
public class XMLParser {

    /**
     * the path to the graph sets
     */
    private String graphPath;

    public void setGraphPath(String graphPath) {
        this.graphPath = graphPath;
    }

    /**
     * @return a graph set with
     * @param filename
     * @throws Exception
     */
    public GraphSet parseCXL(String filename) throws Exception {
        XMLElement xml = new XMLElement();
        FileReader reader = new FileReader(filename);
        xml.parseFromReader(reader);
        GraphSet graphSet = new GraphSet();
        Vector<XMLElement> children = xml.getChildren();
        XMLElement root = children.get(0);
        Enumeration<XMLElement> enumerator = root.enumerateChildren();
        int i = 1;
        while (enumerator.hasMoreElements()) {

            XMLElement child = enumerator.nextElement();
            Graph g = this.parseGXL(this.graphPath
                    + child.getAttribute("file", null) + "");
            g.setFileName(child.getAttribute("file", null) + "");
            g.setClassName((String) child.getAttribute("class", "NO_CLASS"));
            graphSet.add(g);
        }
        return graphSet;
    }

    /**
     * @return a graph set with
     * @param filename
     * @throws Exception
     *
     * @author Vishrant Gupta Gupta
     */
    public GraphSet parseJung(edu.uci.ics.jung.graph.Graph<Vertex, org.dachb.itacmv2.graph.datastructure.Edge> graph, boolean isDirected)
            throws Exception {

        GraphSet graphSet = new GraphSet();

        Graph g = new Graph();
        g.setDirected(isDirected);

        TreeSet<Vertex> vertices = new TreeSet<>(graph.getVertices());
//        Collections.sort(vertices);
        
        Map<Vertex, Integer> map = new HashMap<>();

        int count = 0;
        for (Vertex vertex : vertices) {
            Node node = new Node();
            node.setNodeID(vertex.getFullName());

            node.put("name", node.getNodeID());
            
            map.put(vertex, count);
            count++;

            g.add(node);
        }

        TreeSet<org.dachb.itacmv2.graph.datastructure.Edge> jungEdges = new TreeSet<>(graph.getEdges());
//        Collections.sort(jungEdges);
        
        Edge[][] edgeAdjacenyMatrix = new Edge[vertices.size()][vertices.size()];

        for (org.dachb.itacmv2.graph.datastructure.Edge jungEdge : jungEdges) {
            Edge edge = new Edge();

            Vertex source = graph.getSource(jungEdge);

            Node startNode = new Node();
            startNode.setNodeID(source.getFullName());
            startNode = g.get(g.indexOf(startNode)); // replacing it with graph node instance

            edge.setStartNode(startNode);

            Vertex dest = graph.getDest(jungEdge);

            Node destNode = new Node();
            destNode.setNodeID(dest.getFullName());
            destNode = g.get(g.indexOf(destNode)); // replacing it with graph node instance

            edge.setEndNode(destNode);

            edge.put("from", source.getFullName());
            edge.put("to", dest.getFullName());
            edge.setEdgeID(source.getFullName() + "_<>" + dest.getFullName());

            startNode.getEdges().add(edge);
            destNode.getEdges().add(edge);

            // directed graph
            edgeAdjacenyMatrix[map.get(source)][map.get(dest)] = edge;

        }

        // TODO DELETE
//        for (Edge[] edges : edgeAdjacenyMatrix) {
//            for (Edge edge : edges) {
//                if (edge != null) {
//                    System.out.print(edge.getEdgeID() + " ");
//                } else {
//                    System.out.print(" |||| ");
//                }
//            }
//            System.out.println("");
//        }
//        System.out.println("DONE LOADING ");

        g.setAdjacenyMatrix(edgeAdjacenyMatrix);

        graphSet.add(g);

        return graphSet;
    }

    /**
     * @return a graph with
     * @param filename
     * @throws Exception
     */
    public Graph parseGXL(String filename) throws Exception {

        XMLElement xml = new XMLElement();
        FileReader reader = new FileReader(filename);
        xml.parseFromReader(reader);
        reader.close();
        Graph graph1 = new Graph();
        Vector children = xml.getChildren();
        XMLElement root = (XMLElement) children.get(0);
        String id = (String) root.getAttribute("id", null);
        String edgemode = (String) root.getAttribute("edgemode", "undirected");
        graph1.setGraphID(id);
        if (edgemode.equals("undirected")) {
            graph1.setDirected(false);
        } else {
            graph1.setDirected(true);
        }
        Enumeration enumerator = root.enumerateChildren();
        int n = 0;
        while (enumerator.hasMoreElements()) {
            XMLElement child = (XMLElement) enumerator.nextElement();
            if (child.getName().equals("node")) {
                String nodeId = (String) (child.getAttribute("id", null));
                Node node = new Node();
                node.setNodeID(nodeId);
                Enumeration enum1 = child.enumerateChildren();
                while (enum1.hasMoreElements()) {
                    XMLElement child1 = (XMLElement) enum1.nextElement();
                    if (child1.getName().equals("attr")) {
                        String key = (String) child1.getAttribute("name", null);
                        Vector children2 = child1.getChildren();
                        XMLElement child2 = (XMLElement) children2.get(0);
                        String value = child2.getContent();
                        node.put(key, value);
                    }

                }
                graph1.add(node);
                n++;
            }
        }
        Edge[][] edges = new Edge[n][n];
        graph1.setAdjacenyMatrix(edges);
        enumerator = root.enumerateChildren();
        while (enumerator.hasMoreElements()) {
            XMLElement child = (XMLElement) enumerator.nextElement();
            if (child.getName().equals("edge")) {
                Edge edge = new Edge();
                String from = (String) child.getAttribute("from", null);
                String to = (String) child.getAttribute("to", null);
                edge.put("from", from);
                edge.put("to", to);
                edge.setEdgeID(from + "_<>" + to);
                // *******************************
                Enumeration enum1 = child.enumerateChildren();
                while (enum1.hasMoreElements()) {
                    XMLElement child1 = (XMLElement) enum1.nextElement();
                    if (child1.getName().equals("attr")) {
                        String key = (String) child1.getAttribute("name",
                                "key failed!");
                        Vector children2 = child1.getChildren();
                        XMLElement child2 = (XMLElement) children2.get(0);
                        String value = child2.getContent();
                        edge.put(key, value);
                    }
                }

                for (int i = 0; i < graph1.size(); i++) {
                    Node nodeI = graph1.get(i);
                    if (nodeI.getNodeID().equals(from)) {
                        edge.setStartNode(nodeI);
                        nodeI.getEdges().add(edge);
                        for (int j = 0; j < graph1.size(); j++) {
                            Node nodeJ = graph1.get(j);
                            if (nodeJ.getNodeID().equals(to)) {
                                edge.setEndNode(nodeJ);
                                nodeJ.getEdges().add(edge);
                                edges[i][j] = edge;
                                if (!graph1.isDirected()) {
                                    edges[j][i] = edge;
                                }
                            }
                        }
                    }
                }
            }
        }

        return graph1;
    }

}
