/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import edu.uci.ics.jung.graph.Graph;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.Vertex;

public class Loops {

	private Graph<Vertex,Edge> graph;
	private int limit;								
	private HashMap<Integer, Vertex> vertexMap;		// We numbered vertices for easier access (0,1,2,...)
	private HashMap<String, Integer> namesMap;		// And we have another mapping between names assigned to vertices and their numbers (0,1,2,...)
	private SortedSet cyclesFound;					// Storage to keep found cycles
	
	public Loops(Graph<Vertex,Edge> g){
		this.limit = 10;							// Limit on length of a path, no idea why we needed it
		this.graph = g;
		
		vertexMap = new HashMap<Integer,Vertex>();
		namesMap = new HashMap<String, Integer>();
		int counter = 0;
		for(Vertex node: graph.getVertices()){		// Initialization of our maps using vertices from graph (using counter to create numbers for vertices)
			vertexMap.put(counter, node);
			namesMap.put(node.getFullName(), counter);
			counter++;
		}
	}
	
	public List<List<Vertex>> cycleLength(){
		
/**************************************
 * 		      INITIALIZATION          * 
 **************************************/
		Vector<Vector<Integer>> B;													// We create a list of vertices
		List<List<Vertex>> collectionCycles = new ArrayList<List<Vertex>>();
		
		Vector<Integer> path = new Vector<Integer>();
		cyclesFound = new TreeSet();
		
		boolean[] avail = new boolean[graph.getVertexCount()];						// avail is used in Cycle method to check whether vertex is already on the path
		boolean[] visited = new boolean[graph.getVertexCount()];					// visited is used in here to make sure we don't check the same vertex twice
		for(int i = 0; i<visited.length; i++){
			visited[i] = false;
		}
		
		int s = 0;															// We loop through vertices using their numbers so 0,1,2,....
		while (s < graph.getVertexCount()){
		//	System.out.println(s + "/" + graph.getVertexCount());
			if(graph.getIncidentEdges(vertexMap.get(s)).size() > 0 ){		// Each vertex serves as a source vertex for a cycle, we go through its outgoing edges and look for a cycle
				B = new Vector();											// Main list creation
				for(int i= 0; i <avail.length; i++){
					avail[i] = true;
					B.add(new Vector());									// And list of vertices for given source vertex
				}
				Cycle(s,s,path,avail,B,visited);							// Beginning of the recursion
			}	
			visited[s]= true;												// if we checked this vertex we don't want to do it again
			s++;
		}
		
		//System.out.println("Found " + cyclesFound.toString());
	    
	    // extract cycle lengths from the lists
	    Iterator it = cyclesFound.iterator();								// Final step, transformation of our cycle list (this software purpose only!!!)
	    while (it.hasNext()) {
	    	List<Vertex> cycle = new ArrayList<Vertex>();
	        String element = (String)it.next();
	        element = element.replace("[", "");
	        element = element.replace("]", "");
	        String[] nodes = element.split(",");
	        for(int i=0; i<nodes.length; i++){
//	        	System.out.print(Integer.parseInt(nodes[i].trim())+", ");
	        	cycle.add(vertexMap.get(Integer.parseInt(nodes[i].trim())));
	        }
	      
	        collectionCycles.add(cycle);
	    }
	    
	    return collectionCycles;
	}
	
	
	public void Cycle(int v, int s, Vector<Integer> path, boolean[] avail, Vector<Vector<Integer>> B, boolean[] visited){
		boolean flag = false;
		
		if(visited[v]) return;
		if(limit != -1 && path.size() >= limit) return; 		// limits cycle length
		if(!path.isEmpty() && path.contains(v)) return;
		
		path.add(v);											// we add vertex to the path
		avail[v] = false;										// add mark this vertex as already used to build this path
		
		Collection<Edge> outEdges = graph.getOutEdges(vertexMap.get(v));		// next we explore all the connections going out from our vertex
		for(Edge link: outEdges){												// so for every outgoing edge
			int endNode = namesMap.get(link.getAttribute("End node"));			// we take the vertex which is on the other end of the outgoing edge (no our vertex)
			if(endNode == v) continue;											// avoiding self-loops
			if(endNode == s){													// found a cycle!
				Vector<Integer> result = new Vector();
				Vector<Integer> result_copy = new Vector();
				result.addAll(path.subList(path.lastIndexOf(s),path.size()));		// we save found cycle to some storage vector
				result_copy.addAll(path.subList(path.lastIndexOf(s),path.size()));	// we do the copy of the same cycle in other vector
				Collections.sort(result);											// first vector is sorted and we checked our main storage to make sure we are not having the same cycle again
				if(!cyclesFound.contains(result.toString()))
					cyclesFound.add(result_copy.toString());						// if we don't have this cycle already we can save it to main storage
				flag = true;														// we mark result as true - we found cycle		
				
			}else if(avail[endNode]){												// if we did not found cycle that's ok we look further using recursion (assuming end vertex was not checked yet)
				Cycle(endNode, s, path, avail, B, visited);
			}
		}
		
		if(flag){																	// now if we found cycle we need to do some cleaning
			Unmark(v, avail, B);													// we mark all vertices in found cycle as available again (required to start to look for another cycles)
		}else{
			for(Edge link: outEdges){
				int endV = namesMap.get(link.getAttribute("End node"));				// if cycle was not found we update the path for given end vertex (because there is a connection between our initial vertex v and this end vertex)
				B.get(endV).add(new Integer(v));									// the purpose of this is to keep track which vertices create a cycle (A ->B but also B-> A) unless you use directed edges
			}
		}
		path.remove(path.lastElement());											// last element (added few lines before is being removed for recursion purposes)
		
	}
	
	private void Unmark(int u, boolean[] avail, Vector<Vector<Integer>> B){
		avail[u] = true;
		Vector<Integer> vectorElement = B.get(u);									// we also clean the path for given start vertex u
		for(int i=0; i<vectorElement.size(); i++){
			int w = vectorElement.get(i).intValue();								// and we clean paths for all others vertices involved in path
			vectorElement.remove(new Integer(w));
			if(!avail[w]) Unmark(w, avail, B);
		}
	}
}
