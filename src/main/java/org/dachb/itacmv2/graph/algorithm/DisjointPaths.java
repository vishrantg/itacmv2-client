/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

/**
 *
 * @author Russ Lankenau
 */
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.EdgeFactory;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexFactory;

public class DisjointPaths {

    /*
    Given an input graph, a start node and end node:
    - finds the shortest path between them
    - add the path to the output graph
    - flag the edges of this path as having been used up so they get ignored in future search
    - repeat until the target can't be found anymore
     */
    public static List<List<Edge>> getEdgeDisjointPaths(Graph<Vertex, Edge> inputGraph, Vertex from, Vertex to) {

        //------------------INITIALIZES THE DATA STRUCTURES---------------------
        List<List<Edge>> result = new LinkedList<List<Edge>>();
        HashMap<Vertex, List<Vertex>> visited = new HashMap<Vertex, List<Vertex>>();//every edge that has been visited gets added to the list. So (u,v) being there means visited.
        Vertex root = from, target = to;

        for (Vertex v : inputGraph.getVertices()) {
            visited.put(v, new LinkedList<Vertex>());
        }

        //----------------BFS Procedure-----------------------------------------
        while (true) {
            
            //tries to get a path from the root to the target
            Queue<Vertex> queue = new LinkedList<Vertex>();//list of nodes to visit
            HashMap<Vertex, Vertex> memory = new HashMap<Vertex, Vertex>();
            queue.add(root);
            boolean foundTarget = false;
            memory.put(root, null);//flags root node as being already visited so we don't loop on it
            while (!queue.isEmpty()) {//visits as long as there are nodes to get to
                Vertex tmp = queue.poll();//current node to visit
                if (tmp == target) {//stops if we have found the target, and record it
                    foundTarget = true;
                    break;
                }
                //let's look at each successor of that node
                for (Vertex suc : inputGraph.getSuccessors(tmp)) {
                    if (memory.get(suc) == null && !(visited.get(tmp).contains(suc))) {//if we did not visit before AND we did not flag that edge
                        memory.put(suc, tmp);//flags that suc was reached from tmp
                        queue.add(suc);
                    }
                }
            }
            if (!foundTarget) {
                break;//We need to stop doing the BFS when finding the target can't happen anymore.
            }            //A path has been found, so we now follow it and add it to the result graph
            Vertex curr = target;//start from the target
            Vertex next = null;

            List<Edge> path = new LinkedList<Edge>();

            //This while loop loops once for each node in each path.
            while ((next = memory.get(curr)) != null) {//as long as there is a predecessor to it
                if (next == target || curr == root) {
                    break;//we're starting to loop
                }
                Edge originalEdgeId = inputGraph.findEdge(next, curr);

                path.add(originalEdgeId);

                visited.get(next).add(curr);
                curr = next;//moves down the list
            }

            Collections.reverse(path);
            result.add(path);
        }

        return result;
    }

    public static List<List<Edge>> getVertexDisjointPaths(Graph<Vertex, Edge> g, Vertex from, Vertex to) {
        class VertexPair {

            public Vertex inVertex;
            public Vertex outVertex;

            public VertexPair(Vertex in, Vertex out) {
                inVertex = in;
                outVertex = out;
            }
        }

        List<List<Edge>> result = new LinkedList<List<Edge>>();
        DirectedSparseGraph<Vertex, Edge> graph;
        HashMap<Vertex, VertexPair> expandedVertices = new HashMap<Vertex, VertexPair>();
        HashMap<Vertex, Vertex> reverse_lookup = new HashMap<Vertex, Vertex>();

        VertexFactory v_factory = new VertexFactory();
        EdgeFactory e_factory = new EdgeFactory();

        Vertex fromInternal = null, toInternal = null;

        graph = new DirectedSparseGraph<>();

        // Go through the graph and replace all nodes with a pair of nodes, one with only
        // in edges, and one with only out edges.  This breaks longer disjoint paths that
        // share a vertex.
        for (Vertex v : g.getVertices()) {
            Vertex in = null, out = null;
            if (g.getInEdges(v).size() > 0) {
                in = v_factory.create();
                in.copyAttributes(v);

                if (v.equals(to)) {
                    toInternal = in;
                }
            }

            if (g.getOutEdges(v).size() > 0) {
                out = v_factory.create();
                out.copyAttributes(v);

                if (v.equals(from)) {
                    fromInternal = out;
                }
            }

            expandedVertices.put(v, new VertexPair(in, out));
            if (null != in) {
                graph.addVertex(in);
                reverse_lookup.put(in, v);
            }
            if (null != out) {
                graph.addVertex(out);
                reverse_lookup.put(out, v);
            }
            if (null != in && null != out) {
                Edge virtual_edge = null;

                virtual_edge = e_factory.create();

                // Add these edges to the lookup table with a very small weight so they don't really contribute
                // to overall distance.
                g.addEdge(virtual_edge, in, out);
            }
        }

        for (Vertex v : g.getVertices()) {
            // We're only going to iterate over the out edges, since we'll catch the
            // in edges when we iterate over the vertices that point to this one.
            for (Edge e : g.getOutEdges(v)) {
                Vertex dest = null, v_out = null, dest_in = null;
                Edge new_edge = new Edge(e.getId());

                dest = g.getOpposite(v, e);
                v_out = ((VertexPair) expandedVertices.get(v)).outVertex;
                dest_in = ((VertexPair) expandedVertices.get(dest)).inVertex;

                graph.addEdge(new_edge, v_out, dest_in);
            }
        }

        // Strip out any edges that we added to make the path vertex-disjoint.
        List<List<Edge>> paths = getEdgeDisjointPaths(g, fromInternal, toInternal);
        for (List<Edge> path : paths) {
            List<Edge> result_path = new LinkedList<Edge>();
            for (Edge edge : path) {
                Vertex e_from = g.getSource(edge), e_to = g.getDest(edge);
                Vertex original_from = reverse_lookup.get(e_from);
                Vertex original_to = reverse_lookup.get(e_to);

                if (!original_from.equals(original_to)) {
                    /* This is a valid edge, not a virtual edge */
                    result_path.add(g.findEdge(original_from, original_to));
                }
            }
            result.add(result_path);
        }

        return result;
    }
}
