/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.commons.collections15.Factory;
import org.dachb.itacmv2.graph.datastructure.Edge;
import org.dachb.itacmv2.graph.datastructure.EdgeFactory;
import org.dachb.itacmv2.graph.datastructure.Vertex;
import org.dachb.itacmv2.graph.datastructure.VertexFactory;

public class ListOfEdges {

    private Graph<Vertex, Edge> graph;
    private Factory<Vertex> vertex_factory;
    private Factory<Edge> edge_factory;
    private List<String> edges;
    private List<String> nodes;
    private HashMap<String, Vertex> mapOfNodes;				// The map between string and vertices, will be used to create edge
    private boolean themes;

    public ListOfEdges() {
        graph = new DirectedSparseGraph<Vertex, Edge>();
        vertex_factory = new VertexFactory();
        edge_factory = new EdgeFactory();
        nodes = new ArrayList<String>();
        edges = new ArrayList<String>();
        mapOfNodes = new HashMap<String, Vertex>();
        themes = false;
    }
    
    public void init(Graph<Vertex, Edge> graph, Factory<Vertex> vertex_factory, Factory<Edge> edge_factory, List<String> edges, List<String> nodes, HashMap<String, Vertex> mapOfNodes) {
        this.graph = graph;
        this.vertex_factory = vertex_factory;
        this.edge_factory = edge_factory;
        this.edges = edges;
        this.nodes = nodes;
        this.mapOfNodes = mapOfNodes;
    }

    public Graph<Vertex, Edge> getGraph() {
        return graph;
    }

    public boolean containsThemes() {
        return themes;
    }

    public boolean readDataFromFile(File f) throws Exception {
        boolean successful = true;

        // Check if the file exists
        if (f == null) {
            JOptionPane.showMessageDialog(null, "The chosen file could not be found.", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }

        // Checks that the extension of the given file is correct
        if (!f.getName().endsWith(".txt") && !f.getName().endsWith(".csv")) {
            JOptionPane.showMessageDialog(null, "The file you selected is not the correct type", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }

        BufferedReader br = null;
        String line = "";
        String[] parts;

        // try to open file and read it
        try {
            br = new BufferedReader(new FileReader(f));
            // read the first line to make sure it isn't a header
            line = br.readLine();
//			System.out.println(line);
            if (line.contains("Start node") || line.contains("End node") || line.contains("Weight") || line.contains("Weights")) {
            } else {
                parts = line.split(",");
                if (!nodes.contains(parts[0])) {
                    nodes.add(parts[0].trim());
                }
                if (!nodes.contains(parts[1])) {
                    nodes.add(parts[1].trim());
                }

                if (!edges.contains(line)) {
                    edges.add(line.trim());
                }
            }
            while ((line = br.readLine()) != null && !line.equals("")) {
                parts = line.split(",");
                if (!nodes.contains(parts[0])) {
                    nodes.add(parts[0].trim());
                }
                if (!nodes.contains(parts[1])) {
                    nodes.add(parts[1].trim());
                }

                if (!edges.contains(line)) {
                    edges.add(line.trim());
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Problem while reading file, missing data or wrong separator! ", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return successful;

    }

    public void createGraph() {
        String[] split;

        // Adding vertices to graph
        for (int n = 0; n < nodes.size(); n++) {
            Vertex v = vertex_factory.create();
            v.addAttribute("Fullname", nodes.get(n));
            v.setFullName(nodes.get(n));
            mapOfNodes.put(nodes.get(n), v);

            graph.addVertex(v);
        }

        // Adding edges to graph
        for (int i = 0; i < edges.size(); i++) {
            split = edges.get(i).split(",");
            Edge e = edge_factory.create();

            if (split.length > 2) {
                e.addAttribute("Value", Double.parseDouble(split[2]));

                // Value of weight determinates the "sign" of edge (positive/negative)
                if (split[2].contains("-")) {
                    e.addAttribute("Directionality", "-");
                } else {
                    e.addAttribute("Directionality", "+");
                }
            }

            // Make sure that both vertices exists
            if (mapOfNodes.get(split[0]) != null && mapOfNodes.get(split[1]) != null) {
                e.addAttribute("Start node", split[0]);
                e.addAttribute("End node", split[1]);
                graph.addEdge(e, mapOfNodes.get(split[0]), mapOfNodes.get(split[1]), EdgeType.DIRECTED);
            }
        }

    }

}
