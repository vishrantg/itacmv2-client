/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Vishrant Gupta
 */
public class Cycle {

    private Stack<Integer> stack;

    private int adjacencyMatrix[][];

    public Cycle() {

        stack = new Stack<Integer>();

    }

    public void dfs(int adjacency_matrix[][], int source) {

        int number_of_nodes = adjacency_matrix[source].length - 1;

        adjacencyMatrix = new int[number_of_nodes + 1][number_of_nodes + 1];

        for (int sourcevertex = 1; sourcevertex <= number_of_nodes; sourcevertex++) {

            for (int destinationvertex = 1; destinationvertex <= number_of_nodes; destinationvertex++) {

                adjacencyMatrix[sourcevertex][destinationvertex]
                        = adjacency_matrix[sourcevertex][destinationvertex];

            }

        }

        int visited[] = new int[number_of_nodes + 1];

        int element = source;

        int destination = source;

        visited[source] = 1;

        stack.push(source);

        while (!stack.isEmpty()) {

            element = stack.peek();

            destination = element;

            while (destination <= number_of_nodes) {

                if (adjacencyMatrix[element][destination] == 1 && visited[destination] == 1) {

                    if (stack.contains(destination)) {

                        System.out.println("The Graph contains cycle");

                        return;

                    }

                }

                if (adjacencyMatrix[element][destination] == 1 && visited[destination] == 0) {

                    stack.push(destination);

                    visited[destination] = 1;

                    adjacencyMatrix[element][destination] = 0;

                    element = destination;

                    destination = 1;

                    continue;

                }

                destination++;

            }

            stack.pop();

        }

    }

    public static void main(String... arg) {

        int number_of_nodes, source;

        Scanner scanner = null;

        try {

            System.out.println("Enter the number of nodes in the graph");

            scanner = new Scanner(System.in);

            number_of_nodes = scanner.nextInt();

            int adjacency_matrix[][] = new int[number_of_nodes + 1][number_of_nodes + 1];

            System.out.println("Enter the adjacency matrix");

            for (int i = 1; i <= number_of_nodes; i++) {
                for (int j = 1; j <= number_of_nodes; j++) {
                    adjacency_matrix[i][j] = scanner.nextInt();
                }
            }

            System.out.println("Enter the source for the graph");

            source = scanner.nextInt();

            Cycle checkCycle = new Cycle();

            checkCycle.dfs(adjacency_matrix, source);

        } catch (InputMismatchException inputMismatch) {

            System.out.println("Wrong Input format");

        }

        scanner.close();

    }

}
