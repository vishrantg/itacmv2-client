/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algorithm;

import edu.uci.ics.jung.algorithms.matrix.GraphMatrixOperations;
import edu.uci.ics.jung.algorithms.util.ConstantMap;
import edu.uci.ics.jung.algorithms.util.Indexer;
import edu.uci.ics.jung.graph.Graph;
import java.util.Map;
import org.apache.commons.collections15.BidiMap;

/**
 *
 * @author Vishrant Gupta
 */
public class GraphMatrixOperationsExt extends GraphMatrixOperations {

    static Object[] vertices;

    public static <V, E> SparseDoubleMatrix2DExt graphToSparseMatrix(Graph<V, E> g) {
        return graphToSparseMatrix(g, null);
    }

    public static <V, E> SparseDoubleMatrix2DExt graphToSparseMatrix(Graph<V, E> g, Map<E, Number> nev) {
        if (nev == null) {
            nev = new ConstantMap<E, Number>(1);
        }
        int numVertices = g.getVertexCount();
        SparseDoubleMatrix2DExt matrix = new SparseDoubleMatrix2DExt(numVertices,
                numVertices);

        BidiMap<V, Integer> indexer = Indexer.<V>create(g.getVertices());
        int i = 0;

        vertices = new Object[numVertices];

        for (V v : g.getVertices()) {
            vertices[i] = v;
            for (E e : g.getOutEdges(v)) {
                V w = g.getOpposite(v, e);
                int j = indexer.get(w);
                matrix.set(i, j, matrix.getQuick(i, j) + nev.get(e).doubleValue());
            }
            i++;

        }
        return matrix;
    }

    

}
