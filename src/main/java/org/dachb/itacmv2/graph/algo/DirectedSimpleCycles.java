/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.algo;

import java.util.List;
import org.jgrapht.DirectedGraph;

/**
 * A common interface for classes implementing algorithms
 * for enumeration of the simple cycles of a directed graph.
 *
 * @author Nikolay Ognyanov
 *
 * @param <V> - the vertex type.
 */
public interface DirectedSimpleCycles<V, E>
{
    /**
     * Returns the graph on which the simple cycle
     * search algorithm is executed by this object.
     *
     * @return The graph.
     */
    DirectedGraph<V, E> getGraph();

    /**
     * Sets the graph on which the simple cycle
     * search algorithm is executed by this object.
     *
     * @throws IllegalArgumentException if the
     *         argument is <code>null</code>.
     */
    void setGraph(DirectedGraph<V, E> graph);

    /**
     * Finds the simple cycles of the graph.<br/>
     * Note that the full algorithm is executed on
     * every call since the graph may have changed
     * between calls.
     *
     * @return The list of all simple cycles.
     * Possibly empty but never <code>null</code>.
     * @throws IllegalArgumentException if the
     * current graph is null.
     */
    List<List<V>> findSimpleCycles();
}
