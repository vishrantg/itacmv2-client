/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.datastructure;

import com.google.gson.Gson;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.dachb.itacmv2.graph.GraphReader;
import org.dachb.itacmv2.graph.algorithm.ListOfEdges;

public class Graph {

    private Collection<EdgeDetails> edges;
    private Collection<VertexDetail> nodes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private String name;

    public Collection<EdgeDetails> getEdges() {
        return edges;
    }

    public void setEdges(Collection<EdgeDetails> edges) {
        this.edges = edges;
    }

    // used List instead of Set in case there are multiple nodes with same name
    // depending on application user can filter out the duplicates
    public List<String> getNodeNames() {
        List<String> list = new LinkedList<>();
        if (nodes != null) {
            nodes.forEach((node) -> {
                list.add(node.getName());
            });
        }
        return list;
    }

    public Collection<VertexDetail> getNodes() {
        return nodes;
    }

    public void setNodes(Collection<VertexDetail> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "Graph [edges=" + edges + ", nodes=" + nodes + "]";
    }

    public edu.uci.ics.jung.graph.Graph<Vertex, Edge> convertToJungGraph() {

        edu.uci.ics.jung.graph.Graph<Vertex, Edge> g = new DirectedSparseGraph<>();

        GraphReader gr = new GraphReader();
        edges.forEach((edge) -> {
            gr.addEdge(edge.getSource(), edge.getTarget());
        });

        ListOfEdges listOfEdges = new ListOfEdges();

        listOfEdges.init(g, gr.getVertexFactory(), gr.getEdgeFactory(), gr.getEdges(), gr.getVertex(), gr.getNameToNode());

        listOfEdges.createGraph();

        return listOfEdges.getGraph();
    }

    public void convertFromJungGraph(edu.uci.ics.jung.graph.Graph<Vertex, Edge> g) {

        Collection<Edge> e = g.getEdges();
        GraphReader gr = new GraphReader();

        for (Edge ed : e) {
            gr.addEdge(g.getSource(ed).getFullName(), g.getDest(ed).getFullName());
        }

        ListOfEdges listOfEdges = new ListOfEdges();

        listOfEdges.init(g, gr.getVertexFactory(), gr.getEdgeFactory(), gr.getEdges(), gr.getVertex(), gr.getNameToNode());

        listOfEdges.createGraph();

        Gson gson = new Gson();

        Graph graph = gson.fromJson(gr.getGraphInJSONFormat(), Graph.class);

        this.nodes = graph.getNodes();
        this.edges = graph.getEdges();

    }

}
