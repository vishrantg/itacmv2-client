/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.datastructure;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javafx.scene.paint.Color;

public class Edge implements GraphElement, Comparable<Edge>, Cloneable {

    final int id;
    HashMap<String, Object> attributes;
    String comparableField;
    
    private List<String> reasons;

    public List<String> getReasons() {
        return reasons;
    }

    public void setReason(List<String> reasons) {
        this.reasons = reasons;
    }
    
    // added by Vishrant for student map improvement
    private javafx.scene.paint.Color edgeColor = javafx.scene.paint.Color.BLACK; //default to black

    public Color getEdgeColor() {
        return edgeColor;
    }

    public void setEdgeColor(Color edgeColor) {
        this.edgeColor = edgeColor;
    }

    
    // added by Vishrant
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Edge(int id) {
        this.attributes = new HashMap<String, Object>(3);
        this.id = id;
        this.addAttribute("ID", id);
    }

    public Edge(int id, String key, Object value) {
        this(id);
        this.attributes.put(key, value);
    }

    public int getId() {
        return this.id;
    }

    public void addAttribute(String key, Object value) {
        this.attributes.put(key, value);
    }

    public Object getAttribute(String key) {
        if (this.attributes.containsKey(key)) {
            return this.attributes.get(key);
        } else {
            return null;
        }
    }

    public Double getNumber(String key) {
        if (this.attributes.containsKey(key)) {
            return new Double("" + this.attributes.get(key));
        } else {
            return -1.0;
        }
    }

    public void setComparableField(String field) {
        this.comparableField = field;
    }

    public String toString() {
        return "E" + this.id;
    }

    public String getString(String key) {
        if (this.attributes.containsKey(key)) {
            return "" + (this.getNumber(key));
        } else {
            return this.toString();
        }
    }

    public int compareTo(Edge o) {
        if (this.comparableField != null) {
            return (int) (this.getNumber(this.comparableField) - o.getNumber(this.comparableField));
        } else {
            return this.id - o.getId();
        }
    }

    //Added by P. Giabbanelli in order to perform a deep copy of the graph
    @Override
    protected Object clone() {
        Edge e = new Edge(id);
        Iterator<String> keys = attributes.keySet().iterator();
        while (keys.hasNext()) {
            String tmp = keys.next();
            e.addAttribute(tmp, attributes.get(tmp));
        }
        e.comparableField = comparableField;
        return e;
    }

    //Added by P. Giabbanelli; used in the TableHolder to initialize the headers (columns)
    public String[] getKeys() {
        String[] res = new String[attributes.keySet().size() + 1];
        Iterator<String> it = attributes.keySet().iterator();
        int i = 1;
        res[0] = "ID";
        while (it.hasNext()) {
            res[i] = it.next();
            if (res[i].equals("ID")) {
                continue;
            }
            i++;
        }
        return res;
    }

    //Added by P. Giabbanelli; see motivations in Vertex.java
    public void dropAttribute(String key) {
        attributes.remove(key);
    }

    //Giabbanelli. Short notations. Used in heuristics of subtil.algorithms.heuristics
    public boolean hasAttribute(String key) {
        return attributes.get(key) != null;
    }

}
