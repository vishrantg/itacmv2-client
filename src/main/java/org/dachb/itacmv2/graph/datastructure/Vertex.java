/*
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 *
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 *
 * If you are using this application, please cite this paper: 
 * "An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
 * ill-structured problems"
 *
 * Advisor: Philippe J. Giabbanelli
 * Collaborator: Andrew A. Tawfik
 *
 * MIT License
 *
 * Copyright (c) 2018 Vishrant Gupta
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.dachb.itacmv2.graph.datastructure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javafx.scene.paint.Color;

public class Vertex implements GraphElement, Comparable<Vertex>, Cloneable {

    final int id;
    HashMap<String, Object> attributes;

    private List<String> reasons;

    public List<String> getReasons() {
        return reasons;
    }

    public void setReason(List<String> reasons) {
        this.reasons = reasons;
    }
    
    // added by Vishrant for student map improvement
    private javafx.scene.paint.Color vertexColor = javafx.scene.paint.Color.BLACK; //default to black

    public Color getVertexColor() {
        return vertexColor;
    }

    public void setVertexColor(Color vertexColor) {
        this.vertexColor = vertexColor;
    }
    
    public void setShortName(String s) {
        attributes.put("SHORTNAME", s);
    }

    public void setFullName(String s) {
        attributes.put("FULLNAME", s);
    }

    public String getShortName() {
        return (String) attributes.get("SHORTNAME");
    }

    public String getFullName() {
        return (String) attributes.get("FULLNAME");
    }

    public Vertex(int id) {
        this.attributes = new HashMap<String, Object>(3);
        this.id = id;
    }

    public Vertex(int id, String key, Object value) {
        this.attributes = new HashMap<String, Object>(3);
        this.id = id;
        this.attributes.put(key, value);
    }

    //alias for the function getFullName
    public String getName() {
        return getFullName();
    }

    public int getId() {
        return this.id;
    }

    public void addAttribute(String key, Object value) {
        this.attributes.put(key, value);
        if (key.equals("FULLNAME")) {//Synchronizes short names with long names
            String nodeName = (String) value;
            if (nodeName.length() > 10) {
                this.attributes.put("SHORTNAME", nodeName.substring(0, 10) + "..");
            } else {
                this.attributes.put("SHORTNAME", nodeName);
            }
        }
    }

    public Object getAttribute(String key) {
        if (key.equals("ID")) {
            return getId();
        }
        if (this.attributes.containsKey(key)) {
            return this.attributes.get(key);
        }
        return null;
    }

    public Double getNumber(String key) {
        if (this.attributes.containsKey(key)) {
            return (Double) this.attributes.get(key);
        } else {
            return 0.0;
        }
    }

    @Override
    public String toString() {
        return getName();
        //return "V"+this.id;
    }

    public String getString(String key) {
        if (this.attributes.containsKey(key)) {
            return this.attributes.get(key).toString();
        } else {
            return this.toString();
        }
    }

    //Added by P. Giabbanelli in order to perform a deep copy of the graph
    @Override
    protected Object clone() {
        Vertex v = new Vertex(id);
        Iterator<String> keys = attributes.keySet().iterator();
        while (keys.hasNext()) {
            String tmp = keys.next();
            v.addAttribute(tmp, attributes.get(tmp));
        }
        return v;
    }

    //Added by P. Giabbanelli for graph models in which we had to distinguish
    //between generic vertices.
    public int compareTo(Vertex v) {
        return v.getId() - this.getId();
    }

    //Added by P. Giabbanelli; used in the TableHolder to initialize the headers (columns)
    public String[] getKeys() {
        String[] res = new String[attributes.keySet().size() + 1];
        Iterator<String> it = attributes.keySet().iterator();
        int i = 1;
        res[0] = "ID";
        while (it.hasNext()) {
            res[i] = it.next();
            i++;
        }
        return res;
    }

    //added by Issam in order to verify vertice equality.
    public boolean equals(Vertex v) {
        return (this.getId() == v.getId());
    }

    // added by Vishrant, equality based on full name
    @Override
    public int hashCode() {
        int hash = 3;
        
        String fullName = (String)this.attributes.get("FULLNAME");
        
        hash = 59 * hash + Objects.hashCode(fullName);
        return hash;
    }

    // added by Vishrant, equality based on full name
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex other = (Vertex) obj;
        
        String currFullName = (String)this.attributes.get("FULLNAME");
        String otherFullName = (String)other.attributes.get("FULLNAME");
        
        if (!Objects.equals(currFullName, otherFullName)) {
            return false;
        }
        return true;
    }

    
    
    //Added by P. Giabbanelli. Temporary information are sometimes stored by
    //an algorithm at a Vertex and we prefer to erase it once the computation
    //is done in order to prevent the user from seeing all the crap we forgot.
    public void dropAttribute(String key) {
        attributes.remove(key);
    }

    //Giabbanelli. Since we added it for edges, may as well add it for vertices.
    public boolean hasAttribute(String key) {
        return attributes.get(key) != null;
    }

    //Added by P. Giabbanelli. Copy all specified attributes of the given node.
    public void copyAttributes(HashSet<String> whichAttributes, Vertex v) {
        for (String s : whichAttributes)//we assume that |whichAttributes|<|attributes(v)|
        {
            if (v.attributes.keySet().contains(s)) {
                attributes.put(s, v.getAttribute(s));
            }
        }
    }

    //Copies ALL attributes from the given vertex
    public void copyAttributes(Vertex v) {
        for (String s : v.getKeys()) {
            attributes.put(s, v.getAttribute(s));
        }
    }
}
