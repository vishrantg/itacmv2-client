<?xml version="1.0" encoding="UTF-8"?>
<!--
Written by Vishrant K. Gupta &lt;vishrant.gupta@gmail.com>, January 2018

This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
Mental Representations: Visualizing and Matching Causal Networks"

If you are using this application, please cite this paper: 
"An online environment to compare studentsÃ¢â‚¬â„¢ and expert solutions to 
ill-structured problems"

Advisor: Philippe J. Giabbanelli
Collaborator: Andrew A. Tawfik

MIT License

Copyright (c) 2018 Vishrant Gupta

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-->

<?import java.lang.String?>
<?import javafx.geometry.Insets?>
<?import javafx.scene.chart.CategoryAxis?>
<?import javafx.scene.chart.NumberAxis?>
<?import javafx.scene.chart.StackedBarChart?>
<?import javafx.scene.control.Button?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.control.ScrollPane?>
<?import javafx.scene.control.TitledPane?>
<?import javafx.scene.layout.AnchorPane?>
<?import javafx.scene.layout.ColumnConstraints?>
<?import javafx.scene.layout.GridPane?>
<?import javafx.scene.layout.RowConstraints?>
<?import javafx.scene.layout.VBox?>

<ScrollPane fitToHeight="true" fitToWidth="true" minHeight="-Infinity" xmlns="http://javafx.com/javafx/8.0.111" xmlns:fx="http://javafx.com/fxml/1" fx:controller="org.dachb.itacmv2.ui.controller.GraphMatricesController">
   <content>
      <VBox>
         <children>
            <TitledPane animated="false" collapsible="false">
               <content>
                  <AnchorPane minHeight="0.0" minWidth="0.0" prefWidth="200.0" styleClass="while-background">
                     <children>
                        <GridPane layoutX="14.0" layoutY="14.0" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
                           <columnConstraints>
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                           </columnConstraints>
                           <rowConstraints>
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints />
                           </rowConstraints>
                           <children>
                              <TitledPane expanded="false" styleClass="while-background" text="How does it work?" wrapText="true" GridPane.columnSpan="2" GridPane.rowIndex="2" GridPane.rowSpan="2">
                                 <content>
                                    <AnchorPane minHeight="0.0" minWidth="0.0" prefHeight="180.0" prefWidth="200.0" styleClass="while-background">
                                       <children>
                                          <Label layoutY="4.0" styleClass="while-background" text="Graph Edit Distance (GED) measures the distance (or 'dissimilarity') between two maps by taking into account their structure and the nodes' labels. Its theoretical foundations build on string comparison and graph theory. Practically, the distance is the minimum number of operations required to transform one map into another. Operations include insertion and deletion of nodes or edges, as well as re-labelling of nodes. While each operation can be given a different cost, we assume that they are all as important, e.g. a student's map differs equally from the expert map whether it misses an edge or has an extra one. As finding the minimum number of operations is a difficult optimization problem, we use the beam search heuristics (a variation of the A* algorithm) implemented by Dr. Kaspar Riesen. For software implementations of the GED, we refer the reader to Riesen et al., LNCS 7877, pp. 142–151, 2013, Springer." wrapText="true" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0" />
                                       </children>
                                    </AnchorPane>
                                 </content>
                              </TitledPane>
                              <Label text="The distance is: -" GridPane.halignment="LEFT" />
                           </children>
                           <styleClass>
                              <String fx:value="while-background" />
                              <String fx:value="transparent-background" />
                           </styleClass>
                        </GridPane>
                     </children>
                  </AnchorPane>
               </content>
               <graphic>
                  <GridPane maxWidth="1.7976931348623157E308">
                     <columnConstraints>
                        <ColumnConstraints hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                        <ColumnConstraints halignment="RIGHT" hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                     </columnConstraints>
                     <rowConstraints>
                        <RowConstraints vgrow="ALWAYS" />
                     </rowConstraints>
                     <children>
                        <Button mnemonicParsing="false" text="CALCULATE" GridPane.columnIndex="1" GridPane.halignment="LEFT">
                           <GridPane.margin>
                              <Insets left="10.0" />
                           </GridPane.margin>
                        </Button>
                        <Label text="Graph Edit Distance (GED)" />
                     </children>
                  </GridPane>
               </graphic>
            </TitledPane>
            <TitledPane animated="false" collapsible="false">
               <content>
                  <AnchorPane minHeight="0.0" minWidth="0.0" prefWidth="200.0" styleClass="while-background">
                     <children>
                        <GridPane AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
                           <columnConstraints>
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                           </columnConstraints>
                           <rowConstraints>
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints />
                           </rowConstraints>
                           <children>
                              <StackedBarChart fx:id="graphKernelStackedBarChart" animated="false" GridPane.columnSpan="2" GridPane.rowSpan="2">
                                <xAxis>
                                  <CategoryAxis side="BOTTOM" />
                                </xAxis>
                                <yAxis>
                                  <NumberAxis side="LEFT" />
                                </yAxis>
                              </StackedBarChart>
                              <TitledPane expanded="false" styleClass="while-background" text="How does it work?" wrapText="true" GridPane.columnSpan="2" GridPane.rowIndex="2" GridPane.rowSpan="2">
                                 <content>
                                    <AnchorPane minHeight="0.0" minWidth="0.0" prefHeight="180.0" prefWidth="200.0" styleClass="while-background">
                                       <children>
                                          <Label layoutY="4.0" styleClass="while-background" text="A graph kernel intuitively means that we focus on comparing the &quot;core&quot; of the maps. What is considered as &quot;core&quot; depends on the application. In systems thinking, the presence of 'loops' or 'cycles' is particularly important. A node is part of a loop if we can follow a sequence of causes that eventually feeds back into this node. Our process thus starts by (1) extracting all the loops in the student's and the expert's map. Loops can have different lengths: for instance, A-&gt;B-&gt;C has a length of 2, as two edges are involved. For each map, we thus (2) compute the histogram of the loops' length, where the x-axis is the length of the loop and the y-axis is the prevalence of this length on a normalized scale from 0 to 1. Finally, the distance between the two distributions is computed (3) using the Bhattacharyya distance, which measures the overlap between two discrete probability distributions." wrapText="true" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0" />
                                       </children>
                                    </AnchorPane>
                                 </content>
                              </TitledPane>
                           </children>
                           <styleClass>
                              <String fx:value="transparent-background" />
                              <String fx:value="while-background" />
                           </styleClass>
                        </GridPane>
                     </children>
                  </AnchorPane>
               </content>
               <graphic>
                  <GridPane maxWidth="1.7976931348623157E308">
                     <columnConstraints>
                        <ColumnConstraints hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                        <ColumnConstraints halignment="RIGHT" hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                     </columnConstraints>
                     <rowConstraints>
                        <RowConstraints vgrow="ALWAYS" />
                     </rowConstraints>
                     <children>
                        <Button mnemonicParsing="false" text="CALCULATE" GridPane.columnIndex="1" GridPane.halignment="LEFT">
                           <GridPane.margin>
                              <Insets left="10.0" />
                           </GridPane.margin>
                        </Button>
                        <Label text="Graph Kernel" />
                     </children>
                  </GridPane>
               </graphic>
            </TitledPane>
            <TitledPane animated="false" collapsible="false">
               <content>
                  <AnchorPane minHeight="0.0" minWidth="0.0" prefWidth="200.0" styleClass="while-background">
                     <children>
                        <GridPane styleClass="while-background" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
                           <columnConstraints>
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                              <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" prefWidth="100.0" />
                           </columnConstraints>
                           <rowConstraints>
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
                              <RowConstraints />
                           </rowConstraints>
                           <children>
                              <TitledPane expanded="false" styleClass="while-background" text="How does it work?" wrapText="true" GridPane.columnSpan="2" GridPane.rowIndex="2" GridPane.rowSpan="2">
                                 <content>
                                    <AnchorPane minHeight="0.0" minWidth="0.0" prefHeight="180.0" prefWidth="200.0" styleClass="while-background">
                                       <children>
                                          <Label layoutY="4.0" text="Several characteristics can be measured on a graph, such as the number of nodes (n) or edges (m). A graph can then be placed into a vector space with respect to these characteristics. For instance, a graph with 5 nodes and two edges would be represented by the vector (5, 4). Another graph with 3 nodes and 2 edges would be (3, 2). Here, we include n and m as well as their interaction via the graph density, which captures the ratio of edges present compared to those that could exist. Formally, the graph density is defined as 2m / (n*(n-1)). To compare two graphs via their vectors, we use the cosine similarity. A value of −1 means the maps are exactly opposite, whereas 1 means they are identical with respect to the characteristics retained, and 0 indicates no correlation whatsoever. As the method is flexible, additional characteristics could be included, with each new one leading to another dimension in the vector space." wrapText="true" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0" />
                                       </children>
                                    </AnchorPane>
                                 </content>
                              </TitledPane>
                           </children>
                        </GridPane>
                     </children>
                  </AnchorPane>
               </content>
               <graphic>
                  <GridPane maxWidth="1.7976931348623157E308">
                     <columnConstraints>
                        <ColumnConstraints hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                        <ColumnConstraints halignment="RIGHT" hgrow="ALWAYS" minWidth="200.0" prefWidth="200.0" />
                     </columnConstraints>
                     <rowConstraints>
                        <RowConstraints vgrow="ALWAYS" />
                     </rowConstraints>
                     <children>
                        <Button mnemonicParsing="false" text="CALCULATE" GridPane.columnIndex="1" GridPane.halignment="LEFT">
                           <GridPane.margin>
                              <Insets left="10.0" />
                           </GridPane.margin>
                        </Button>
                        <Label text="Graph Embedding" />
                     </children>
                  </GridPane>
               </graphic>
            </TitledPane>
         </children>
      </VBox>
   </content>
</ScrollPane>
